#pragma once

#include <vector>

#include "physics/PhysicsEvent.hpp"
#include "physics/Body.hpp"
#include "physics/DynamicBody.hpp"
#include "physics/RadialCollision.hpp"
#include "physics/CollisionPrediction.hpp"
#include "system/Time.hpp"

namespace ss { namespace physics {
    struct CollisionCandidate {
        size_t indexOfBodyA;
        size_t indexOfBodyB;
        CollisionPrediction prediction;
    };

    class System {
    public:
        void attachBody(DynamicBody::shared& body);
        void attachStatic(DynamicBody::shared& body);
        void update(const system::Time dt);

        std::vector<DynamicBody::shared> getDynamicBodiesBoundingPoint(const math::Vector2f point);
        std::vector<DynamicBody::shared> getStaticBodiesBoundingPoint(const math::Vector2f point);
    private:
        void processEvents(const system::Time dt);
        void processCollisionEvent(const CollisionEvent collision, system::Time dt);
        std::vector<DynamicBody::shared> staticBodies;
        std::vector<DynamicBody::shared> dynamicBodies;
        std::vector<Event> events;

        const math::Vector2f gravityAcceleration = math::Vector2f(0.f, 100.f);
    };
}
}