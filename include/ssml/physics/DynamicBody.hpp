#pragma once

#include <memory>

#include "math/Transform.hpp"
#include "math/Vector2.hpp"
#include "math/Rect.hpp"
#include "system/Time.hpp"
#include "physics/Body.hpp"

namespace ss { namespace physics {
    using Force = math::Vector2f;
    using Impulse = math::Vector2f;

    //! A dynamic rigid body
    /*! This class represents a dynamic rigid body, with associated velocity and
        a collision mesh. Although this class itself work, the current implementation
        of collision meshes are unfinished and will not work. */
    class DynamicBody
    : public physics::Body {
    public:
        using shared = std::shared_ptr<DynamicBody>;
    public:
        DynamicBody();

        virtual inline bool isDynamic() { return true; }

        void addForce(const Force force, const system::Time& dt);
        void addForce(const Force force, math::Vector2f forceOffset, const system::Time& dt);

        void addMomentum(const Impulse impulse);
        void addAngularMomentum(const float angularMomentum);
        
        void addVelocity(const math::Vector2f vel);
        void setVelocity(const math::Vector2f vel);
        math::Vector2f getVelocity() const;

        void setAngularVelocity(const float radsPerSec);
        void addAngularVelocity(const float radsPerSec);
        float getAngularVelocity() const;
        
        void setMass(const float m);
        float getMass() const;

        //! Set the inertial moment of the body, as measured from the position.
        void setInertialMoment(const float moment);
        float getInertialMoment() const;

        //! If very slow or not moving at all, this returns True.
        bool isAtRest() const;

        //! Advance time and update position
        void update(const system::Time& dt);

        // Get the linearly predicted transform of the body given a time offset dt.
        math::Transform getFutureTransform(const float dt) const;
    private:
        float mass;
        float inertialMoment;
        float angularVelocity;
        math::Vector2f velocity;
    };
}
}