#pragma once

#include "physics/DynamicBody.hpp"
#include "math/Vector2.hpp"
#include "math/VectorArithmetic.hpp"
#include "system/Time.hpp"

namespace ss { namespace physics {
    struct CollisionPrediction {
        enum Flag {
            NoCollision,
            Collision
        } predictionFlag;

        system::Time timeUntilCollision;

        math::Vector2f positionAtImpactA;
        math::Vector2f positionAtImpactB;
    };

    bool operator < (const CollisionPrediction& lhs, const CollisionPrediction& rhs);

    CollisionPrediction predictRadialCollision(const physics::DynamicBody::shared bodyA, const physics::DynamicBody::shared bodyB);
    CollisionPrediction predictRadialCollision(
        const math::Vector2f posA, const math::Vector2f posB,
        const math::Vector2f velA, const math::Vector2f velB,
        const float radA, const float radB);
}
}