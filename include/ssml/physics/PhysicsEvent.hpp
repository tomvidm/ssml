#pragma once

#include "physics/DynamicBody.hpp"
#include "system/Time.hpp"

namespace ss { namespace physics {
    enum EventType{
        None,
        Collision,
        BodyEntersRestState,
        BodyExitsRestState
    };

    class CollisionEvent {
    public:
        CollisionEvent();
        CollisionEvent(DynamicBody::shared bodyA, DynamicBody::shared bodyB, system::Time dt);
        CollisionEvent(const CollisionEvent& other);
        CollisionEvent(CollisionEvent&& other);
        ~CollisionEvent();

        CollisionEvent operator=(const CollisionEvent& other);

        inline DynamicBody::shared getBodyA() const { return bodyA; }
        inline DynamicBody::shared getBodyB() const { return bodyB; }
        inline system::Time getTimeToImpact() const { return timeToImpact; }
    protected:
        DynamicBody::shared bodyA;
        DynamicBody::shared bodyB;
        system::Time timeToImpact;
    };

    class Event {
    public:
        Event();
        Event(const CollisionEvent& event);
        Event(const Event& other);
        Event(Event&& other);
        ~Event();

        Event operator=(const Event& other);

        inline EventType getType() const { return type; }
        CollisionEvent getCollisionEvent() const;
    protected:
        EventType type;

        // During assignment, a segfault happened when
        // using a union. When needed, be prepared to fix that segfault.
        CollisionEvent collisionEvent;
    };

    Event makeCollisionEvent(DynamicBody::shared bodyA, DynamicBody::shared bodyB, system::Time dt);
    //Event buildCollisionEvent(Body::shared bodyA, Body::shared bodyB, system::Time dt);
}
}