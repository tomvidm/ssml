#pragma once

#include <memory>

#include "math/Transform.hpp"
#include "math/Vector2.hpp"
#include "math/Rect.hpp"
#include "physics/CollisionMesh.hpp"

namespace ss { namespace physics {
    class Body
    : public math::Transformable {
    public:
        using shared = std::shared_ptr<Body>;
    public:
        Body();

        math::Rect<float> getLocalBounds() const;
        math::Rect<float> getGlobalBounds() const;

        virtual inline bool isDynamic() { return false; }
        void setCollisionMesh(CollisionMesh::shared mesh);
        const CollisionMesh::shared getSharedCollisionMesh() const;
        const CollisionMesh* getRawCollisionMesh() const;
    private:
        physics::CollisionMesh::shared collisionMesh;
    };

    using SharedBody = std::shared_ptr<Body>;
}
}