#pragma once

#include "physics/PhysicsEvent.hpp"
#include "physics/Body.hpp"
#include "physics/DynamicBody.hpp"
#include "physics/CollisionMesh.hpp"
#include "physics/CollisionMeshIntersection.hpp"
#include "system/Time.hpp"

namespace ss { namespace physics {
    Event predictCollision(
        const DynamicBody::shared bodyA,
        const DynamicBody::shared bodyB,
        const system::Time dt
    );

    bool checkTunneling(
        const DynamicBody::shared bodyA,
        const DynamicBody::shared bodyB,
        const system::Time dt
    );
}
}