#pragma once

#include "math/Transform.hpp"

#include "physics/CollisionMesh.hpp"

/*
    Notes on complexity.
    This is solved using double dispatch (I think?).
    Solution may have to be refined, but first I have to see how I can better
    utilize polymorphism. Currently, there is still explicit branch with switch statements.

    For N collision mesh types:
    Number of methods:
    * Unkown, Unknown = 1 (Switch banching over N mesh types)
    * Known, Unknown = N (Switch branch over N mesh types)
    * Known, Known = N(N + 1)/2
    
    |F| = 1 + N + N(N + 1)/2
    |F| = 1 + N(1 + (N + 1)/2)
    
    For MeshTypes Line, AABB, Box, Circle, Convex and Concave, this gives N = 6
    Which gives f(6) = 1 + 6 + 6*7 / 2
                     = 28

    28 Methods.

    Should be manageable.
*/

namespace ss { namespace physics {
    // Determine mesh type of first argument and call the relevant method under
    bool checkIntersection(
        const CollisionMesh* meshA,
        const CollisionMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    // Determine mesh type of second argument given that first agument is LineMesh
    // and call the relevant method under
    bool checkIntersection(
        const LineMesh* meshA,
        const CollisionMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    // Determine mesh type of second argument given that first agument is AABBMesh
    // and call the relevant method
    bool checkIntersection(
        const AABBMesh* meshA,
        const CollisionMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    // Determine mesh type of second argument given that first agument is CircleMesh
    // and call the relevant method
    bool checkIntersection(
        const CircleMesh* meshA,
        const CollisionMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    // Determine mesh type of second argument given that first agument is ConvexMesh
    // and call the relevant method
    bool checkIntersection(
        const ConvexMesh* meshA,
        const CollisionMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    bool checkLineToLineIntersection(
        const LineMesh* meshA, 
        const LineMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    bool checkLineToAABBIntersection(
        const LineMesh* meshA, 
        const AABBMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    bool checkLineToCircleIntersection(
        const LineMesh* meshA, 
        const CircleMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    bool checkLineToConvexIntersection(
        const LineMesh* meshA, 
        const ConvexMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    bool checkAABBToAABBIntersection(
        const AABBMesh* meshA, 
        const AABBMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    bool checkAABBToCircleIntersection(
        const AABBMesh* meshA, 
        const CircleMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    bool checkAABBToConvexIntersection(
        const AABBMesh* meshA, 
        const ConvexMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    bool checkCircleToCircleIntersection(
        const CircleMesh* meshA, 
        const CircleMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    bool checkCircleToConvexIntersection(
        const CircleMesh* meshA, 
        const ConvexMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );

    bool checkConvexToConvexIntersection(
        const ConvexMesh* meshA, 
        const ConvexMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    );
}
}
