#pragma once

#include "physics/Body.hpp"
#include "math/Rect.hpp"
#include "math/Vector2.hpp"
#include "system/Time.hpp"

namespace ss { namespace physics {
    bool sweepGhostAABB(physics::Body::shared& bodyA,
                        physics::Body::shared& bodyB,
                        const system::Time ghostingInterval);

    bool sweepGhostAABB(const math::Rect<float> rectA,
                        const math::Rect<float> rectN,
                        const math::Vector2f velocityA,
                        const math::Vector2f velocityB,
                        const system::Time ghostingInterval);
}
}