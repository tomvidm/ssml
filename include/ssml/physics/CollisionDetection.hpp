#pragma once

#include "physics/DynamicBody.hpp"

namespace ss { namespace physics {
    bool bodiesIntersect(const DynamicBody* bodyA, const DynamicBody* bodyB, const float timeDelay = 0.f);
}
}
