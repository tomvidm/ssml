#pragma once

#include <memory>
#include <vector>

#include "graphics/Vertex.hpp"
#include "math/Transform.hpp"
#include "math/Rect.hpp"
#include "math/Vector2.hpp"
#include "math/VectorGeometry.hpp"
#include "math/ConvexPolygon.hpp"

namespace ss { namespace physics {
    using std::shared_ptr;

    enum MeshType {
        Line,
        AABB,
        // Box
        Circle,
        Convex
        // ConvexPolygon
        // ConcavePolygon
    };

    //! An abstract class representing a collision mesh.
    class CollisionMesh {
    public:
        using shared = std::shared_ptr<CollisionMesh>;
    public:
        virtual std::vector<math::Vector2f> getVertices() const = 0;
        virtual math::Rect<float> getBoundingRect() const = 0;
        virtual math::Rect<float> getTransformedBoundingRect(const math::Transform& transform) const = 0;
        virtual MeshType getMeshType() const = 0;
    };

    //! A collision mesh representing lines.
    /*! The line mesh represents a collidable line segment set between two points. */
    class LineMesh 
    : public CollisionMesh {
    public:
        LineMesh(const math::Line line);
        
        virtual std::vector<math::Vector2f> getVertices() const;
        virtual math::Rect<float> getBoundingRect() const;
        virtual math::Rect<float> getTransformedBoundingRect(const math::Transform& transform) const;
        inline virtual MeshType getMeshType() const { return MeshType::Line; }
        inline math::Line getLine() const { return line; }

    private:
        math::Line line;
    };

    //! The Axis Aligned Bounding Box mesh represents what the name says.
    class AABBMesh
    : public CollisionMesh {
    public:
        AABBMesh(const math::Rect<float> rect);

        virtual std::vector<math::Vector2f> getVertices() const;
        virtual math::Rect<float> getBoundingRect() const;
        virtual math::Rect<float> getTransformedBoundingRect(const math::Transform& transform) const;
        inline virtual MeshType getMeshType() const { return MeshType::AABB; }
        inline math::Rect<float> getRect() const { return rect; }

    private:
        math::Rect<float> rect;
    };

    //! A simple circular collision mesh.
    class CircleMesh 
    : public CollisionMesh {
    public:
        CircleMesh(const float radius);

        virtual std::vector<math::Vector2f> getVertices() const;
        virtual math::Rect<float> getBoundingRect() const;
        virtual math::Rect<float> getTransformedBoundingRect(const math::Transform& transform) const;
        inline virtual MeshType getMeshType() const { return MeshType::Circle; }
        inline float getRadius() const { return radius; }

    private:
        float radius;
    };

    //! Only god knows how to implement this one in a good way. I certainly don't. Yet.
    class ConvexMesh
    : public CollisionMesh {
    public:
        ConvexMesh(const math::ConvexPolygon::shared& convex);

        virtual std::vector<math::Vector2f> getVertices() const;
        virtual math::Rect<float> getBoundingRect() const;
        virtual math::Rect<float> getTransformedBoundingRect(const math::Transform& transform) const;
        inline virtual MeshType getMeshType() const { return MeshType::Convex; }
        inline math::ConvexPolygon::shared getConvexPolygon() const { return convex; }
    private:
        math::ConvexPolygon::shared convex;
    };
}
}
