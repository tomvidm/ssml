#pragma once

#include "system/Time.hpp"
#include "math/Rect.hpp"

namespace ss { namespace animation {
    //! POD that holds the frame duration and the texture rect
    struct Frame {
        const system::Time frameDuration;
        const math::Rect<float> textureRect;
    };
}
}