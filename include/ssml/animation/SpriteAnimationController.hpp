#pragma once

#include <vector>
#include <functional>

#include "system/Time.hpp"

#include "animation/SpriteAnimation.hpp"

namespace ss { namespace animation {
    //! Controls the state of a sprite animation
    /*! This class is used by Sprite to hold state information about the current
        animation.
    */
    class SpriteAnimationController {
    public:
        SpriteAnimationController();
        void setAnimation(const SpriteAnimation& frameSeq, const bool resetFrameCounter = true);
        void reset();
        
        //! Advance time and update the internal state
        /*  This method updates the internal state of the controller, and makes sure that
            the animation will update correctly, independent of frame rate.
        */
        bool update(const system::Time& dt);

        //! Return the current frame
        const Frame getCurrentFrame() const;
        //! Return the duration of the current frame
        const system::Time getCurrentFrameDuration() const;
        //! Return the texture rect of the current frame
        const math::Rect<float> getCurrentFrameRect() const;
        //! Return the current frame index
        const size_t getCurrentFrameIndex() const;
    private:
        size_t currentFrameIndex;
        system::Time frameDuration;
        bool animationIsFinished;
        SpriteAnimation* animationPtr;
    };
}
}