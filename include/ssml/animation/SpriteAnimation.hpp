#pragma once

#include <vector>

#include "animation/Frame.hpp"

namespace ss { namespace animation {
    //! A container for animation frames
    /*  The SpriteAnimation class holds a vector of frames, 
        and provides accessor methods. Might just as well be a vector,
        but this interface communicates purpose better.
    */
    class SpriteAnimation {
    public:
        //! Constructs an empty animation
        SpriteAnimation();

        void addFrame(const Frame& frame);

        const size_t getNumFrames() const;
        const Frame getFrame(const size_t index) const;
    private:
        std::vector<Frame> frames;
    };
}
}