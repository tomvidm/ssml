#pragma once

#include "math/Vector2.hpp"

namespace ss { namespace animation {
    class Keyframe {
    public:
        Keyframe(const math::Vector2f& position, 
                const math::Vector2f& scale, 
                const float angle);
        Keyframe(const Keyframe& other);

        inline math::Vector2f getPosition() const { return position; }
        inline math::Vector2f getScale() const { return scale; }
        inline float getAngle() const { return angle; }
    private:
        math::Vector2f position;
        math::Vector2f scale;
        float angle;
    };

    Keyframe operator * (const Keyframe& lhs, const float factor);
    Keyframe operator * (const float factor, const Keyframe& lhs);
}
}
