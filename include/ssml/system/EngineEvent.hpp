#pragma once

namespace ss { namespace system {
    //! Engine event types
    enum class EngineEventType : size_t {
        Quit,
        InitFailure,
        InitSuccess,
        None
    };

    //! Event object describing some engine event
    /*! This class encapsulates information about engine events, such as quitting,
        setup failure, setup success, loss of focus etc. They are stored in the
        engine event queue.
    */
    class EngineEvent {
    public:
        //! Constuct an empty event
        EngineEvent() 
        : type(EngineEventType::None) {;}

        //! Construct default based on type
        EngineEvent(const EngineEventType type)
        : type(type) {;}

        //! Return the engine event type
        inline EngineEventType getType() const { return type; }
    private:
        EngineEventType type;
    };
}
}