#pragma once

#include <vector>
#include <map>
#include <string>
#include <memory>
#include <stdexcept>

#include "SFML/Graphics.hpp"

#include "graphics/Renderable.hpp"
#include "system/RenderLayer.hpp"

namespace ss { namespace system {
    //! The SSML Renderer
    /*! The Renderer is a class that handles rendering, so that you
        don't have to write your own rendering loop. The renderer interfaces
        with both SFML drawables and SSML renderables. 
        
        However, because SFML drawables don't give access to any information about the
        size of the drawn object, the renderer can't perform culling on these. So drawables
        will always be "drawn", no matter if they are, in fact, located way off screen. Therefore,
        using the SSML renderables is better, as they provide an interface for retrieving a rect for culling.*/
    class Renderer {
    public:
        Renderer();
        //! Add a renderable for rendering
        void addRenderable(SharedRenderable renderable);
        //! Add a drawable for rendering (DEPRECATE THIS)
        void addDrawable(sf::Drawable* drawable);
        //! Sets the window in which the renderer will draw
        void setRenderWindow(sf::RenderWindow* windowPtr);
        //! Draw everything covered by provided rect to the window
        void render(math::Rect<float> coverage);
        //! Draw everything to the window.
        void renderAll();
    private:
        sf::RenderWindow* window;

        std::vector<graphics::Renderable*> renderables;
        RenderLayer defaultLayer;
        std::vector<system::RenderLayer> renderLayers;
        std::vector<sf::Drawable*> drawables;
    };
}
}