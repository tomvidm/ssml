#pragma once

#include <string>
#include <vector>

#include "system/Renderer.hpp"
#include "system/Time.hpp"
#include "input/Input.hpp"

#include "EngineEvent.hpp"
#include "EngineConfig.hpp"

namespace ss { namespace system {
    //! A instance of the SSML engine
    /*! Possibly a misguided name. This class is intended to handle the details of window handling,
        rendering and to decouple game logic and rendering. However, the user must still decide how to respond to various
        events, such as input events, physics events and engine events - all of which the Engine object gives direct access to.

        The Engine object is constructed by supplying it with a configuration object on construction (see EngineConfig ).

        The following presents a minimal working example of running the engine.
        \code{cpp}
#include "Engine.hpp"

int main() {
    ss::EngineConfig config = 
        ss::EngineConfig::loadFromJSON("../resources/configs/config.json");
    ss::Engine engine(config);
    while (engine.isRunning()) {
        const ss::system::Time time = engine.perFrameUpdate(); // Returns time since last frame
            // Game logic goes here
        engine.render()
    }
    return 0;
}
        \endcode
    */
    class Engine {
    public:
        Engine() = delete;
        Engine(const EngineConfig& config);

        //! Loads engine configurations from config file
        void loadConfigurations(const std::string& pathToConfig);

        //! Process inputs and overwrite old input events
        void processInputs();

        //! Mystery function
        void render();

        //! Perform per frame update of the engine and returns the current frame time
        /*! This method clears any old input events from the list and processes events again.
            This method also advances time for the physics system (when it is implemented.) */
        system::Time perFrameUpdate();

        //! Returns a reference to the renderer
        inline system::Renderer& getRenderer() { return renderer; }
        //! Returns a const reference to the renderer
        inline const system::Renderer& getRenderer() const { return renderer; }
        //! Return a const reference to the input handler
        inline const input::InputHandler& getInputHandler() const { return inputHandler; }

        //! Returns a vector of input events
        inline const std::vector<input::InputEvent>& getInputEvents() const { return inputEvents; }
        //! Returns a vector of engine events
        inline const std::vector<EngineEvent>& getEngineEvents() const { return engineEvents; }

        //! Returns true if still running
        inline bool isRunning() const { return m_isRunning; }
    private:
        bool m_isRunning;
        const EngineConfig engineConfig;
        system::Clock perFrameTimer;
        sf::RenderWindow window;
        system::Renderer renderer;
        input::InputHandler inputHandler;

        std::vector<input::InputEvent> inputEvents;
        std::vector<EngineEvent> engineEvents;
    };
}
}