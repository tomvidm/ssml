#pragma once

#include <vector>
#include <stack>

#include "SFML/Graphics.hpp"

#include "graphics/Renderable.hpp"

#include "math/Rect.hpp"

namespace ss { namespace system {
    //! A RenderLayer is meant to help organize renderables in layers.
    using graphics::SharedRenderable;
    using graphics::Renderable;

    class RenderLayer {
    public:
        void addRenderable(SharedRenderable& renderable);
        //! Updates the render list and culls the renderables
        void updateRenderList(const math::Rect<float> cullingRect);
        //! Removes all renderables which are only referenced by the renderer
        void cleanRenderables();
        //! Renders to the given window
        void render(sf::RenderWindow* window);
    private:
        std::vector<SharedRenderable> renderables;
        std::vector<sf::Drawable*> renderList;
    }; 
}
}