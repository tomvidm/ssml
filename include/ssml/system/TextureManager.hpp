#pragma once

#include <string>
#include <map>

#include "graphics/Texture.hpp"
#include "graphics/Tileset.hpp"

namespace ss { namespace system {
    class TextureManager {
    public:
        TextureManager();
        
        void loadTextureFromFile(const std::string& textureAlias, const std::string& filepath);
        void loadTilesetFromJSON(const std::string& tilesetAlias, const std::string& filepath);

        graphics::SharedTexture getTexture(const std::string& textureAlias) const;
        graphics::SharedTileset getTileset(const std::string& tilesetAlias) const;
    private:
        std::string defaultFilepath;
        std::map<std::string, graphics::SharedTexture> textureMap;
        std::map<std::string, graphics::SharedTileset> tilesetMap;
    };
}
}