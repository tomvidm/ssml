#pragma once

#include <vector>

#include "SFML/Graphics.hpp"

#include "math/Rect.hpp"
#include "math/Transform.hpp"
#include "graphics/Renderable.hpp"
#include "system/RenderLayer.hpp"

namespace ss { namespace system {
    class RenderView 
    : math::Transformable {
    public:
        void render(sf::RenderTarget& target) const;
        void performCulling();
    private:
        math::Rect<float> screenSpaceRect;

        std::vector<RenderLayer*> renderLayers;
        std::vector<bool> visibleLayers;
        std::vector<graphics::Renderable*> cullingList;
    };
}
}