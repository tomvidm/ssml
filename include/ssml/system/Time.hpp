#pragma once

#include "SFML/System/Time.hpp"
#include "SFML/System/Clock.hpp"

#include "math/ClampedValue.hpp"

namespace ss { namespace system {
    using sf::Time;
    using sf::seconds;
    using sf::milliseconds;
    using sf::microseconds;
    using sf::Clock;
}
}