#pragma once

#include <string>
//#include <boost/filesystem.hpp>

#include "system/Time.hpp"

namespace ss { namespace system {
    //! Setup config for an engine instance
    /*! To make the SSML engine flexible, configuration should be loaded
        at runtime instead of hardcoded at compile time. This class loads
        configurations from a config file, ready to be fed into the
        Engine constructor.

        Currently, only JSON is supported because rapidjson is just too convenient.
    */
    class EngineConfig {
    public:
        EngineConfig() = delete;
        EngineConfig(
            const unsigned winWidth,
            const unsigned winHeight,
            const unsigned frameLimit,
            const std::string& resSearchPath
        );

        //! Loads the config from a path relative to working directory
        static EngineConfig loadFromJSON(const std::string& pathToConfig);

        inline unsigned getWindowWidth() const { return windowWidth; }
        inline unsigned getWindowHeight() const { return windowHeight; }
        inline unsigned getFrameRateLimit() const { return frameRateLimit; }
        inline system::Time getFrameInterval() const { return system::milliseconds(1000 / frameRateLimit); }
        inline const std::string& getResourceSearchPath() const { return resourceSearchPath; }
    private:
        const unsigned windowWidth;
        const unsigned windowHeight;
        const unsigned frameRateLimit;
        const std::string resourceSearchPath; // Deprecate to give room for boost::filesystem
        //const boost::filesystem::path resourceDirectory;
    };
}
}