#pragma once

#include <vector>
#include <string>
#include <map>
#include <unordered_map>

#include "rapidjson/filereadstream.h"
#include "rapidjson/document.h"

#include "graphics/Texture.hpp"
#include "animation/SpriteAnimation.hpp"
#include "math/Rect.hpp"
#include "system/Time.hpp"

namespace ss { namespace graphics {
    using math::Rect;
    
    //! Tileset class containing static sprites and animations
    /*! The Tileset class stores texture rects for static sprites and
        animations. It also holds string mappings for accessing sprites and
        animations based on their string alias. This should be used instead of
        standalone textures, as SSML renderables are designed to use shared resources.
    */
    class Tileset {
    public:
        Tileset();
        
        //! Load sprites and animations from a JSON file
        /*! This method loads the information stored in a JSON file in the path provided.
            The format of the JSON input must strictly follow certain formats. Currently,
            only the following three formats are supported.

            Format 1 requires the subsprites to be uniformly sized and spaced,
            but allows for concise enumeration of animated sprites.
            \code{.json}
{
    "texture_file": "path/to/texture.png",
    "tileset_format": "regular",
    "subsprite_spacing_x": 0,
    "subsprite_spacing_y": 0,
    "subsprite_size_x": 64,
    "subsprite_size_y": 64,
    "subsprites": [
        {
            "type": "single",
            "alias": "idle_up",
            "tile": [8, 0]
        },
        {
            "type": "animation",    
            "alias": "walk_up",
            "tiles": [
                [8, 1, 100],
                [8, 2, 100],
                [8, 3, 100],
                [8, 4, 100]
            ]
        }
    ]
}
            \endcode
            The values enclosed in square brackets (such as [8, 1, 100]) are, respectively, 
            the row, the column and the duration (in millisecond) of frames. Those without
            a duration are static sprites.

            Format 2 is suitable for non-animated subsprite, and can be of any size.
            \code{.json}
{
    "texture_file": "tiles_spritesheet.png",
    "tileset_format": "irregular",
    "subsprites": [
        {
            "name": "box",
            "x": 0,
            "y": 864,
            "width": 70,
            "height": 70
        },
        {
            "name": "boxAlt",
            "x": 0,
            "y": 792,
            "width": 70,
            "height": 70
        }
    ]
}
            \endcode
            This format is for loading spritesheets for SpriteText usage.
            \code{.json}
{
    "texture_file": "spritetext_spritesheet.png",
    "tileset_format": "spritetext",
    "subsprites": {
        "a": {
            "x": 0,
            "y": 0,
            "width": 32,
            "height": 32
        },
        "b": {
            "x": 0,
            "y": 0,
            "width": 32,
            "height": 32
        }
    }
}
            \endcode
        */
        void loadFromJSON(const std::string filename);

        //! Returns an texture rect mapped to by the provided string alias
        const Rect<float> getTextureRect(const std::string& alias) const;
        //! Returns the texture rect with the given index
        const Rect<float> getTextureRect(const size_t index) const;

        //! Returns an animation mapped to by the provided string alias
        const animation::SpriteAnimation& getAnimation(const std::string& alias) const;

        //! Returns the animation with the given index
        const animation::SpriteAnimation& getAnimation(const size_t index) const;

        inline const size_t getNumSubsprites() const { return textureRects.size(); }
        inline const size_t getNumAnimations() const { return animations.size(); }

        SharedTexture getSharedTexture();
        Texture* getTexturePtr();
    private:
        void loadFormatA(const rapidjson::Document& doc);
        void loadFormatB(const rapidjson::Document& doc);

        SharedTexture spritesheet;
        std::map<std::string, size_t> idToSpriteIndexMap;
        std::map<std::string, size_t> idToAnimationIndexMap;
        std::vector<Rect<float>> textureRects;
        std::vector<animation::SpriteAnimation> animations;

    };

    using SharedTileset = std::shared_ptr<Tileset>;
}
}