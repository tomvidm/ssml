#pragma once

#include <memory>

#include "SFML/Graphics/Texture.hpp"

namespace ss { namespace graphics {
    using sf::Texture;
    using SharedTexture = std::shared_ptr<Texture>;
}
}