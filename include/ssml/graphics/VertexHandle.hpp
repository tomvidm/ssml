#pragma once

#include "graphics/Vertex.hpp"
#include "math/Vector2.hpp"
#include "math/Transform.hpp"

namespace ss { namespace graphics {
    //! A helper class for dealing with vertices in VertexArrays and VertexBuffers.
    /*! This template class serves as a helper for dealing with vertex transformations
        in vertex arrays. Each handle stores the original state of the vertices it covers,
        so that multiple applications of the transform does not apply multiplicatively.

        TODO: What if the owner of the vertices are deleted? How to handle dangling VertexHandles?
    */
    template <unsigned N>
    class VertexHandle {
    public:
        VertexHandle() = delete;
        VertexHandle(Vertex* const vertexPtr);
        void applyTransform(const math::Transform& transform);
        void setPosition(const math::Vector2f vec);
        void setColor(const Color color);
        size_t getNumVertices() const;

        VertexHandle operator = (const VertexHandle<N>& other);
    private:
        Vertex* vertexPtr;
        Vertex untransformedVertices[N];
    };

    template <unsigned N>
    VertexHandle<N>::VertexHandle(Vertex* vertexPtr)
    : vertexPtr(vertexPtr) {
        for (size_t i = 0; i < N; i++) {
            untransformedVertices[i] = vertexPtr[i];
        }
    }

    template <unsigned N>
    void VertexHandle<N>::applyTransform(const math::Transform& transform) {
        for (size_t i = 0; i < N; i++) {
            vertexPtr[i].position = transform.transformPoint(untransformedVertices[i].position);
        }
    }

    template <unsigned N>
    void VertexHandle<N>::setColor(const Color color) {
        for (size_t i = 0; i < N; i++) {
            vertexPtr[i].color = color;
        }
    }

    template <unsigned N>
    size_t VertexHandle<N>::getNumVertices() const {
        return N;
    }

    template <unsigned N>
    VertexHandle<N> VertexHandle<N>::operator = (const VertexHandle<N>& other) {
        vertexPtr = other.vertexPtr;
        for (size_t i = 0; i < N; i++) {
            untransformedVertices[i] = other.untransformedVertices[i];
        }
        return *this;
    }

    using QuadHandle = VertexHandle<4>;
    using HexHandle = VertexHandle<8>;
    using HexOutlineHandle = VertexHandle<24>;
}
}