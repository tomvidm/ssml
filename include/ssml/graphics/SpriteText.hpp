#pragma once

#include <array>
#include <map>
#include <string>
#include <vector>

#include "SFML/Graphics.hpp"

#include "graphics/Vertex.hpp"
#include "graphics/VertexHandle.hpp"
#include "graphics/Renderable.hpp"
#include "graphics/Tileset.hpp"
#include "graphics/SpriteFont.hpp"
#include "math/Rect.hpp"
#include "math/VectorArithmetic.hpp"

namespace ss { namespace graphics {
    struct SpriteTextStyle {
        math::Vector2f charSize;
        float charSpacing;
        float lineSpacing;

        static SpriteTextStyle default_style;
    };

    //! Draw text using sprites.
    class SpriteText 
    : public Renderable {
    public:
        SpriteText();
        SpriteText(const std::string& string, const SpriteTextStyle& style);

        void setSpriteFont(const std::shared_ptr<SpriteFont>& newSpriteFont);
        void setString(const std::string& newstring);
        inline std::string getString() const { return string; }

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        virtual const math::Rect<float> getLocalBounds() const;
        virtual const math::Rect<float> getGlobalBounds() const;
    private:
        void rebuildVertices();

        std::string string;
        std::vector<Vertex> vertices;

        Tileset tileset;
        VertexBuffer vbuf;

        std::shared_ptr<SpriteFont> spriteFont;
        SpriteTextStyle style;

        math::Rect<float> localBounds;
    };
}
}