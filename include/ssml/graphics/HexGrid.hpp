#pragma once

#include <vector>
#include <memory>

#include "SFML/Graphics.hpp"

#include "math/Vector2.hpp"
#include "math/VectorArithmetic.hpp"
#include "math/LinearInterpolation.hpp"
#include "math/Transform.hpp"
#include "math/Heightmap.hpp"
#include "math/Hex.hpp"
#include "math/HexConfiguration.hpp"
#include "graphics/Texture.hpp"
#include "graphics/Vertex.hpp"
#include "graphics/VertexHandle.hpp"
#include "graphics/Renderable.hpp"
#include "graphics/HexBuilder.hpp"

namespace ss { namespace graphics {
    //! Simple class for rendering hexagonal grids
    /*! This class renders a hexagonal grid based on some hex configuration.
        The configuration is done at initialization, by providing a HexConfig
        to the constructor.
    */
    class HexGrid
    : public graphics::Renderable {
    public:
        HexGrid() = delete;
        HexGrid(const size_t usize,    
                const size_t vsize, 
                const math::HexConfiguration config);

        //! Set the color of the given hex
        void setHexColor(const math::Hex<int>& hex,
                         const Color color);
        //! Set the color of the hex with u,v coordinates
        void setHexColor(const size_t u,
                         const size_t v,
                         const Color color);

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        virtual const math::Rect<float> getLocalBounds() const;
        virtual const math::Rect<float> getGlobalBounds() const;

        //! Return a copy of the hex configuration
        inline math::HexConfiguration getConfig() const { return config; }

        math::Hex<int> getHexFromGlobal(const math::Vector2f global) const;
        math::Hex<int> getHexFromLocal(const math::Vector2f local) const;

        HexHandle getHexHandle(const size_t u, const size_t v);

        inline size_t getSizeU() const { return usize; }
        inline size_t getSizeV() const { return vsize; }

    private:
        void updateHexes();
        void updateHex(const size_t u, size_t v);
        void buildHexes();

        Vertex* const getPointerToHexFromUV(const size_t u, const size_t v);

        const size_t usize;
        const size_t vsize;
        const math::HexConfiguration config;

        std::vector<Vertex> vertices;
        std::vector<HexHandle> hexHandles;
        VertexBuffer vertexBuffer;
    };
}
}