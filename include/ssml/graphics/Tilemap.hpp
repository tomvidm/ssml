#pragma once

#include "graphics/QuadGrid.hpp"
#include "graphics/Tileset.hpp"

namespace ss { namespace graphics {
    struct Tile {
        size_t tilesetIndex;
    };

    //! Tilemaps for rendering... Tiles?
    /*! This class allows a simple interface for working with, and rendering Tilemaps. The Tilemap
        class is built on top of the QuadArray class. The Tilemap needs a Tileset to work, and uses its internal tile data 
        to obtain texture rects from the associated Tileset. */
    class Tilemap 
    : public graphics::QuadGrid {
    public:
        //! Construct a Tileset with the specified amount of rows and columns, and uses the provided Tileset.
        Tilemap(const size_t rows, const size_t cols, const math::Vector2f tileSize, SharedTileset tileset);

        void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        //! Set a Tile to whatever. Funny things happen if the Tile does not exist.
        void setTile(const size_t row, const size_t col, const Tile tile);
    protected:
        SharedTileset tileset;

        std::vector<Tile> tileData;
    };

    using SharedTilemap = std::shared_ptr<Tilemap>;
}
}