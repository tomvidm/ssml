#pragma once

#include "graphics/VertexHandle.hpp"
#include "math/HexConfiguration.hpp"

namespace ss { namespace graphics {
    //! A helper class for initializing the vertices, based on a center and a hex configuration.
    /*! This class provides simple static methods for initializing vertices, given a pointer to
        the vertices to initialize. In addition to initializing the vertices, the methods also
        returns appropriate handles, so that the user can store them and easily manipulate the vertices,
        without accessing the containing object itself.
    */
    class HexBuilder {
    public:
        static HexHandle buildHex(
            Vertex* const vertexPtr, 
            const math::Vector2f center, 
            const math::HexConfiguration& config,
            const float scale = 1.f);

        static HexOutlineHandle buildHexOutline(
            Vertex* const vertexPtr,
            const math::Vector2f center, 
            const math::HexConfiguration& config,
            const float thickness);                                             
    };
}
}