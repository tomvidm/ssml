#pragma once

#include "SFML/Graphics/Vertex.hpp"
#include "SFML/Graphics/VertexArray.hpp"
#include "SFML/Graphics/VertexBuffer.hpp"

namespace ss { namespace graphics {
    using sf::Vertex;
    using sf::VertexArray;
    using sf::VertexBuffer;
    using sf::Color;
}
}