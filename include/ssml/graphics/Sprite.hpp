#pragma once

#include <memory>

#include "animation/SpriteAnimation.hpp"
#include "animation/SpriteAnimationController.hpp"
#include "math/Transform.hpp"

#include "graphics/Renderable.hpp"
#include "graphics/SpriteBase.hpp"

namespace ss { namespace graphics {
    //! A sprite that supports sprite animations
    /*! The Sprite class represents a rectangular sprite and provides
        a simple interface for assigning either static graphics or sprite animations.
    */
    class Sprite 
    : public SpriteBase, 
      public Renderable {
    public:
        using shared = std::shared_ptr<Sprite>;
    public:
        Sprite();
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        //! Updates the sprite if animated
        /*! If the sprite is animated, this methods must be called with a time object
            to update the animation. The time is passed to the underlying SpriteAnimationController
            which will tell the Sprite whether or not to update its graphics.
        */
        void update(const system::Time& dt);
        void setAnimation(const animation::SpriteAnimation& animation);
        void setStaticSprite(const math::Rect<float> textureRect);

        virtual const math::Rect<float> getLocalBounds() const;
        virtual const math::Rect<float> getGlobalBounds() const;

        //! Returns a const reference to the underlying SpriteAnimationController
        inline const animation::SpriteAnimationController& getAnimationController() const { return animationController; }
    private:
        animation::SpriteAnimationController animationController;
        bool isAnimated;
    };
}
}