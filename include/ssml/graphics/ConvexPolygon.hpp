#pragma once

#include <memory>

#include "SFML/Graphics.hpp"

#include "math/ConvexPolygon.hpp"
#include "graphics/Renderable.hpp"

namespace ss { namespace graphics {
    class ConvexPolygon
    : public Renderable {
    public:
        using shared = std::shared_ptr<ConvexPolygon>;
        ConvexPolygon() = delete;
        ConvexPolygon(const math::ConvexPolygon& polygon);

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        void setPolygon(const math::ConvexPolygon& polygon);

        virtual const math::Rect<float> getLocalBounds() const;
        virtual const math::Rect<float> getGlobalBounds() const;
    private:
        sf::VertexArray mesh;
        math::ConvexPolygon _polygon;
    };
}
}