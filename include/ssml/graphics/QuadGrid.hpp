#pragma once

#include <vector>
#include <memory>

#include "SFML/Graphics.hpp"

#include "math/Vector2.hpp"
#include "math/Transform.hpp"
#include "math/Heightmap.hpp"
#include "graphics/Texture.hpp"
#include "graphics/Vertex.hpp"
#include "graphics/Renderable.hpp"

namespace ss { namespace graphics {
    struct QuadGridStyle {
        math::Vector2f tileSize;
    };

    class QuadGridConfig {
    private:
        math::Vector2f tileSize;
        math::Vector2f spacing;
    };

    //! A grid of regularly spaced rectangled.
    /*! This class is the basis for Tilemaps. Each tile is referred to as a quad,
        and each quad can be individually assigned a color and a texture rect.
    */
    class QuadGrid
    : public graphics::Renderable {
    public:
        QuadGrid(const size_t rows, const size_t cols, const QuadGridStyle& style);

        // Inherited from Renderable
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        const math::Rect<float> getLocalBounds() const;
        const math::Rect<float> getGlobalBounds() const;

        // Modify quads
        void paintByHeightmap(const math::Heightmap& hmap);

        //! Set the color of the quad given an tile coordinate
        void setQuadColor(const math::Vector2i coord, const Color color);
        void setQuadColor(const size_t row, const size_t col, const Color color);
        //! Set the texture rect for the tile given the row-column coordinate
        void setQuadTextureRect(const size_t row, 
                                const size_t col, 
                                const math::Rect<float> texRect);
        void setQuadColorAndTextureRect(
            const size_t row, const size_t col,
            const Color color, math::Rect<float> texRect);

        void setQuadColorNoUpdate(const math::Vector2i coord, const Color color);
        void setQuadColorNoUpdate(const size_t row, const size_t col, const Color color);
        void setQuadTextureRectNoUpdate(const size_t row, 
                                const size_t col, 
                                const math::Rect<float> texRect);
        void setQuadColorAndTextureRectNoUpdate(
            const size_t row, const size_t col,
            const Color color, math::Rect<float> texRect);

        //! Returns the tile coordinate given a point in global space
        math::Vector2i getCoordByGlobal(const math::Vector2f globalPoint) const;
        //! Returns the tile coordinate given a point in local space
        math::Vector2i getCoordByLocal(const math::Vector2f localPoint) const;

        //! Convert the row-column coordinate to the index of the quad.
        size_t coordToIndex(const size_t row, const size_t col) const;
        //! Convert the x-y coordinate to the index of the quad.
        size_t coordToIndex(const math::Vector2i coord) const;
    protected:
        void buildQuads();
        void buildQuad(const size_t row, const size_t col);
        void updateQuad(const size_t row, const size_t col);
        void updateQuads(const size_t r0, const size_t r1,
                         const size_t c0, const size_t c1);
        void updateQuads();
        Vertex* getQuadPointer(const size_t row, const size_t col);

        const size_t rows;
        const size_t columns;
        const QuadGridStyle style;
        SharedTexture texture;

        std::vector<Vertex> vertices;
        VertexBuffer vertexBuffer;
    };

    using SharedQuadGrid = std::shared_ptr<QuadGrid>;
}
}