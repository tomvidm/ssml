#pragma once

#include "SFML/Graphics.hpp"

#include "graphics/Vertex.hpp"
#include "graphics/Texture.hpp"
#include "graphics/Tileset.hpp"
#include "math/Rect.hpp"
#include "math/Vector2.hpp"

namespace ss { namespace graphics {
    //! The basic sprite class, from which Sprite derives.
    /*! This class stores the vertices of a sprite, a pointer to the used texture,
        its current texture rect and local bounds.
    */
    class SpriteBase {
    public:
        SpriteBase();
        //! Sets the current texture used in any draw call
        void setTexture(SharedTexture sharedTexture);

        //! Sets the current texture, based on the Tileset provided
        void setTextureFromTileset(Tileset& tileset);
        void setTextureRect(const math::Rect<float>& rect);
        void setSize(const math::Vector2f& size);

        math::Rect<float> getTextureRect() const;
    protected:
        Vertex vertices[4];
        SharedTexture texture;
        math::Rect<float> localBounds;
    private:
        math::Rect<float> textureRect;
    };
}
}