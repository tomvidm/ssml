#pragma once

#include <memory>

#include "SFML/Graphics/Drawable.hpp"

#include "math/Transform.hpp"
#include "math/Rect.hpp"

namespace ss { namespace graphics {
    //! An abstract class representing renderable objects
    /*! The Renderable class is an abstract class that repersents any object
        that is renderable and transformable. It is derived from the SFML classes
        sf::Drawable and sf::Transformable, but also provides methods for obtaining
        the local and global bounds of the object. This is useful for letting the
        renderer handle culling.
    */
    class Renderable
    : public math::Transformable,
      public sf::Drawable {
    
    public:
        //! Returns the local bounds of the Renderable
        virtual const math::Rect<float> getLocalBounds() const = 0;
        //! Returns the global bounds of the Renderable
        virtual const math::Rect<float> getGlobalBounds() const = 0;
    };

    using SharedRenderable = std::shared_ptr<Renderable>;
}
}