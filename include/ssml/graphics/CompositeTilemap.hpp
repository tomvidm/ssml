#pragma once

#include <vector>
#include <unordered_map>

#include "SFML/Graphics.hpp"

#include "graphics/Tilemap.hpp"
#include "math/Rect.hpp"
#include "math/Vector2.hpp"

namespace ss { namespace graphics {
//! This class provides an interface for aligning and interacting with arbitrarily sized Tilemaps
/*! CompositeTilemap enables arbitrarily sized tilemaps, by storing shared
    pointers to Tilemap instances in a hash map, mapped to by integer vector
    coordinates. It maintains a list of visible Tilemaps, which can be updated
    by the updateVisibility method.
    A Tile coordinate refers to the integer xy coordinate of some tile.
    A Tilemap coordinate refers to the integer xy coordinate of a Tilemap.
    A local coordinate is a position relative to the origin.
    A global coordinate is an absolute position.
*/
    class CompositeTilemap 
    : public Renderable {
    public:
        //! Constructor
        /*! Constructs a CompositeTilemap with the provided configuration.*/
        CompositeTilemap(
            const size_t rowsPerTilemap,
            const size_t colsPerTilemap,
            const math::Vector2f tileSize);
        //! Draw to render target
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        const math::Rect<float> getLocalBounds() const;
        const math::Rect<float> getGlobalBounds() const;

        //! Returns tile coordinate given a position in local space
        math::Vector2i getTileCoordByLocal(const math::Vector2f local) const;
        //! Returns a Tile coordinate given a position in global space
        math::Vector2i getTileCoordByGlobal(const math::Vector2f global) const;
        //! Returns the Tilemap coordinate given a local position
        math::Vector2i getTilemapCoordByLocal(const math::Vector2f localPoint) const;
        //! Returns the Tilemap coordinate given a global position
        math::Vector2i getTilemapCoordByGlobal(const math::Vector2f globalPoint) const;
        //! Returns the Tilemap coordinate given a Tile coordinate
        math::Vector2i getTilemapCoordByTile(const math::Vector2i tileCoord) const;
        //! Returns a SharedTilemap from a local position
        SharedTilemap getTilemapByLocal(const math::Vector2f localPoint) const;
        //! Returns a SharedTilemap from a global position
        SharedTilemap getTilemapByGlobal(const math::Vector2f globalPoint) const;
        //! Returns a SharedTilemap from a Tile coordinate
        SharedTilemap getTilemapByTile(const math::Vector2i tileCoord) const;

        //! Updates the visibility list
        /*! Uses the provided Rect (in world space coordinates) to
            add all Tilemaps that it covers to the visibility list.*/
        void updateVisibility(math::Rect<float> visbilityRect);

        math::Rect<int> coordinatesCoveredBy(math::Rect<float> rect) const;

        bool addTilemap(const math::Vector2i coord, SharedTilemap sharedTilemap);
        bool removeTilemap(const math::Vector2i coord);

        inline math::Vector2u getTilemapSize() const { return math::Vector2u(rowsPerTilemap, colsPerTilemap); }
        inline math::Vector2f getTilSize() const { return tileSize; }
    private:
        const size_t rowsPerTilemap;
        const size_t colsPerTilemap;
        const math::Vector2f tileSize;
        std::vector<SharedTilemap> visibilityList;
        std::unordered_map<math::Vector2i, SharedTilemap> tilemapCache;
    };
}
}