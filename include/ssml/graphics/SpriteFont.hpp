#pragma once

#include <map>

#include "math/Rect.hpp"

namespace ss { namespace graphics {
    class SpriteFont {
    public:
        void addTexRect(const char c, const math::Rect<float>& rect);
        math::Rect<float> getTexRect(const char c) const;
    private:
        std::map<char, math::Rect<float>> charmap;
    };
}
}