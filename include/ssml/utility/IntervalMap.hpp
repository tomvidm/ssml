#pragma once

#include <map>
#include <utility>
#include <limits>

namespace ss { namespace utility {
    template <typename K, typename V>
    class IntervalMap {
    public:
        IntervalMap(const V& val);

        const V& operator[] (const K& key) const;
        void assign(const K& lowerBound, const K& upperBound, const V& val);
        inline size_t numIntervals() const { return binTree.size(); }
    private:
        std::map<K, V> binTree;
    };

    template <typename K, typename V>
    IntervalMap<K, V>::IntervalMap(const V& val) {
        binTree.insert(std::make_pair(std::numeric_limits<K>::lowest(), val));
    }

    template <typename K, typename V>
    const V& IntervalMap<K, V>::operator[] (const K& key) const {
        return (*--binTree.upper_bound(key)).second;
    }

    template <typename K, typename V>
    void IntervalMap<K, V>::assign(const K& lowerBound, const K& upperBound, const V& val) {
        // Assign values to lower and upper bounds
        // Use these bounds to remove all entries between.
        // Make sure the value assigned to upper bound, is the same as the
        // --upper_bound of upperBound key.
    }
}
}