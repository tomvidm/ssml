/*! \file */

/*! \namespace ss
    \brief The top level namespace for SSML
*/
namespace ss {
    /*! \namespace math 
        \brief The namespace containing purely mathematical functions. 
    */
    namespace math {}

}