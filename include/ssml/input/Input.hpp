#pragma once

#include "input/Mouse.hpp"
#include "input/Keyboard.hpp"

namespace ss { namespace input {
    enum class InputEventType : size_t {
        Mouse,
        Keyboard,
        None
    };
    
    //! A simple data structure holding information about input events.
    /*! When an SFML event is passed to the InputHandler, an InputEvent object
        is returned. 

        Although both the InputEvent and the INputHandler can be queried to determine if
        a button is held, their purposes are different. Upon reception of an SFML event,
        the current state of the state machine is changed, and the InputEvent returned is
        based on what changed.
        
        ISSUE: Because the structure, at any depth, does not contain references or pointers,
        a union is used. However, a future improvement may invovle switching over to boost::variant
        to reduce the amount of boiler plate, should any changes be needed.
    */
    class InputEvent {
    public:
        InputEvent();
        InputEvent(const InputEvent& other);
        InputEvent(const MouseEvent& mouseEvent);
        InputEvent(const KeyboardKeyEvent& keyboardKeyEvent);
        ~InputEvent();

        bool isLeftClick() const;
        
        InputEvent operator=(const InputEvent& other);
        inline InputEventType getType() const { return type; }
        inline MouseEvent getMouseEvent() const { return mouseEvent; }
        inline KeyboardKeyEvent getKeyboardKeyEvent() const { return keyboardKeyEvent; }


        bool operator== (const InputEvent& other) const;
    protected:
        InputEventType type;

        union {
            MouseEvent mouseEvent;
            KeyboardKeyEvent keyboardKeyEvent;
        };
    };

    //! Main interface for the input system
    /*! The input handler keeps track of the state of all inputs that the window has received.
        These states are encapsulated in internal state objects such as MouseState and KeyboardState.
        Any received SFML sf::Event objects passed to the onEvent method will return an InputEvent,
        containing a bit more details than the normal SFML events.
    */
    class InputHandler {
    public:
        InputEvent onEvent(const sf::Event& sfmlEvent);

        const MouseState& getMouseState() const;

        bool isKeyboardKeyPressed(const input::KeyboardKey key) const;
        bool isMouseButtonPressed(const input::Button button) const;
        system::Time getMouseButtonHoldTime(const input::Button button) const;
    private:
        MouseState mouseState;
        KeyboardState keyboardState;
    };
}
}
