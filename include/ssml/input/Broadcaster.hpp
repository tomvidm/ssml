#pragma once

#include "input/Input.hpp"
#include "input/Listener.hpp"

namespace ss {
    class Broadcaster {
    public:
        void attachListener(const InputEvent& event, std::shared_ptr<Listener>& listener);
    private:
        std::multimap<InputEvent, std::shared_ptr<Listener>> event2listeners;
        std::multimap<std::shared_ptr<Listener>, InputEvent> listener2events;
    };
}