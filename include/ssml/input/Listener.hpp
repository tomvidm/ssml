#pragma once

#include <map>

#include "input/Input.hpp"

namespace ss { namespace input {
    //! Abstract class representing an event listener
    class Listener {
    public:
        virtual void onEvent(const InputEvent& event) = 0;
    };

    class Broadcaster {
    public:
        void attachListener(const std::shared_ptr<Listener> listener);
        void detachListener(const std::shared_ptr<Listener> listener);
        void broadcastEvent(const InputEvent& event);
    private:
    };
}
}