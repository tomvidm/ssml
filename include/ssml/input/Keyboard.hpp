#pragma once

#include "SFML/Window.hpp"

#include "math/Vector2.hpp"
#include "system/Time.hpp"

namespace ss { namespace input {
    using KeyboardKey = sf::Keyboard::Key;

    enum class KeyboardKeyEventType : size_t {
        Pressed,
        Holding,
        Released,
        None
    };

    class KeyboardKeyEvent {
    public:
        KeyboardKeyEvent();
        KeyboardKeyEvent(const KeyboardKeyEventType type, KeyboardKey key);
        KeyboardKeyEvent(const KeyboardKeyEvent& other);
        ~KeyboardKeyEvent();

        KeyboardKeyEvent operator=(const KeyboardKeyEvent& other);
        bool operator==(const KeyboardKeyEvent& other) const;

        inline KeyboardKeyEventType getType() const { return type; }
        inline KeyboardKey getKey() const { return key; }
    protected:
        KeyboardKeyEventType type;
        KeyboardKey key;
    };

    class KeyboardKeyState {
    public:
        KeyboardKeyState();

        KeyboardKeyState operator=(const KeyboardKeyState& other);

        KeyboardKeyEvent onEvent(const sf::Event& sfmlEvent);
        KeyboardKeyEvent onKeyPressEvent(const sf::Event& sfmlEvent);
        KeyboardKeyEvent onKeyReleaseEvent(const sf::Event& sfmlEvent);

        inline system::Time getKeyHoldTime(const KeyboardKey key) const {
            return isKeyPressed ? lastPressTimer.getElapsedTime() : system::milliseconds(0);
        }
        inline bool isPressed() const { return isKeyPressed; }
    protected:
        bool isKeyPressed;
        system::Clock lastPressTimer;
        system::Clock lastReleaseTimer;
    };

    class KeyboardState {
    public:
        KeyboardKeyEvent onEvent(const sf::Event& sfmlEvent);

        const KeyboardKeyState getKeyState(KeyboardKey key) const;
    private:
        KeyboardKeyState keyStates[static_cast<size_t>(KeyboardKey::KeyCount)];
    };
}
}