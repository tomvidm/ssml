#pragma once

#include "SFML/Window.hpp"

#include "math/Vector2.hpp"
#include "system/Time.hpp"

namespace ss { namespace input {
    enum class Button : size_t {
        Left,
        Right,
        Middle,
        NumButtons,
        None
    };

    enum class WheelDirection : size_t {
        Up,
        Down,
        None
    };

    enum class ButtonEventTrigger : size_t {
        Press,
        Release,
        None
    };

    enum class ButtonEventType : size_t {
        Press,
        Release,
        Click,
        DoubleClick,
        None
    };

    enum class MouseEventType : size_t {
        Button,
        Move,
        Wheel,
        None
    };

    class ButtonEvent {
    public:
        ButtonEvent();
        ButtonEvent(
            const Button button, 
            const ButtonEventTrigger trigger, 
            const ButtonEventType type,
            const math::Vector2f cursorPosition);
        ButtonEvent(const ButtonEvent& other);
        ~ButtonEvent();

        ButtonEvent operator=(const ButtonEvent& other);
        bool operator==(const ButtonEvent& other) const;

        inline Button getButton() const { return button; }
        inline ButtonEventTrigger getTrigger() const { return trigger; }
        inline ButtonEventType getType() const { return type; }
        inline math::Vector2f getPosition() const { return cursorPosition; }
    protected:
        Button button;
        ButtonEventTrigger trigger;
        ButtonEventType type;
        math::Vector2f cursorPosition;
    };

    class ButtonState {
    public:
        ButtonState();

        ButtonEvent onEvent(const sf::Event& sfmlEvent);
        ButtonEvent onPress(const sf::Event& sfmlEvent);
        ButtonEvent onRelease(const sf::Event& sfmlEvent);

        inline math::Vector2f getPositionOfLastPress() const { return positionOfLastPress; }
        inline math::Vector2f getPositionOfLastRelease() const { return positionOfLastRelease; }
        inline system::Time getTimeSinceLastPress() const { return lastPressTimer.getElapsedTime(); }
        inline system::Time getTimeSinceLastRelease() const { return lastReleaseTimer.getElapsedTime(); }
        inline system::Time getHoldTime() const { return isButtonPressed ? lastPressTimer.getElapsedTime() : system::milliseconds(0); }        inline bool isPressed() const { return isButtonPressed; }
    private:
        bool isButtonPressed;
        math::Vector2f positionOfLastPress;
        math::Vector2f positionOfLastRelease;
        system::Clock lastPressTimer;
        system::Clock lastReleaseTimer;
        system::Time clickTimeThreshold;
        ButtonEventType lastTriggeredEventType;
    };

    class MoveEvent {
    public:
        MoveEvent();
        MoveEvent(const math::Vector2f pos);
        MoveEvent(const MoveEvent& other);
        ~MoveEvent();

        MoveEvent operator=(const MoveEvent& other);
        bool operator==(const MoveEvent& other) const;

        inline math::Vector2f getPosition() const { return position; }
    protected:
        math::Vector2f position;
    };

    class WheelEvent {
    public:
        WheelEvent();
        WheelEvent(const WheelDirection direction);
        WheelEvent(const WheelEvent& other);
        ~WheelEvent();

        WheelEvent operator=(const WheelEvent& other);
        bool operator==(const WheelEvent& other) const;

        inline WheelDirection getDirection() const { return direction; }
    protected:
        WheelDirection direction;
    };

    class MouseEvent {
    public:
        MouseEvent();
        MouseEvent(const ButtonEvent& buttonEvent);
        MouseEvent(const MoveEvent& moveEvent);
        MouseEvent(const WheelEvent& wheelEvent);
        MouseEvent(const MouseEvent& other);
        ~MouseEvent();

        MouseEvent operator=(const MouseEvent& other);
        bool operator==(const MouseEvent& other) const;

        inline MouseEventType getType() const { return type; }
        inline ButtonEvent getButtonEvent() const { return buttonEvent; }
        inline MoveEvent getMoveEvent() const { return moveEvent; }
        inline WheelEvent getWheelEvent() const { return wheelEvent; }
    public:
        MouseEventType type;

        union {
            ButtonEvent buttonEvent;
            MoveEvent moveEvent;
            WheelEvent wheelEvent;
        };
    };

    //! A class that manages the state of the mouse
    /*  This class keeps track of the state of the mouse. However, the internal states are
        only changed on calling onEvent method with a valid SFML sf::Event object.
    */
    class MouseState {
    public:
        MouseState();

        //! Update the MouseState and return a MouseEvent
        MouseEvent onEvent(const sf::Event& sfmlEvent);

        //! Get a reference to the state of a mouse button
        const ButtonState& getButtonState(const Button button) const;
        
        //! Return how far the mouse has moved while holding a button
        /*! This methods returns how far the mouse has moved, while holding the button
            corresponding to the argument. If button is not held, it returns a zero vector. */
        math::Vector2f getDragDelta(const Button button) const;

        //! Return the moues position in float coordinates
        inline math::Vector2f getPosition() const { return lastRegisteredPosition; }

        //! Return the mouse position in pixel coordinates
        inline math::Vector2i getPixelPosition() const { return lastRegisteredPixelPosition; }
    private:
        math::Vector2f lastRegisteredPosition;
        math::Vector2i lastRegisteredPixelPosition;
        ButtonState buttons[static_cast<size_t>(Button::NumButtons)];
    };
}
}