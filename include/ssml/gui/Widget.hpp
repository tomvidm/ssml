#pragma once

#include <memory>

#include "graphics/Renderable.hpp"
#include "input/Listener.hpp"

namespace ss { namespace gui {
    class Widget
    : graphics::Renderable,
      input::Listener {
    public:
        virtual void onEvent(const input::InputEvent& event) = 0;

        virtual const math::Rect<float> getLocalBounds() const = 0;
        virtual const math::Rect<float> getGlobalBounds() const = 0;

        void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
    };

    using SharedWidget = std::shared_ptr<Widget>;
}
}