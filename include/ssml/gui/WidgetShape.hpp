#pragma once

#include <vector>

#include "graphics/Vertex.hpp"

#include "graphics/Renderable.hpp"

namespace ss { namespace gui {
    class WidgetShape
    {//: graphics::Renderable {
    public:
        WidgetShape();
        //virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    private:
        graphics::VertexBuffer vbuffer;
        std::vector<graphics::Vertex> vertices;
    };
}
}