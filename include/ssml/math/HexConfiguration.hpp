#pragma once

#include <vector>

#include "math/Vector2.hpp"
#include "math/Hex.hpp"
#include "math/Transform.hpp"

namespace ss { namespace math {
    class HexConfiguration {
    public:
        HexConfiguration() = delete;
        HexConfiguration(
            const Vector2f u,
            const Vector2f v,
            const Vector2f w
        );

        Vector2f hexToVector(const Hex<int> hex) const;
        Vector2f hexToVector(const Hex<float> hex) const;
        Hex<float> toHexSpace(const Vector2f vec) const;
        Hex<int> nearestHex(const Vector2f vec) const;
        Hex<int> nearestHex(const Hex<float> hexf) const;
        std::vector<std::pair<float, Hex<int>>> nearestNeighbors(const Vector2f pos) const;

        inline Vector2f getUnitU() const { return unitU; }
        inline Vector2f getUnitV() const { return unitV; }
        inline Vector2f getUnitW() const { return unitW; }

        inline Vector2f getVertexU() const { return vertexU; }
        inline Vector2f getVertexV() const { return vertexV; }
        inline Vector2f getVertexW() const { return vertexW; }

        static HexConfiguration makeFlatTop(
            const float majorWidth,
            const float minorWidth,
            const float height
        );

        static HexConfiguration makePointyTop(
            const float majorHeight,
            const float minorHeight,
            const float width
        );


    private:
        const Vector2f vertexU;
        const Vector2f vertexV;
        const Vector2f vertexW;

        const Vector2f unitU;
        const Vector2f unitV;
        const Vector2f unitW;

        const math::Transform2 uv2xy;
        const math::Transform2 xy2uv;
    };
}
}