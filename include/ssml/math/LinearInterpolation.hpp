#pragma once

namespace ss { namespace math {
    //! A simple generic lerp function.
    template <typename T>
    T lerp(const T& a, const T& b, float t) {
        if (t < 0.f) t = 0.f;
        if (t > 1.f) t = 1.f;
        return a + (t * b);
    }
}
}