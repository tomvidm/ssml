#pragma once

#include <algorithm>
#include <vector>
#include <memory>

#include "math/BilinearInterpolation.hpp"
#include "math/LinearInterpolation.hpp"
#include "math/Vector2.hpp"

namespace ss { namespace math {
    class Heightmap {
    public:
        Heightmap(const size_t rows, const size_t cols, const float value = 0.f);
        Heightmap(
            const Heightmap& quadrant00,
            const Heightmap& quadrant01,
            const Heightmap& quadrant10,
            const Heightmap& quadrant11
        );

        float& get(const size_t r, const size_t c);
        float get(const size_t r, const size_t c) const;
        float& get(const math::Vector2u coord);
        float get(const math::Vector2u coord) const;
        void set(const size_t r, const size_t c, const float value);
        void set(const math::Vector2u coord, const float value);

        float getMinValue() const;
        float getMaxValue() const;
        inline size_t getNumRows() const { return rows; }
        inline size_t getNumCols() const { return cols; }
        inline math::Vector2u getSize() const { return math::Vector2u(rows, cols); }
        void normalize();
        void threshold(const float t);
        void operator += (const Heightmap& other);
        void operator *= (const float factor);
    private:
        const size_t rows;
        const size_t cols;
        float maxValue;
        float minValue;
    protected:
        std::vector<std::vector<float>> values;
    };

    using SharedHeightmap = std::shared_ptr<Heightmap>;
}
}