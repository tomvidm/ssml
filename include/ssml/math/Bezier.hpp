#pragma once

#include "math/Vector2.hpp"

//! \file
namespace ss { namespace math {
    
    math::Vector2f quadraticBezier(
        const math::Vector2f p0,
        const math::Vector2f p1,
        const math::Vector2f p2,
        const float t
    );

    float quadraticBezierUnitBox(
        const math::Vector2f cpoint,
        const float t
    );
}
}