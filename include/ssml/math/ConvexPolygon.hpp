#pragma once

#include <memory>
#include <vector>

#include "math/Vector2.hpp"
#include "math/Rect.hpp"


namespace ss { namespace math {
    const float two_pi = 6.28318530718f;
    
    class ConvexPolygon {
    public:
        using shared = std::shared_ptr<ConvexPolygon>;
        ConvexPolygon() {;}
        ConvexPolygon(std::vector<math::Vector2f> vectors);

        bool containsPoint(const Vector2f point) const;

        Vector2f getFarthestInDirectionOf(const math::Vector2f dir) const;
        Vector2f getVertexVector(const size_t vertexIndex) const;
        Vector2f getEdgeVector(const size_t edgeIndex) const;

        Rect<float> getBoundingBox() const;
        const std::vector<Vector2f> getVertexList() const;
    private:
        std::vector<Vector2f> vertexList;
    };

    ConvexPolygon generateCircle(const size_t numVertices, const float radius);
}
}