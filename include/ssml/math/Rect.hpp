#pragma once

#include "SFML/Graphics/Rect.hpp"

#include "math/Vector2.hpp"

namespace ss { namespace math {
    using sf::Rect;

    template <typename T>
    std::array<Vector2<T>, 4> getCornerVectors(const Rect<T>& rect) {
        return {
            Vector2<T>(rect.left, rect.top),
            Vector2<T>(rect.left + rect.width, rect.top),
            Vector2<T>(rect.left + rect.width, rect.top + rect.height),
            Vector2<T>(rect.left, rect.top + rect.height)
        };
    }
}
}