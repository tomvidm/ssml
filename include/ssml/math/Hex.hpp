#pragma once

#include <cmath>
#include <algorithm>
#include <stdexcept>
#include <functional>

namespace ss { namespace math {
    class InvalidHex : public std::runtime_error {
    public:
        InvalidHex() : std::runtime_error("Invalid hex coordinates!") {;}
    };

    //! The Hex class represents a point in cubic space.
    /*!  
        The Hex is similar to math::Vector2<T>, but has additional
        methods that makes it convenient to use when working with hexagonal
        coordinates. 
    */
    template <typename T>
    class Hex {
    public:
        Hex() : u(T(0)), v(T(0)), w(T(0)) {;}
        Hex(const T u, const T v) : u(u), v(v), w(-(u+v)) {;}
        Hex(const T u, const T v, const T w) : u(u), v(v), w(w) {
            if (u + v + w != 0) {
                throw InvalidHex();
            }
        }
        Hex(const Hex<T>& other) : u(other.u), v(other.v), w(other.w) {;}

        inline T getU() const { return u; }
        inline T getV() const { return v; }
        inline T getW() const { return w; }

        Hex<T> operator = (const Hex<T>& other);

        bool operator == (const Hex<T>& other) const;
        bool operator < (const Hex<T>& other) const;
        
        T u;
        T v;
        T w;
    };

    template <typename T>
    Hex<T> Hex<T>::operator = (const Hex<T>& other) {
        u = other.u;
        v = other.v;
        w = other.w;
        return *this;
    }

    template <typename T>
    bool Hex<T>::operator == (const Hex<T>& other) const {
        return (u == other.u) &&
               (v == other.v) &&
               (w == other.w);
    }

    template <typename T>
    bool Hex<T>::operator < (const Hex<T>& other) const {
        if (u < other.u) {
            return true;
        } else {
            if (v < other.v) {
                return true;
            } else {
                if (w < other.w) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    template <typename T>
    Hex<float> toFloatHex(const Hex<T>& hex) {
        return Hex<float>(
            static_cast<float>(hex.getU()),
            static_cast<float>(hex.getV()),
            static_cast<float>(hex.getW())
        );
    }

    template <typename T>
    Hex<T> operator + (const Hex<T>& lhs, const Hex<T>& rhs) {
        return Hex<T>(lhs.u + rhs.u, lhs.v + rhs.v, lhs.w + rhs.w);
    }

    template <typename T>
    Hex<T> operator - (const Hex<T>& lhs, const Hex<T>& rhs) {
        return Hex<T>(lhs.u - rhs.u, lhs.v - rhs.v, lhs.w - rhs.w);
    }

    template <typename T>
    Hex<T> operator * (const T lhs, const Hex<T>& rhs) {
        return Hex<T>(lhs * rhs.u, lhs * rhs.v, lhs * rhs.w);
    }

    //! Calculate the distance in cubic coordinates between lhs and rhs
    template <typename T>
    T cubeDistance(const Hex<T>& lhs, const Hex<T>& rhs) {
        return std::max({
            abs(lhs.getU() - rhs.getU()),
            abs(lhs.getV() - rhs.getV()),
            abs(lhs.getW() - rhs.getW())
        });
    }
}
}

namespace std {
    template <typename T>
    struct hash<ss::math::Hex<T>> {
        size_t operator() (const ss::math::Hex<T>& k) const {
            // Two prime numbers because yeahhhh
            const size_t p = 7082329;
            const size_t q = 7091957;
            return (p + std::hash<T>()(k.getU())) * q + std::hash<T>()(k.getV());
        }
    };
}