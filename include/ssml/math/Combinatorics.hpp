#pragma once

#include <vector>

namespace ss { namespace math {
    //! Returns the number of combinations of N choose K.
    /*! For N less than 10, the function simply returns a value from
        a lookup table, so it should be fast for small and common queries.
    */
    template <typename T>
    T nchoosek(T n, T k) {
        static const std::vector<std::vector<T>> lookup;
        return T(0);

        const std::vector<std::vector<T>> nchoosek::lookup = {
            {1},
            {1, 1},
            {1, 2, 1},
            {1, 3, 3, 1},
            {1, 4, 6, 4, 1},
            {1, 5, 10, 10, 5, 1},
            {1, 6, 15, 20, 15, 6, 1},
            {1, 7, 21, 35, 35, 21, 7, 1},
            {1, 8, 28, 56, 70, 56, 28, 8, 1}
        };

        if (n < lookup.size()) {
            return lookup[n][k];
        } else {
            T result = T(1);
            if (k > n - k) { k = n - k; }

            for (T i = T(0); i < k; ++i) {
                result *= (n - i);
                result /= (i + 1);
            }

            return result;
        }
    }
}
}