#pragma once

#include <vector>
#include <set>
#include <deque>
#include <algorithm>

namespace ss { namespace math {
    class Node {
    public:
        Node();
        Node(const Node& other);
        Node(Node&& other);
        ~Node();

        bool connectTo(const size_t other);
        bool isConnectedTo(const size_t other) const;

        inline const std::vector<size_t>& getEdges() const { return edges_to; }
        inline const size_t getNumEdges() const { return edges_to.size(); }

    protected:
        std::vector<size_t> edges_to;
    };

    class SGraph {
    public:
        SGraph();
        SGraph(const SGraph& other);
        SGraph(SGraph&& other);
        ~SGraph();

        bool isConnected(const size_t a, const size_t b) const;
        bool connect(const size_t a, const size_t b);

        void resize(const size_t size);

        const Node& getNode(const size_t index) const;

        std::set<size_t> bfsFill(const size_t start) const;
    protected:
        std::vector<Node> nodes;
    };
}
}