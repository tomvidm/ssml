#pragma once

#include "math/Vector2.hpp"

namespace ss { namespace math {
    //! Bilinear interpolation for some defined region
    /*! This class precomputes the polynomial coefficients for a region,
        and provides the value at any point (x, y) through the () operator.
    */
    class Blerp {
    public:
        Blerp() = delete;
        Blerp(
            const float f00, const float f01,
            const float f10, const float f11,
            const float x0, const float x1,
            const float y0, const float y1
        );

        float operator () (const math::Vector2f xy) const;
        float get(const float x, const float y) const;
    private:
        float a0, a1, a2, a3;
    };

    float blerp(
        const float f00, const float f01,
        const float f10, const float f11,
        math::Vector2f origin,
        math::Vector2f size,
        math::Vector2f xy
    );

    float blerp(
        const float f00, const float f01,
        const float f10, const float f11,
        const float x0, const float x1,
        const float y0, const float y1,
        const float x, 
        const float y
    );
}
}