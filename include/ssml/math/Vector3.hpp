#pragma once

#include "SFML/System/Vector3.hpp"
#include <functional>

namespace ss { namespace math {

    // Hide SFML interface
    using Vector3f = sf::Vector3<float>;
    using Vector3u = sf::Vector3<unsigned>;
    using Vector3i = sf::Vector3<int>;
}
}

namespace std {
    template <typename T>
    struct hash<sf::Vector3<T>> {
        size_t operator() (const sf::Vector3<T>& k) const {
            // Two prime numbers because yeahhhh
            const size_t p = 7082329;
            const size_t q = 7091957;
            const size_t r = 7091357;
            return ((p + std::hash<T>()(k.x)) * q + 
                     std::hash<T>()(k.y)) * r +
                     std::hash<T>()(k.z);
        }
    };
}