#pragma once

#include <math.h>

#include "math/Vector2.hpp"
#include "math/Vector3.hpp"

namespace ss { namespace math {
    float dot(const Vector2f& lhs, const Vector2f& rhs);
    float dot(const Vector3f& lhs, const Vector3f& rhs);
    
    float dotSquared(const Vector2f& lhs, const Vector2f& rhs);
    float dotSquared(const Vector3f& lhs, const Vector3f& rhs);

    float magnitudeSquared(const Vector2f& vec);
    float magnitudeSquared(const Vector3f& vec);

    float magnitude(const Vector2f& vec);
    float magnitude(const Vector3f& vec);

    float cross(const Vector2f& lhs, const Vector2f& rhs);
    //float cross(const Vector3f& lhs, const Vector3f& rhs);

    Vector2f normalize(const Vector2f& vec);
    Vector2f project(const Vector2f& lhs, const Vector2f& rhs);
    Vector2f projectToX(const Vector2f& vec);
    Vector2f projectToY(const Vector2f& vec);

    Vector2f reflectAbout(const Vector2f& v, const Vector2f& L);
}
}
