#pragma once

namespace ss { namespace math {
    //! Simple template class for clamped values
    template <typename T>
    class ClampedValue {
    public:
        ClampedValue(const T val, const T min, const T max);
        ClampedValue(const ClampedValue& other);
        ~ClampedValue();

        ClampedValue<T> operator = (const ClampedValue<T>& other);
        ClampedValue<T> operator = (const T other);

        void operator += (const ClampedValue<T>& other);
        void operator -= (const ClampedValue<T>& other);
        void operator *= (const ClampedValue<T>& other);
        void operator /= (const ClampedValue<T>& other);

        void operator += (const T other);
        void operator -= (const T other);
        void operator *= (const T other);
        void operator /= (const T other);

        inline T getValue() const { return value; }
        inline T getMin() const { return min; }
        inline T getMax() const { return max; }
    protected:
        const T min;
        const T max;
        T value;
    };

    template <typename T>
    ClampedValue<T>::ClampedValue(const T val, const T min, const T max)
    : min(min), max(max) {
        value = val < min ? min : (val > max ? max : val);
    }

    template <typename T>
    ClampedValue<T>::ClampedValue(const ClampedValue& other)
    : min(other.min), max(other.max), value(other.value) {
        ;
    }

    template <typename T>
    ClampedValue<T>::~ClampedValue() {
        ;
    }

    template <typename T>
    ClampedValue<T> ClampedValue<T>::operator = (const ClampedValue<T>& other) {
        value = other.getValue() < min ? min : (other.getValue() > max ? max : other.getValue());
    }

    template <typename T>
    ClampedValue<T> ClampedValue<T>::operator = (const T other) {
        value = other < min ? min : (other > max ? max : other);
    }

    // ClampedValue<T> += ClampedValue<T>

    template <typename T>
    void ClampedValue<T>::operator += (const ClampedValue<T>& other) {
        value += other.getValue();
        if (value > max) { value = max; }
        else if (value < min) { value = min; }
    }

    template <typename T>
    void ClampedValue<T>::operator -= (const ClampedValue<T>& other) {
        value -= other.getValue();
        if (value > max) { value = max; }
        else if (value < min) { value = min; }
    }

    template <typename T>
    void ClampedValue<T>::operator *= (const ClampedValue<T>& other) {
        value *= other.getValue();
        if (value > max) { value = max; }
        else if (value < min) { value = min; }
    }

    template <typename T>
    void ClampedValue<T>::operator /= (const ClampedValue<T>& other) {
        value /= other.getValue();
        if (value > max) { value = max; }
        else if (value < min) { value = min; }
    }

    // ClampedValue<T> += T

    template <typename T>
    void ClampedValue<T>::operator += (const T other) {
        value += other;
        if (value > max) { value = max; }
        else if (value < min) { value = min; }
    }

    template <typename T>
    void ClampedValue<T>::operator -= (const T other) {
        value -= other;
        if (value > max) { value = max; }
        else if (value < min) { value = min; }
    }

    template <typename T>
    void ClampedValue<T>::operator *= (const T other) {
        value *= other;
        if (value > max) { value = max; }
        else if (value < min) { value = min; }
    }

    template <typename T>
    void ClampedValue<T>::operator /= (const T other) {
        value /= other;
        if (value > max) { value = max; }
        else if (value < min) { value = min; }
    }

    // ClampedValue<T> + ClampedValue<T>

    template <typename T>
    ClampedValue<T> operator + (const ClampedValue<T>& lhs, const ClampedValue<T>& rhs) {
        return ClampedValue<T>(lhs.getValue() + rhs.getValue(), lhs.getMin(), lhs.getMax());
    }

    template <typename T>
    ClampedValue<T> operator - (const ClampedValue<T>& lhs, const ClampedValue<T>& rhs) {
        return ClampedValue<T>(lhs.getValue() - rhs.getValue(), lhs.getMin(), lhs.getMax());
    }

    template <typename T>
    ClampedValue<T> operator * (const ClampedValue<T>& lhs, const ClampedValue<T>& rhs) {
        return ClampedValue<T>(lhs.getValue() * rhs.getValue(), lhs.getMin(), lhs.getMax());
    }

    template <typename T>
    ClampedValue<T> operator / (const ClampedValue<T>& lhs, const ClampedValue<T>& rhs) {
        return ClampedValue<T>(lhs.getValue() / rhs.getValue(), lhs.getMin(), lhs.getMax());
    }

    // T + ClampedValue<T>

    template <typename T>
    ClampedValue<T> operator + (const T lhs, const ClampedValue<T>& rhs) {
        return ClampedValue<T>(lhs + rhs.getValue(), rhs.getMin(), rhs.getMax());
    }

    template <typename T>
    ClampedValue<T> operator - (const T lhs, const ClampedValue<T>& rhs) {
        return ClampedValue<T>(lhs - rhs.getValue(), rhs.getMin(), rhs.getMax());
    }

    template <typename T>
    ClampedValue<T> operator * (const T lhs, const ClampedValue<T>& rhs) {
        return ClampedValue<T>(lhs * rhs.getValue(), rhs.getMin(), rhs.getMax());
    }

    template <typename T>
    ClampedValue<T> operator / (const T lhs, const ClampedValue<T>& rhs) {
        return ClampedValue<T>(lhs / rhs.getValue(), rhs.getMin(), rhs.getMax());
    }

    // ClampedValue<T> + T

    template <typename T>
    ClampedValue<T> operator + (const ClampedValue<T>& lhs, const T rhs) {
        return ClampedValue<T>(lhs.getValue() + rhs, lhs.getMin(), lhs.getMax());
    }

    template <typename T>
    ClampedValue<T> operator - (const ClampedValue<T>& lhs, const T rhs) {
        return ClampedValue<T>(lhs.getValue() - rhs, lhs.getMin(), lhs.getMax());
    }

    template <typename T>
    ClampedValue<T> operator * (const ClampedValue<T>& lhs, const T rhs) {
        return ClampedValue<T>(lhs.getValue() - rhs, lhs.getMin(), lhs.getMax());
    }

    template <typename T>
    ClampedValue<T> operator / (const ClampedValue<T>& lhs, const T rhs) {
        return ClampedValue<T>(lhs.getValue() / rhs, lhs.getMin(), lhs.getMax());
    }
}
}