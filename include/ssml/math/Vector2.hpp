#pragma once

#include "SFML/System/Vector2.hpp"
#include <functional>

namespace ss { namespace math {

    // Hide SFML interface
  
    using sf::Vector2;
    using sf::Vector2f;
    using sf::Vector2u;
    using sf::Vector2i;

    Vector2f dir(const float radian);

    struct Vector2iCompare {
        bool operator()(const Vector2i& a, const Vector2i& b) const;
    };
}
}

namespace std {
    template <typename T>
    struct hash<sf::Vector2<T>> {
        size_t operator() (const sf::Vector2<T>& k) const {
            // Two prime numbers because yeahhhh
            const size_t p = 7082329;
            const size_t q = 7091957;
            return (p + std::hash<T>()(k.x)) * q + std::hash<T>()(k.y);
        }
    };
}