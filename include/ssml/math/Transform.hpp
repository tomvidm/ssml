#pragma once

#include "SFML/Graphics/Transform.hpp"
#include "SFML/Graphics/Transformable.hpp"

#include "math/Vector2.hpp"
#include "math/Vector3.hpp"

namespace ss { namespace math {
    // Hide SFML interface
    using sf::Transform;
    using sf::Transformable;

    inline math::Vector2f getTranslationVector(const math::Transform& transform) {
        return math::Vector2f(
            transform.getMatrix()[12],
            transform.getMatrix()[13]
        );
    }

    inline float inverse(
        const float a00, const float a01,
        const float a10, const float a11
    ) { return a00 * a11 - a01 * a10; }

    

    class Transform2 {
    public:
        Transform2();
        
        Transform2(
            math::Vector2f v0,
            math::Vector2f v1
        );

        Transform2(
            const float a00, const float a01,
            const float a10, const float a11
        );

        inline float determinant() const {
            return a00 * a11 - a01 * a10;
        }

        friend Transform2 inverse(const Transform2& tform);
        friend Transform2 operator * (const float lhs, const Transform2& rhs);
        friend math::Vector2f operator * (const Transform2& lhs, const math::Vector2f rhs);
    private:
        const float a00, a01, 
                    a10, a11;
    };

    Transform2 inverse(const Transform2& tform);

    class Transform3 {
    public:
        Transform3();

        Transform3(
            math::Vector3f v0,
            math::Vector3f v1,
            math::Vector3f v2
        );

        Transform3(
            const float a00, const float a01, const float a02,
            const float a10, const float a11, const float a12,
            const float a20, const float a21, const float a22
        );

        float determinant() const;

        friend Transform3 inverse(const Transform3& t);
        friend Transform3 operator * (const float lhs, const Transform3& rhs);
        friend math::Vector3f operator * (const Transform3& lhs, const math::Vector3f rhs);
    
    private:
        const float a00, a01, a02,
                    a10, a11, a12,
                    a20, a21, a22;
    };
}
}

