#pragma once

#include "math/Rect.hpp"
#include "math/Transform.hpp"
#include "math/Vector2.hpp"
#include "math/VectorArithmetic.hpp"

namespace ss { namespace math {
    struct Line {
        const Vector2f p0;
        const Vector2f p1;

        Vector2f getDifference() const;
        float getLength() const;
        Rect<float> getBoundingRect() const;
        Line getTransformed(const math::Transform& transform);
    };

    bool linesIntersect(
        const Vector2f& u0,
        const Vector2f& u1,
        const Vector2f& v0,
        const Vector2f& v1
    );

    bool linesIntersect(
        const Line& line0,
        const Line& line1
    );

    bool lineIntersectsCircle(const Line& line, const Vector2f center, const float radius);

    float distanceToLineSquared(const Line& line, const Vector2f& point);
    float distanceToLine(const Line& line, const Vector2f& point);

    float distanceAlongLineSquared(const Line& line, const Vector2f& point);
    float distanceAlongLine(const Line& line, const Vector2f& point);
}
}