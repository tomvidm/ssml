#pragma once

#include "rapidjson/filereadstream.h"
#include "rapidjson/document.h"

namespace ss { namespace json {
    rapidjson::Document parseJsonFile(const std::string& filepath);
} // namespace json
} // namespace p2d