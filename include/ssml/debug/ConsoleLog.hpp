#pragma once

#include <deque>
#include <string>

namespace ss { namespace debug {
    class ConsoleLog {
    public:
        ConsoleLog();

        void pushLogMessage(const std::string logMessage);
        void setCapacity(const size_t newCap);

        std::string toString(const size_t numLines) const;
    private:
        size_t capacity;
        std::deque<std::string> stringDeque;
    };
}
}