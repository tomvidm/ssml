#pragma once

#include <random>
#include <stdexcept>

namespace ss { namespace random {
    //! A template class, intended for sampling from a finite range of values.
    /*! This class wraps some c++ <random> library functionality, to enable a clean and readable way
        to sample instances of type S from a finite collection.

    */
    template <typename S>
    class DiscreteSampler {
    public:
        DiscreteSampler(const std::vector<float> weights,
                        const std::vector<S> samples);

        S operator() ();
    private:
        std::vector<float> weights;
        std::vector<S> samples;

        std::discrete_distribution distribution;
        std::default_random_engine generator;
    };

    template <typename S>
    DiscreteSampler::DiscreteSampler(const std::vector<float> weights,
                                     const std::vector<S> samples)
    : weights(weights),
      samples(samples) {
        if (weights.size() != samples.size()) {
            throw std::runtime_error("The size of the weights and the samples are mismatched.");
        }
        distribution = std::discrete_distribution(weights);
    }

    template <typename S>
    S DiscreteSampler::operator() () {
        return samples[distribution(generator)];
    }
}
}