#include "gtest/gtest.h"

#include "physics/DynamicBody.hpp"
#include "physics/CollisionMesh.hpp"
#include "physics/CollisionDetection.hpp"

namespace ss { namespace physics {
    TEST(TestCollision, test_line_mesh) {
        CollisionMesh::shared lineMesh = std::make_shared<LineMesh>(
            math::Line{
                math::Vector2f(-1.f, 1.f),
                math::Vector2f(1.f, -1.f)
            }
        );

        math::Transform translate(
            1.f, 0.f, 2.f,
            0.f, 1.f, 2.f,
            0.f, 0.f, 1.f
        );

        math::Rect<float> lineMeshBounds = math::Rect<float>(
            -1.f, -1.f, 2.f, 2.f
        );

        math::Rect<float> lineMeshBoundsTranslated = math::Rect<float>(
            1.f, 1.f, 2.f, 2.f
        );

        EXPECT_EQ(lineMesh->getBoundingRect(), lineMeshBounds);
        EXPECT_EQ(translate.transformRect(lineMesh->getBoundingRect()), lineMeshBoundsTranslated);
    }

    TEST(TestCollision, test_circle_mesh) {
        CollisionMesh::shared circleMesh = std::make_shared<CircleMesh>(1.f);

        math::Transform translate(
            1.f, 0.f, 2.f,
            0.f, 1.f, 2.f,
            0.f, 0.f, 1.f
        );

        math::Rect<float> circleMeshBounds = math::Rect<float>(
            -1.f, -1.f, 2.f, 2.f
        );

        math::Rect<float> circleMeshBoundsTranslated = math::Rect<float>(
            1.f, 1.f, 2.f, 2.f
        );

        EXPECT_EQ(circleMesh->getBoundingRect(), circleMeshBounds);
        EXPECT_EQ(translate.transformRect(circleMesh->getBoundingRect()), circleMeshBoundsTranslated);
    }

    TEST(TestCollision, test_line_to_circle_intersection) {
        physics::DynamicBody bodyA;
        physics::DynamicBody bodyB;

        CollisionMesh::shared colliderA = std::make_shared<CircleMesh>(1.f);
        CollisionMesh::shared colliderB = std::make_shared<LineMesh>(
            math::Line{
                math::Vector2f(-1.f, 3.f),
                math::Vector2f(1.f, -3.f)
            }
        );
        
        bodyA.setCollisionMesh(colliderA);
        bodyB.setCollisionMesh(colliderB);

        bodyA.setPosition(math::Vector2f(-2.f, 0.f));
        bodyB.setPosition(math::Vector2f(2.f, 0.f));

        // Broad phase negative
        // Narrow phase skipped
        EXPECT_FALSE(physics::bodiesIntersect(
            &bodyA,
            &bodyB
        ));

        // Broad phase positive
        // Narrow phase negative
        bodyB.setPosition(math::Vector2f(-1.f, 1.f));

        EXPECT_FALSE(physics::bodiesIntersect(
            &bodyA,
            &bodyB
        ));

        // Broad phase positive
        // Narrow phase positive
        bodyB.setPosition(math::Vector2f(-1.f, -1.f));

        EXPECT_TRUE(physics::bodiesIntersect(
            &bodyA,
            &bodyB
        ));
    }

    TEST(TestCollision, test_circle_to_circle_intersection) {
        physics::DynamicBody bodyA;
        physics::DynamicBody bodyB;

        CollisionMesh::shared colliderA = std::make_shared<CircleMesh>(1.f);
        CollisionMesh::shared colliderB = std::make_shared<CircleMesh>(2.f);
        
        bodyA.setCollisionMesh(colliderA);
        bodyB.setCollisionMesh(colliderB);

        bodyA.setPosition(math::Vector2f(-1.f, 0.f));
        bodyB.setPosition(math::Vector2f(2.f, 0.f));

        EXPECT_FALSE(physics::bodiesIntersect(
            &bodyA,
            &bodyB
        ));

        bodyA.setPosition(math::Vector2f(-0.5f, 0.f));

        EXPECT_TRUE(physics::bodiesIntersect(
            &bodyA,
            &bodyB
        ));
    }
}
}