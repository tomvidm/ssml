#include "gtest/gtest.h"

#include "animation/Frame.hpp"
#include "animation/SpriteAnimation.hpp"
#include "animation/SpriteAnimationController.hpp"

#include "system/Time.hpp"

namespace ss { namespace math {
    TEST(TestAnimation, sprite_animation_controller) {
        using animation::Frame;
        using animation::SpriteAnimation;
        using animation::SpriteAnimationController;
        using system::seconds;
        using math::Rect;

        SpriteAnimationController animController;

        SpriteAnimation anim;
        anim.addFrame(Frame{seconds(1.f), Rect<float>()});
        anim.addFrame(Frame{seconds(1.f), Rect<float>()});
        anim.addFrame(Frame{seconds(1.f), Rect<float>()});
        anim.addFrame(Frame{seconds(1.f), Rect<float>()});

        EXPECT_EQ(anim.getNumFrames(), 4);

        animController.setAnimation(anim);

        EXPECT_EQ(animController.getCurrentFrameIndex(), 0);
        EXPECT_FALSE(animController.update(seconds(0.5f)));
        EXPECT_TRUE(animController.update(seconds(1.f)));
        EXPECT_EQ(animController.getCurrentFrameIndex(), 1);
        animController.update(seconds(2.f));
        EXPECT_EQ(animController.getCurrentFrameIndex(), 3);
    }
}
}