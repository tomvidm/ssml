#include "gtest/gtest.h"

#include "utility/IntervalMap.hpp"

namespace ss { namespace utility {
    TEST(TestUtility, interval_map) {
        IntervalMap<int, float> intmap(0.f);
        EXPECT_EQ(intmap[5], 0.f);
    }
}
}