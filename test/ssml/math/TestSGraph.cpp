#include "gtest/gtest.h"

#include "math/SGraph.hpp"

namespace ss { namespace math {
    TEST(TestMath, sgraph_basic) {
        SGraph graph;
        graph.resize(5);

        EXPECT_FALSE(graph.isConnected(0, 1));
        graph.connect(0, 1);
        EXPECT_TRUE(graph.isConnected(0, 1));
        graph.connect(0, 3);
        graph.connect(1, 2);
        graph.connect(1, 3);
        graph.connect(2, 4);
        graph.connect(3, 4);

        EXPECT_TRUE(graph.isConnected(2, 4));
        EXPECT_EQ(graph.getNode(2).getNumEdges(), 2);
        graph.resize(4);
        EXPECT_FALSE(graph.isConnected(2, 4));
        EXPECT_EQ(graph.getNode(2).getNumEdges(), 1);
    }
}
}