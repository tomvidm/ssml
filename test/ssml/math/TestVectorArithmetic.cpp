#include "gtest/gtest.h"

#include "math/Vector2.hpp"
#include "math/VectorArithmetic.hpp"

namespace ss { namespace math {
    TEST(TestMath, vector_operations) {
        Vector2f up_right(2.f, 2.f);
        Vector2f right(2.f, 0.f);
        Vector2f down_right(2.f, -2.f);
        Vector2f down(0.f, -2.f);

        EXPECT_EQ(dot(right, right), 4.f);
        EXPECT_EQ(dot(up_right, down_right), 0.f);

        EXPECT_EQ(magnitudeSquared(up_right), 8.f);
        EXPECT_EQ(magnitude(up_right), sqrtf(8.f));

        EXPECT_EQ(cross(right, right), 0.f);
        EXPECT_EQ(cross(up_right, down_right), -8.f);

        EXPECT_EQ(normalize(right), Vector2f(1.f, 0.f));
    }
}
}