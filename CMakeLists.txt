cmake_minimum_required(VERSION 3.5)

option(Testing "Build tests." OFF)

if (UNIX)
    message(STATUS "Generating project for Unix")    
    set(SFML_DIR "/usr/share/SFML-2.5.1/lib/cmake/SFML")
    set(CMAKE_CXX_FLAGS "-g -Wall -std=c++11")
endif(UNIX)

if (WIN32)
    message(STATUS "Generating project for Win32")
    set(SFML_DIR "C:/lib/SFML-2.5.1/lib/cmake/SFML")
    set(BOOST_ROOT "C:/local/boost_1_59_0")
    set(BOOST_INCLUDEDIR "${BOOST_ROOT}")
    set(BOOST_LIBRARYDIR "C:/local/boost_1_59_0/lib32-msvc-14.0")
    set(CMAKE_CXX_FLAGS "-std=c++11")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MTd")
endif(WIN32)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake/")

set(SSML_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include/ssml)
set(SSML_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/src/ssml)
set(RAPIDJSON_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/external/rapidjson/include)
set(FASTNOISE_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/external/FastNoise)

find_package(SFML 2.5 COMPONENTS graphics window system REQUIRED)
find_package(Boost 1.59 COMPONENTS filesystem REQUIRED)

add_subdirectory(src)

message(STATUS "${Boost_INCLUDE_DIRS}")

include_directories(
    ${FASTNOISE_INCLUDE_DIR}
    ${Boost_INCLUDE_DIRS}
)

add_library(fastnoise
    external/FastNoise/FastNoise.cpp
)

if (Testing)
    add_subdirectory(external/googletest)
    add_subdirectory(test)
endif()
