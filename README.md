# SSML (Simple & Slow Media Library)

API Reference: https://tomvidm.gitlab.io/ssml/

SSML is a hobby project. It is an attempt to create a framework built upon SFML for building simple games. Writing simple applications directly wtih SFML is simple, but I intend to make it even simpler with SSML. No more writing those pesky rendering loops. SSML is intended to provide (among other things):

* A renderer which handles draw order and culling. Render layers provide additional convenience for hiding or drawing certain elements.
* Automatic handling of texture resources. Easy access through string mappings.
* Input events with more detail based on underlying states and button specific timers. Want to execute some code when the left mouse button has been held for 10 seconds while button mashing the spacebar? I intend to ensure that SSML makes this as simple as listening for an event object.
* Tilesets for easy mapping of strings to texture rectangles.
* Quad Arrays for easy drawing of stuff without the restrictions of tilemaps.
* Bilinear interpolation! Bezier curves and B-splines!
* Tilemaps with a simple interface for assigning textures, loading from a JSON file and getting which tile the mouse is pointing on!
* Composite tilemaps for very large tilemaps and procedural generation of maps. 2048x2048 tilemap? Why not make a 32x32 grid of 64x64 tilemaps and load them when needed? Culling included. Yay!
* Procedural generation, heightmaps, color mapping!
* Isometric tilemaps, with possibility to use heightmaps to displace tiles vertically
* Hexagonal tilemaps! Make civ clones instead of terraria clones!
* Animated sprites and transforms. Make an explosion rotate, just because!
* Collision detection and resolution.
* Simple implementation of directed graphs.

What is finished and what is not can be seen under the Issues list.

## Dependencies
All dependencies should be placed in a `external` folder in the main directory. Googletest and rapidjson can be directly cloned into this folder, while SFML works by ensuring that the `SFML_ROOT` environment variable is set the location of a prebuilt SFML binary. It is tested on Ubuntu with gcc and on Windows with Visual Studio 2017.

* [SFML 2.5.0 prebuilt binary](https://www.sfml-dev.org/index.php)
* [Rapidjson](https://github.com/Tencent/rapidjson)
* [Google testing framework](https://github.com/google/googletest)
* [FastNoise](https://github.com/Auburns/FastNoise)

## How to build and run
```
cd path/to/SSML
mkdir build
cd build
cmake ..
```

### Ubuntu
```
make
```

### Windows (Visual Studio 2017)
Open the solution file that appeared in the build folder and build the solution.

Currently, when running an executable from VS which uses any resources from the `resources``folder, the following should be set:
`Project properties-> Debugging -> Working Directory -> Set to ${SolutionDir}`
This ensures that the hardcoded paths currently used by some resource loaders hit the right spot. They are all hardcoded relative to the build directory.