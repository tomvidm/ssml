#include "graphics/QuadGrid.hpp"
#include <iostream>
namespace ss { namespace graphics {
    QuadGrid::QuadGrid(const size_t rows, const size_t cols, const QuadGridStyle& style)
    : rows(rows),
      columns(cols),
      style(style),
      texture(nullptr) {
        vertexBuffer.create(4 * (rows * columns));
        vertexBuffer.setPrimitiveType(sf::PrimitiveType::Quads);
        vertexBuffer.setUsage(sf::VertexBuffer::Usage::Dynamic);
        vertices.resize(4 * rows * columns);

        buildQuads();
    }

    void QuadGrid::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= this->getTransform();
        target.draw(vertexBuffer, states);
    }

    const math::Rect<float> QuadGrid::getLocalBounds() const {
        return math::Rect<float>(
            math::Vector2f(0.f, 0.f),
            math::Vector2f(
                style.tileSize.x * static_cast<float>(rows + 1),
                style.tileSize.y * static_cast<float>(columns + 1)
            )
        );
    }

    const math::Rect<float> QuadGrid::getGlobalBounds() const {
        return this->getTransform().transformRect(this->getLocalBounds());
    }

    void QuadGrid::paintByHeightmap(const math::Heightmap& hmap) {
        for (size_t r = 0; r < rows; r++) {
            for (size_t c = 0; c < columns; c++) {
                const unsigned value = static_cast<unsigned>(hmap.get(r, c) * 255.f);
                const Color color = Color(
                    value,
                    value,
                    value
                );
                setQuadColorNoUpdate(r, c, color);
            }
        }
        updateQuads();
    }

    void QuadGrid::setQuadColor(const math::Vector2i coord, const Color color) {
        setQuadColorNoUpdate(coord, color);
        updateQuad(coord.y, coord.x);
    }

    void QuadGrid::setQuadColor(const size_t row, const size_t col, const Color color) {
        setQuadColorNoUpdate(row, col, color);
        updateQuad(row, col);
    }

    void QuadGrid::setQuadTextureRect(
            const size_t row,
            const size_t col,
            const math::Rect<float> texRect) {
        setQuadTextureRectNoUpdate(row, col, texRect);
        updateQuad(row, col);
    }

    void QuadGrid::setQuadColorAndTextureRect(
            const size_t row, const size_t col,
            const Color color, math::Rect<float> texRect) {
        setQuadColorAndTextureRectNoUpdate(row, col, color, texRect);
        updateQuad(row, col);
    }

    void QuadGrid::setQuadColorNoUpdate(const math::Vector2i coord, const Color color) {
        const size_t row = static_cast<size_t>(coord.y);
        const size_t col = static_cast<size_t>(coord.x);
        if (row < rows && col < columns) {
            setQuadColor(row, col, color);
        }
    }

    void QuadGrid::setQuadColorNoUpdate(const size_t row, const size_t col, const Color color) {
        Vertex* pointerToQuad = getQuadPointer(row, col);
        pointerToQuad[0].color = color;
        pointerToQuad[1].color = color;
        pointerToQuad[2].color = color;
        pointerToQuad[3].color = color;
        // TODO: Need to fix updateQuad(r, c)
        updateQuad(row, col);
    }

    void QuadGrid::setQuadTextureRectNoUpdate(
            const size_t row, 
            const size_t col, 
            const math::Rect<float> texRect) {
        Vertex* pointerToQuad = getQuadPointer(row, col);
        pointerToQuad[0].texCoords = math::Vector2f(
            texRect.left, texRect.top
        );
        pointerToQuad[1].texCoords = math::Vector2f(
            texRect.left + texRect.width, texRect.top
        );
        pointerToQuad[2].texCoords = math::Vector2f(
            texRect.left + texRect.width, texRect.top + texRect.height
        );
        pointerToQuad[3].texCoords = math::Vector2f(
            texRect.left, texRect.top + texRect.height
        );
    }

    void QuadGrid::setQuadColorAndTextureRectNoUpdate(
            const size_t row, const size_t col,
            const Color color, math::Rect<float> texRect) {
        Vertex* pointerToQuad = getQuadPointer(row, col);
        
        pointerToQuad[0].color = color;
        pointerToQuad[1].color = color;
        pointerToQuad[2].color = color;
        pointerToQuad[3].color = color;

        pointerToQuad[0].texCoords = math::Vector2f(
            texRect.left, texRect.top
        );
        pointerToQuad[1].texCoords = math::Vector2f(
            texRect.left + texRect.width, texRect.top
        );
        pointerToQuad[2].texCoords = math::Vector2f(
            texRect.left + texRect.width, texRect.top + texRect.height
        );
        pointerToQuad[3].texCoords = math::Vector2f(
            texRect.left, texRect.top + texRect.height
        );
    }

    math::Vector2i QuadGrid::getCoordByGlobal(const math::Vector2f globalPoint) const {
        const math::Vector2f local = this->getInverseTransform().transformPoint(globalPoint);
        return getCoordByLocal(local);
    }

    math::Vector2i QuadGrid::getCoordByLocal(const math::Vector2f localPoint) const {
        const int row = static_cast<int>(localPoint.x / style.tileSize.x);
        const int col = static_cast<int>(localPoint.y / style.tileSize.y);
        return math::Vector2i(row, col);
    }

    size_t QuadGrid::coordToIndex(const size_t row, const size_t col) const {
        return row * columns + col;
    }

    size_t QuadGrid::coordToIndex(const math::Vector2i coord) const {
        return coord.y * columns + coord.x;
    }

    void QuadGrid::buildQuads() {
        for (size_t r = 0; r < rows; r++) {
            for (size_t c = 0; c < columns; c++) {
                buildQuad(r, c);
            }
        }

        updateQuads();
    }

    void QuadGrid::buildQuad(const size_t row, const size_t col) {
        Vertex* quadPointer = getQuadPointer(row, col);
        quadPointer[0].position = math::Vector2f(
            style.tileSize.x * static_cast<float>(col),
            style.tileSize.y * static_cast<float>(row));
        quadPointer[1].position = math::Vector2f(
            style.tileSize.x * static_cast<float>(col + 1),
            style.tileSize.y * static_cast<float>(row));
        quadPointer[2].position = math::Vector2f(
            style.tileSize.x * static_cast<float>(col + 1),
            style.tileSize.y * static_cast<float>(row + 1));
        quadPointer[3].position = math::Vector2f(
            style.tileSize.x * static_cast<float>(col),
            style.tileSize.y * static_cast<float>(row + 1));
    }

    void QuadGrid::updateQuads() {
        vertexBuffer.update(vertices.data());
    }

    void QuadGrid::updateQuad(const size_t row, const size_t col) {
        vertexBuffer.update(getQuadPointer(row, col), 4, 4*(row * columns + col));
    }

    void QuadGrid::updateQuads(const size_t r0, const size_t r1,
                                const size_t c0, const size_t c1) {
        const size_t dc = c1 - c0;
        for (size_t r = r0; r <= r1; r++) {
            vertexBuffer.update(vertices.data(), 4, 4*(r * columns + c0));
        }
    }

    Vertex* QuadGrid::getQuadPointer(const size_t row, const size_t col) {
        return &(vertices.data()[4*(row * columns + col)]);
    }
}
}