#include "graphics/Sprite.hpp"

namespace ss { namespace graphics {
    Sprite::Sprite()
    : isAnimated(false) {
        ;
    }

    void Sprite::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= getTransform();
        states.texture = texture.get();
        target.draw(vertices, 4, sf::PrimitiveType::Quads, states);
    }

    void Sprite::update(const system::Time& dt) {
        if (isAnimated && animationController.update(dt)) {
            setTextureRect(animationController.getCurrentFrameRect());
        }
    }

    void Sprite::setAnimation(const animation::SpriteAnimation& animation) {
        isAnimated = true;
        animationController.setAnimation(animation);
    }

    void Sprite::setStaticSprite(const math::Rect<float> textureRect) {
        isAnimated = false;
        setTextureRect(textureRect);
    }

    const math::Rect<float> Sprite::getLocalBounds() const {
        return localBounds;
    };

    const math::Rect<float> Sprite::getGlobalBounds() const {
        return getTransform().transformRect(localBounds);
    }
}
}