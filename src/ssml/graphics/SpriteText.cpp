#include "graphics/SpriteText.hpp"

#include <boost/algorithm/string.hpp>

#include <iostream>

namespace ss { namespace graphics {
    SpriteTextStyle SpriteTextStyle::default_style = SpriteTextStyle{
        math::Vector2f(32.f, 32.f),
        4.f,
        4.f
    };

    SpriteText::SpriteText()
    : SpriteText(std::string("hello\nworld"), SpriteTextStyle::default_style) {
        rebuildVertices();
    }

    SpriteText::SpriteText(const std::string& string, const SpriteTextStyle& style)
    : string(string),
      vbuf(sf::PrimitiveType::Quads),
      style(style) {
        rebuildVertices();
    }

    void SpriteText::setSpriteFont(const std::shared_ptr<SpriteFont>& newSpriteFont) {
        spriteFont = newSpriteFont;
    }

    void SpriteText::setString(const std::string& newstring) {
        string = newstring;
        rebuildVertices();
    }

    void SpriteText::rebuildVertices() {
        std::vector<std::string> lines;
        boost::split(lines, string, boost::is_any_of("\n"));
        
        size_t numLetters = 0;
        size_t maxLineSize = 0;
        for (auto& line : lines) { 
            numLetters += line.size(); 
            if (line.size() > maxLineSize) {
                maxLineSize = line.size();
            }
        }

        localBounds = math::Rect<float>(
            0.f, 0.f,
            static_cast<float>(maxLineSize) * (style.charSize.x + style.charSpacing),
            static_cast<float>(lines.size()) * (style.charSize.y + style.lineSpacing)
        );

        vbuf.create(4 * numLetters);
        vertices.resize(4 * numLetters);

        size_t vertexIndex = 0;
        for (size_t lineIndex = 0; lineIndex < lines.size(); lineIndex++) {
            for (size_t charIndex = 0; charIndex < lines[lineIndex].size(); charIndex++) {
                const math::Vector2f upperLeft(
                    static_cast<float>(charIndex) * (style.charSize.x + style.charSpacing),
                    static_cast<float>(lineIndex) * (style.charSize.y + style.lineSpacing)
                );
                
                vertices[vertexIndex].position = upperLeft;
                vertices[vertexIndex + 1].position = upperLeft + math::projectToX(style.charSize);
                vertices[vertexIndex + 2].position = upperLeft + style.charSize;
                vertices[vertexIndex + 3].position = upperLeft + math::projectToY(style.charSize);
                
                //std::array<math::Vector2f, 4> texRectCorners = math::getCornerVectors<float>(spriteFont->getTexRect(lines[lineIndex][charIndex]));

                vertexIndex += 4;
            }
        }

        vbuf.update(vertices.data());
    }

    void SpriteText::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= getTransform();
        target.draw(vbuf, states);
    }

    const math::Rect<float> SpriteText::getLocalBounds() const {
        return localBounds;
    }

    const math::Rect<float> SpriteText::getGlobalBounds() const {
        return getTransform().transformRect(localBounds);
    }
}
}