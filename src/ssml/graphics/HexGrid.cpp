#include "graphics/HexGrid.hpp"

#include <cmath>
#include <iostream>

namespace ss { namespace graphics {
    HexGrid::HexGrid(const size_t usize, 
                     const size_t vsize, 
                     const math::HexConfiguration config)
    : usize(usize),
      vsize(vsize),
      config(config),
      vertices(8 * usize * vsize, Vertex()),
      vertexBuffer(sf::PrimitiveType::Quads) {
        vertexBuffer.create(8 * usize * vsize);
        buildHexes();
        updateHexes();
    }

    void HexGrid::setHexColor(const math::Hex<int>& hex,
                              const Color color) {
        setHexColor(hex.getU(), hex.getV(), color);
    }

    void HexGrid::setHexColor(const size_t u,
                              const size_t v,
                              const Color color) {
        HexHandle hex(getPointerToHexFromUV(u, v));
        
        hex.setColor(color);

        updateHex(u, v);
    }

    void HexGrid::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= getTransform();
        target.draw(vertexBuffer, states);
    }

    const math::Rect<float> HexGrid::getLocalBounds() const {
        return math::Rect<float>(0.f, 0.f, 1.f, 1.f);    
    }

    const math::Rect<float> HexGrid::getGlobalBounds() const {
        return math::Rect<float>(0.f, 0.f, 1.f, 1.f);
    }

    math::Hex<int> HexGrid::getHexFromGlobal(const math::Vector2f global) const {
        return config.nearestHex(getInverseTransform().transformPoint(global));
    }

    math::Hex<int> HexGrid::getHexFromLocal(const math::Vector2f local) const {
        return config.nearestHex(local);
    }

    HexHandle HexGrid::getHexHandle(const size_t u, const size_t v) {
        return HexHandle(getPointerToHexFromUV(u, v));
    }

    void HexGrid::updateHexes() {
        vertexBuffer.update(vertices.data());
    }

    void HexGrid::updateHex(const size_t u, const size_t v) {
        vertexBuffer.update(
            getPointerToHexFromUV(u, v),
            8,
            8 * (u * vsize + v)
        );
    }

    void HexGrid::buildHexes() {
        for (size_t u = 0; u < usize; u++) {
            for (size_t v = 0; v < vsize; v++) {
                // Time will tell if the hex handles will be useful here.
                hexHandles.push_back(
                    HexBuilder::buildHex(
                        getPointerToHexFromUV(u, v),
                        config.hexToVector(math::Hex<int>(u, v)),
                        config,
                        0.925f
                    )
                );
            }
        }
    }

    Vertex* const HexGrid::getPointerToHexFromUV(const size_t u, const size_t v) {
        size_t index = 8 * (u * vsize + v);
        return &(vertices.data()[index]);
    }
}
}