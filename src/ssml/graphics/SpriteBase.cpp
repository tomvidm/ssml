#include "math/Vector2.hpp"
#include "math/VectorArithmetic.hpp"

#include "graphics/SpriteBase.hpp"

namespace ss { namespace graphics {
    SpriteBase::SpriteBase() {
        ;
    }

    void SpriteBase::setTexture(SharedTexture sharedTexture) {
        texture = sharedTexture;

        const math::Vector2u texturePixelSize = texture->getSize();
        const math::Vector2f textureSize = math::Vector2f(
            static_cast<float>(texturePixelSize.x),
            static_cast<float>(texturePixelSize.y)
        );

        vertices[0].position = math::Vector2f();
        vertices[1].position = math::projectToX(textureSize);
        vertices[2].position = textureSize;
        vertices[3].position = math::projectToY(textureSize);

        vertices[0].texCoords = math::Vector2f();
        vertices[1].texCoords = math::projectToX(textureSize);
        vertices[2].texCoords = textureSize;
        vertices[3].texCoords = math::projectToY(textureSize);

        localBounds = math::Rect<float>(math::Vector2f(), textureSize);
        textureRect = math::Rect<float>(math::Vector2f(), textureSize);
    }

    void SpriteBase::setTextureFromTileset(Tileset& tileset) {
        texture = tileset.getSharedTexture();
    }

    void SpriteBase::setTextureRect(const math::Rect<float>& rect) {
        const math::Vector2f topLeft(rect.left, rect.top);
        const math::Vector2f topRight(rect.left + rect.width, rect.top);
        const math::Vector2f bottomRight(rect.left + rect.width, rect.top + rect.height);
        const math::Vector2f bottomLeft(rect.left, rect.top + rect.height);
        const math::Vector2f size(rect.width, rect.height);
        
        vertices[0].position = math::Vector2f();
        vertices[1].position = math::projectToX(size);
        vertices[2].position = size;
        vertices[3].position = math::projectToY(size);

        vertices[0].texCoords = topLeft;
        vertices[1].texCoords = topRight;
        vertices[2].texCoords = bottomRight;
        vertices[3].texCoords = bottomLeft;

        localBounds = math::Rect<float>(math::Vector2f(), size);
        textureRect = rect;
    }

    void SpriteBase::setSize(const math::Vector2f& size) {
        vertices[0].position = math::Vector2f();
        vertices[1].position = math::projectToX(size);
        vertices[2].position = size;
        vertices[3].position = math::projectToY(size);
    }

    math::Rect<float> SpriteBase::getTextureRect() const {
        return textureRect;
    }
}
}