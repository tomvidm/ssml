#include "graphics/CompositeTilemap.hpp"

#include <iostream>
#include <stdexcept>
#include <cmath>

namespace ss { namespace graphics {
    CompositeTilemap::CompositeTilemap(
            const size_t rowsPerTilemap,
            const size_t colsPerTilemap,
            const math::Vector2f tileSize)
    : rowsPerTilemap(rowsPerTilemap),
      colsPerTilemap(colsPerTilemap),
      tileSize(tileSize) {
        ;
    }

    void CompositeTilemap::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= getTransform();

        for (auto& tmap : visibilityList) {
            target.draw(*tmap, states);
        }
    }

    const math::Rect<float> CompositeTilemap::getLocalBounds() const {
        return math::Rect<float>(0.f, 0.f, 0.f, 0.f);
    }

    const math::Rect<float> CompositeTilemap::getGlobalBounds() const {
        return math::Rect<float>(0.f, 0.f, 0.f, 0.f);
    }

    math::Vector2i CompositeTilemap::getTileCoordByLocal(const math::Vector2f local) const {
        return math::Vector2i(floor(local.x), floor(local.y));
    }

    math::Vector2i CompositeTilemap::getTileCoordByGlobal(const math::Vector2f global) const {
        return getTileCoordByLocal(getInverseTransform().transformPoint(global));
    }

    math::Vector2i CompositeTilemap::getTilemapCoordByLocal(const math::Vector2f localPoint) const {
        const float tilemapWidth = tileSize.x * static_cast<float>(colsPerTilemap);
        const float tilemapHeight = tileSize.y * static_cast<float>(rowsPerTilemap);
        const int tilemapColumnIndex = static_cast<int>(floor(localPoint.x / tilemapWidth));
        const int tilemapRowIndex = static_cast<int>(floor(localPoint.y / tilemapHeight));
        return math::Vector2i(tilemapColumnIndex, tilemapRowIndex);
    }

    math::Vector2i CompositeTilemap::getTilemapCoordByGlobal(const math::Vector2f globalPoint) const {
        return getTilemapCoordByLocal(getInverseTransform().transformPoint(globalPoint));
    }

    math::Vector2i CompositeTilemap::getTilemapCoordByTile(const math::Vector2i tileCoord) const {
        return math::Vector2i(tileCoord.x / colsPerTilemap, tileCoord.y / rowsPerTilemap);
    }

    SharedTilemap CompositeTilemap::getTilemapByLocal(const math::Vector2f localPoint) const {
        const math::Vector2i coord = getTilemapCoordByLocal(localPoint);
        return tilemapCache.at(coord);
    }

    SharedTilemap CompositeTilemap::getTilemapByGlobal(const math::Vector2f globalPoint) const {
        return getTilemapByLocal(getInverseTransform().transformPoint(globalPoint));
    }

    SharedTilemap CompositeTilemap::getTilemapByTile(const math::Vector2i tileCoord) const {
        const math::Vector2i coord = getTilemapCoordByTile(tileCoord);
        return tilemapCache.at(coord);
    }

    void CompositeTilemap::updateVisibility(math::Rect<float> rect) {
        math::Rect<int> coverage = coordinatesCoveredBy(rect);
        visibilityList.clear();
        visibilityList.reserve((coverage.width) * (coverage.height));
        for (int r = coverage.top; r < coverage.top + coverage.height; r++) {
            for (int c = coverage.left; c < coverage.left + coverage.width; c++) {
                const math::Vector2i coord(c, r);
                try {
                    visibilityList.push_back(tilemapCache.at(coord));
                } catch (std::out_of_range& except) {
                    std::cerr << except.what() << std::endl;
                }
            }
        }
    }

    math::Rect<int> CompositeTilemap::coordinatesCoveredBy(math::Rect<float> rect) const {
        const float tilemapWidth = tileSize.x * static_cast<float>(colsPerTilemap);
        const float tilemapHeight = tileSize.y * static_cast<float>(rowsPerTilemap);
        const int left =   static_cast<int>(floor(rect.left / tilemapWidth));
        const int top =    static_cast<int>(floor(rect.top / tilemapHeight));
        const int width =  1 + static_cast<int>(ceil(rect.width / tilemapWidth));
        const int height = 1 + static_cast<int>(ceil(rect.height / tilemapHeight));
        return math::Rect<int>(left, top, width, height);
    }

    bool CompositeTilemap::addTilemap(const math::Vector2i coord, SharedTilemap sharedTilemap) {
        const float tilemapWidth = tileSize.x * static_cast<float>(colsPerTilemap);
        const float tilemapHeight = tileSize.y * static_cast<float>(rowsPerTilemap);
        sharedTilemap->setPosition(math::Vector2f(
            tilemapWidth * static_cast<float>(coord.x),
            tilemapHeight * static_cast<float>(coord.y)
        ));
        if (tilemapCache.count(coord) == 1) {
            return false;
        } else {
            tilemapCache.insert({coord, sharedTilemap});
            return true;
        }
    }

    bool CompositeTilemap::removeTilemap(const math::Vector2i coord) {
        if (tilemapCache.count(coord) == 1) {
            tilemapCache.erase(coord);
            return true;
        } else {
            return false;
        }
    }
}
}