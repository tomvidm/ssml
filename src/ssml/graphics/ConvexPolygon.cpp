#include "graphics/ConvexPolygon.hpp"

namespace ss { namespace graphics {
    ConvexPolygon::ConvexPolygon(const math::ConvexPolygon& polygon) {
        mesh.setPrimitiveType(sf::PrimitiveType::TrianglesFan);
        setPolygon(polygon);
    }

    void ConvexPolygon::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= getTransform();
        target.draw(mesh, states);
    }

    void ConvexPolygon::setPolygon(const math::ConvexPolygon& polygon) {
        _polygon = polygon;
        mesh.clear();
        for (auto& v : _polygon.getVertexList()) {
            mesh.append(
                sf::Vertex(
                    v,
                    sf::Color::Red
                )
            );
        }
    }

    const math::Rect<float> ConvexPolygon::getLocalBounds() const {
        return _polygon.getBoundingBox();
    }

    const math::Rect<float> ConvexPolygon::getGlobalBounds() const {
        return getTransform().transformRect(_polygon.getBoundingBox());
    }
}
}