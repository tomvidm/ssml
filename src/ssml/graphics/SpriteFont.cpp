#include "graphics/SpriteFont.hpp"

namespace ss { namespace graphics {
    void SpriteFont::addTexRect(const char c, const math::Rect<float>& rect) {
        charmap.insert(std::make_pair(c, rect));
    }

    math::Rect<float> SpriteFont::getTexRect(const char c) const {
        return charmap.at(c);
    }
}
}