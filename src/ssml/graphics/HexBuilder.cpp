#include "graphics/HexBuilder.hpp"

namespace ss { namespace graphics {
    HexHandle HexBuilder::buildHex(
        Vertex* const vertexPtr, 
        const math::Vector2f center, 
        const math::HexConfiguration& config,
        const float scale) 
    {
                              
        // Upper quad
        vertexPtr[0].position = center + scale * config.getVertexU();
        vertexPtr[1].position = center - scale * config.getVertexU();
        vertexPtr[2].position = center + scale * config.getVertexW();
        vertexPtr[3].position = center - scale * config.getVertexV();

        // Lower quad
        vertexPtr[4].position = center + scale * config.getVertexU();
        vertexPtr[5].position = center - scale * config.getVertexW();
        vertexPtr[6].position = center + scale * config.getVertexV();
        vertexPtr[7].position = center - scale * config.getVertexU();

        return HexHandle(vertexPtr);
    }

    HexOutlineHandle HexBuilder::buildHexOutline(
        Vertex* const vertexPtr, 
        const math::Vector2f center, 
        const math::HexConfiguration& config,
        const float thickness) 
    {
                                                          
        const float inner = 1.f - thickness;
        const float outer = 1.f + thickness;
        const math::Vector2f innerU = inner * config.getVertexU();
        const math::Vector2f outerU = outer * config.getVertexU();
        const math::Vector2f innerV = inner * config.getVertexV();
        const math::Vector2f outerV = outer * config.getVertexV();
        const math::Vector2f innerW = inner * config.getVertexW();
        const math::Vector2f outerW = outer * config.getVertexW();

        // Band 0
        vertexPtr[0].position = center + outerU;
        vertexPtr[1].position = center + innerU;
        vertexPtr[2].position = center - innerV;
        vertexPtr[3].position = center - outerV;

        // Band 1
        vertexPtr[4].position = center - outerW;
        vertexPtr[5].position = center - innerW;
        vertexPtr[6].position = center + innerU;
        vertexPtr[7].position = center + outerU;

        // Band 2
        vertexPtr[8].position = center + outerV;
        vertexPtr[9].position = center + innerV;
        vertexPtr[10].position = center - innerW;
        vertexPtr[11].position = center - outerW;

        // Band 3
        vertexPtr[12].position = center - outerU;
        vertexPtr[13].position = center - innerU;
        vertexPtr[14].position = center + innerV;
        vertexPtr[15].position = center + outerV;

        // Band 4
        vertexPtr[16].position = center + outerW;
        vertexPtr[17].position = center + innerW;
        vertexPtr[18].position = center - innerU;
        vertexPtr[19].position = center - outerU;

        // Band 5
        vertexPtr[20].position = center - outerV;
        vertexPtr[21].position = center - innerV;
        vertexPtr[22].position = center + innerW;
        vertexPtr[23].position = center + outerW;

        return HexOutlineHandle(vertexPtr);
    }
}
}