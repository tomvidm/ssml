#include "graphics/Tilemap.hpp"

#include <iostream>

namespace ss { namespace graphics {
    Tilemap::Tilemap(
        const size_t rows,
        const size_t cols,
        const math::Vector2f tileSize,
        SharedTileset tileset)
    : QuadGrid(rows, cols, QuadGridStyle{tileSize}),
      tileset(tileset) {
        tileData.resize(rows * columns);
    }

    void Tilemap::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= this->getTransform();

        if (tileset != nullptr) {
            states.texture = tileset->getTexturePtr();
        }

        target.draw(vertexBuffer, states);
    }

    void Tilemap::setTile(const size_t row, const size_t col, const Tile tile) {
        tileData[coordToIndex(row, col)] = tile;
        setQuadTextureRect(row, col, tileset->getTextureRect(tile.tilesetIndex));
    }
}
}