#include "json/JsonHelpers.hpp"

#include "math/Vector2.hpp"
#include "graphics/Tileset.hpp"
#include <iostream>
namespace ss { namespace graphics {
    Tileset::Tileset() {
        spritesheet = std::make_shared<Texture>();
    }

    void Tileset::loadFromJSON(const std::string filename) {
        std::cout << "Loading Tileset " << filename << std::endl;
        const rapidjson::Document doc = json::parseJsonFile("../resources/textures/" + filename);
        const std::string textureFilename = doc["texture_file"].GetString();
        const std::string tilesetFormat = doc["tileset_format"].GetString();

        spritesheet->loadFromFile("../resources/textures/" + textureFilename); 
        if (tilesetFormat == "regular") {
            loadFormatA(doc);
        } else {
            loadFormatB(doc);
        }

        unsigned number_of_loaded_subsprites = 0;
    }

    void Tileset::loadFormatB(const rapidjson::Document& doc) {
        for (auto& subsprite : doc["subsprites"].GetArray()) {
            const size_t subspriteId = textureRects.size();
            const std::string alias = subsprite["name"].GetString();
            std::cout << alias << " - " << subspriteId << std::endl;
            const math::Rect<float> textureRect(
                static_cast<float>(subsprite["x"].GetInt()),
                static_cast<float>(subsprite["y"].GetInt()),
                static_cast<float>(subsprite["width"].GetInt()),
                static_cast<float>(subsprite["height"].GetInt())
            );
            textureRects.push_back(textureRect);
        }
    }

    void Tileset::loadFormatA(const rapidjson::Document& doc) {
        const int spacingX = doc["subsprite_spacing_x"].GetInt();
        const int spacingY = doc["subsprite_spacing_y"].GetInt();
        const int sizeX = doc["subsprite_size_x"].GetInt();
        const int sizeY = doc["subsprite_size_y"].GetInt();
        for (auto& entry : doc["subsprites"].GetArray()) {
            const std::string type = entry["type"].GetString();
            const size_t spriteGroupId = textureRects.size();
            if (type == "single") {
                const std::string alias = entry["alias"].GetString();
                idToSpriteIndexMap[alias] = spriteGroupId;
                std::cout << alias << " - " << spriteGroupId << std::endl;

                const int row = entry["tile"][0].GetInt();
                const int col = entry["tile"][1].GetInt();
                const math::Rect<float> textureRect(
                    static_cast<float>((sizeX + spacingX) * col),
                    static_cast<float>((sizeY + spacingY) * row),
                    static_cast<float>(sizeX),
                    static_cast<float>(sizeY)
                );

                textureRects.push_back(textureRect);
            } else if (type == "animation") {
                animation::SpriteAnimation animation;

                const std::string animAlias = entry["alias"].GetString();
                for (auto& tile : entry["tiles"].GetArray()) {
                    const size_t spriteId = textureRects.size() - spriteGroupId;
                    const size_t animationId = animations.size();
                    const std::string spriteAlias = animAlias + std::to_string(spriteId); 
                    idToSpriteIndexMap[spriteAlias] = spriteId;
                    idToAnimationIndexMap[animAlias] = animationId;

                    const int row = tile[0].GetInt();
                    const int col = tile[1].GetInt();
                    const system::Time frameDuration = system::milliseconds(tile[2].GetInt());
                    
                    const math::Rect<float> textureRect(
                        static_cast<float>((sizeX + spacingX) * col),
                        static_cast<float>((sizeY + spacingY) * row),
                        static_cast<float>(sizeX),
                        static_cast<float>(sizeY)
                    );

                    const animation::Frame frame{
                        frameDuration,
                        textureRect
                    };

                    animation.addFrame(frame);
                }
                animations.push_back(animation);
            } else {
                return;
            }
        }
    }

    const Rect<float> Tileset::getTextureRect(const std::string& alias) const {
        const size_t index = idToSpriteIndexMap.at(alias);
        return textureRects[index];
    }

    const Rect<float> Tileset::getTextureRect(const size_t index) const {
        return textureRects[index];
    }

    const animation::SpriteAnimation& Tileset::getAnimation(const std::string& alias) const {
        const size_t index = idToAnimationIndexMap.at(alias);
        return animations[index];
    }

    const animation::SpriteAnimation& Tileset::getAnimation(const size_t index) const {
        return animations[index];
    }

    SharedTexture Tileset::getSharedTexture() {
        return spritesheet;
    }

    Texture* Tileset::getTexturePtr() {
        return spritesheet.get();
    }
}
}