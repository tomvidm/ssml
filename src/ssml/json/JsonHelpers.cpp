#include <cstdio>

#include "json/JsonHelpers.hpp"

namespace ss { namespace json {
    rapidjson::Document parseJsonFile(const std::string& filepath) {
        FILE* file = fopen(filepath.c_str(), "r");
        if (file == NULL) {
            printf("Failed to open file %s\n", filepath.c_str());
            return rapidjson::Document();
        }
        const size_t bufferSize = 4096;
        char buffer[bufferSize];
        rapidjson::FileReadStream fReadStream(file, buffer, sizeof(buffer));

        rapidjson::Document doc;
        doc.ParseStream(fReadStream);
        fclose(file);

        return doc;
    }
} // namespace json
} // namespace p2d