#include "system/RenderLayer.hpp"

#include <algorithm>
#include <iostream>

namespace ss { namespace system {
    void RenderLayer::addRenderable(SharedRenderable& renderable) {
        // Problem! No mechanism for catching duplicates. Duplicate entries
        // will cause the use_count to always be D > 1 + number of duplicates
        // Also, the mechanism should be moved to the Renderer, as the user
        // may attmpt to add the renderable to multiple layers. 
        renderables.push_back(renderable);
    }

    void RenderLayer::updateRenderList(const math::Rect<float> cullingRect) {
        renderList.clear();
        for (size_t i = 0; i < renderables.size(); i++) {
            if (renderables[i].use_count() > 1 &&
                cullingRect.intersects(renderables[i]->getGlobalBounds())) 
            {
                renderList.push_back(renderables[i].get());
            }
        }
    }

    void RenderLayer::cleanRenderables() {
        for (size_t i = 0; i < renderables.size(); i++) {
            if (renderables[i].use_count() == 1) {
                std::swap(renderables[i], renderables.back());
                renderables.pop_back();
                i--; // Look at the swapped object too
            }
        }
    }

    void RenderLayer::render(sf::RenderWindow* window) {
        for (size_t i = 0; i < renderList.size(); i++) {
            window->draw(*renderList[i]);
        }
    }
}
}