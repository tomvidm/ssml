#include "system/Renderer.hpp"

#include <limits>

namespace ss { namespace system {
    Renderer::Renderer() {
        ;
    }

    void Renderer::addRenderable(SharedRenderable renderable) {
        defaultLayer.addRenderable(renderable);
    }

    void Renderer::addDrawable(sf::Drawable* drawable) {
        drawables.push_back(drawable);
    }

    void Renderer::setRenderWindow(sf::RenderWindow* windowPtr) {
        window = windowPtr;
    }

    void Renderer::render(math::Rect<float> coverage) {
        window->clear();
        defaultLayer.updateRenderList(coverage);
        defaultLayer.cleanRenderables();
        defaultLayer.render(window);

        for (auto& layer : renderLayers) {
            layer.cleanRenderables();
            layer.updateRenderList(coverage);
            layer.render(window);
        }
        window->display();
    }

    void Renderer::renderAll() {
        render(math::Rect<float>(
            std::numeric_limits<float>::lowest() / 2.f,
            std::numeric_limits<float>::lowest() / 2.f,
            std::numeric_limits<float>::max(),
            std::numeric_limits<float>::max()
        ));
    }
}
}