#include "json/JsonHelpers.hpp"

#include "system/EngineConfig.hpp"

namespace ss { namespace system {
    EngineConfig::EngineConfig(
        const unsigned winWidth,
        const unsigned winHeight,
        const unsigned frameLimit,
        const std::string& resSearchPath
    ) : windowWidth(winWidth),
        windowHeight(winHeight),
        frameRateLimit(frameLimit),
        resourceSearchPath(resSearchPath) /*,
        resourceDirectory(resourceSearchPath)*/ {;}

    EngineConfig EngineConfig::loadFromJSON(const std::string& pathToConfig) {
        rapidjson::Document doc = json::parseJsonFile(pathToConfig);
        return EngineConfig(
            doc["window_settings"]["width"].GetUint(),
            doc["window_settings"]["height"].GetUint(),
            doc["engine_settings"]["frame_rate_limit"].GetUint(),
            doc["engine_settings"]["resource_search_path"].GetString()
        );
    }
}
}