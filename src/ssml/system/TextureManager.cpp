#include "system/TextureManager.hpp"

namespace ss { namespace system {
    TextureManager::TextureManager() {
        defaultFilepath = "../resources/textures/";
    }

    void TextureManager::loadTextureFromFile(const std::string& textureAlias, const std::string& filename) {
        graphics::SharedTexture texturePtr = std::make_shared<graphics::Texture>();
        texturePtr->loadFromFile(defaultFilepath + filename);
        std::pair<std::string, graphics::SharedTexture> mapEntry(
            textureAlias,
            texturePtr
        );

        textureMap.insert(mapEntry);
    }

    void TextureManager::loadTilesetFromJSON(const std::string& tilesetAlias, const std::string& filepath) {
        graphics::SharedTileset newTileset = std::make_shared<graphics::Tileset>();
        newTileset->loadFromJSON(filepath);

        std::pair<std::string, graphics::SharedTileset> mapEntry(
            tilesetAlias,
            newTileset
        );

        tilesetMap.insert(mapEntry);
    }

    graphics::SharedTexture TextureManager::getTexture(const std::string& textureAlias) const {
        return textureMap.at(textureAlias);
    }

    graphics::SharedTileset TextureManager::getTileset(const std::string& tilesetAlias) const {
        return tilesetMap.at(tilesetAlias);
    }
}
}