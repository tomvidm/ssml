#include "system/RenderView.hpp"

namespace ss { namespace system {
    void RenderView::render(sf::RenderTarget& target) const {
        for (size_t i = 0; i < cullingList.size(); i++) {
            target.draw(*cullingList[i]);
        }
    }

    void RenderView::performCulling() {
        cullingList.clear();

        for (size_t i = 0; i < renderLayers.size(); i++) {
            if (visibleLayers[i] == false) {
                continue;
            }

            std::vector<graphics::Renderable*> culledLayerList;
            culledLayerList = renderLayers[i]->getCulledLayerList(screenSpaceRect);
            cullingList.insert(cullingList.end(),
                               culledLayerList.begin(),
                               culledLayerList.end());
        }
    }
}
}