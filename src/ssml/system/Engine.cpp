#include "SFML/Graphics.hpp"

#include "system/EngineConfig.hpp"
#include "system/EngineEvent.hpp"
#include "system/Engine.hpp"

#include <iostream>

namespace ss { namespace system {
    Engine::Engine(const EngineConfig& config)
    : m_isRunning(false),
      engineConfig(config),
      window(
        sf::VideoMode(config.getWindowWidth(), 
                      config.getWindowHeight()), 
                      "SSML") {
        renderer.setRenderWindow(&window);
        perFrameTimer.restart();
        engineEvents.push_back(EngineEvent(EngineEventType::InitSuccess));
        m_isRunning = true;
    }

    void Engine::processInputs() {
        inputEvents.clear();
        sf::Event sfmlEvent;
        while (window.pollEvent(sfmlEvent)) {
            if (sfmlEvent.type == sf::Event::EventType::Closed) {
                window.close();
                m_isRunning = false;
                engineEvents.push_back(EngineEvent(EngineEventType::Quit));
            } else {
                inputEvents.push_back(inputHandler.onEvent(sfmlEvent));
            }
        }
    }

    system::Time Engine::perFrameUpdate() {
        engineEvents.clear();
        processInputs();
        return perFrameTimer.getElapsedTime();
    }

    void Engine::render() {
        if (perFrameTimer.getElapsedTime() > engineConfig.getFrameInterval()) {
            perFrameTimer.restart();
            renderer.renderAll();
        }
    }
}
}