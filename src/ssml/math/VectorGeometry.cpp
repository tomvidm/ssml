#include "math/VectorArithmetic.hpp"
#include "math/VectorGeometry.hpp"

namespace ss { namespace math {
    Vector2f Line::getDifference() const {
        return p1 - p0;
    }

    float Line::getLength() const {
        return magnitude(p1 - p0);
    }

    Rect<float> Line::getBoundingRect() const {
        const float xMin = std::min(p0.x, p1.x);
        const float yMin = std::min(p0.y, p1.y);
        const float xMax = std::max(p0.x, p1.x);
        const float yMax = std::max(p0.y, p1.y);
        return Rect<float>(
            xMin, yMin, xMax - xMin, yMax - yMin
        );
    }

    Line Line::getTransformed(const math::Transform& transform) {
        return Line{
            transform.transformPoint(p0),
            transform.transformPoint(p1)
        };
    }

    bool linesIntersect(
        const Vector2f& u0,
        const Vector2f& u1,
        const Vector2f& v0,
        const Vector2f& v1
    ) {
        Line line0{u0, u1};
        Line line1{v0, v1};

        return linesIntersect(line0, line1);
    }

    bool linesIntersect(
        const Line& line0,
        const Line& line1
    ) {
        const Vector2f d0 = line0.p1 - line0.p0;
        const Vector2f d1 = line1.p1 - line1.p0;
        const Vector2f diff = line0.p0 - line1.p0;

        const float cross_d0_diff = cross(d0, diff);
        const float cross_d1_diff = cross(d1, diff);
        const float cross_d0_d1 = cross(d0, d1);

        const float t0 = cross_d0_diff / cross_d0_d1;
        const float t1 = cross_d1_diff / cross_d0_d1;

        if (0.f < t0 && t0 < 1.f &&
            0.f < t1 && t1 < 1.f) {
            return true;
        } else {
            return false;
        }
    }

    bool lineIntersectsCircle(const Line& line, const Vector2f center, const float radius) {
        const Line shiftedLine = Line{line.p0 - center, line.p1 - center};
        const math::Vector2f diffL = shiftedLine.getDifference();
        const float dotP_DD = math::dot(diffL, diffL);
        const float dotP_0D = math::dot(shiftedLine.p0, diffL);
        const float dotP_00 = math::dot(shiftedLine.p0, shiftedLine.p0);

        const float det = (dotP_0D * dotP_0D) / (dotP_DD * dotP_DD) - (dotP_00 - radius * radius) / dotP_DD;

        // No solution - no intersection
        if (det < 0.f) {
            return 0.f;
        }

        const float det_sqrt = sqrtf(det);

        const float s0 = -dotP_0D / dotP_DD - det_sqrt;
        const float s1 = s0 + 2*det_sqrt;

        if ((s0 > 0.f && s0 < 1.f) && (s1 > 0.f && s1 < 1.f)) {
            return true;
        } else {
            return false;
        }
        
    }

    float distanceToLineSquared(const Line& line, const Vector2f& point) {
        Vector2f dLine = line.getDifference();
        Vector2f toPoint = point - line.p0;

        return magnitudeSquared(toPoint) - dotSquared(dLine, toPoint) / magnitudeSquared(dLine);
    }

    float distanceToLine(const Line& line, const Vector2f& point) {
        return sqrtf(distanceToLineSquared(line, point));
    }

    float distanceAlongLineSquared(const Line& line, const Vector2f& point) {
        const Vector2f dLine = line.getDifference();
        const Vector2f toPoint = point - line.p0;
        const float dotPL = math::dot(dLine, toPoint);
        
        return dotPL * dotPL / math::magnitudeSquared(dLine);
    }

    float distanceAlongLine(const Line& line, const Vector2f& point) {
        const Vector2f dLine = line.getDifference();
        const Vector2f toPoint = point - line.p0;
        const float dotPL = math::dot(dLine, toPoint);

        if (dotPL < 0.f) {
            return dotPL / math::magnitude(dLine);
        } else {
            return sqrtf(distanceAlongLineSquared(line, point));
        }
    }
}
}