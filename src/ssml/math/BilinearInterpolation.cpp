#include "math/BilinearInterpolation.hpp"

namespace ss { namespace math {
    Blerp::Blerp(
        const float f00, const float f01,
        const float f10, const float f11,
        const float x0, const float x1,
        const float y0, const float y1
    ) {
        const float x1x0 = x1 - x0;
        const float x0x1 = x0 - x1;
        const float y1y0 = y1 - y0;
        const float y0y1 = y0 - y1;

        // According to wikipedia

        a0 = f00 * x1 * y1 / x0x1 / y0y1 +
             f01 * x1 * y0 / x0x1 / y1y0 +
             f10 * x0 * y1 / x0x1 / y1y0 +
             f11 * x0 * y0 / x0x1 / y0y1;

        a1 = f00 * y1 / x0x1 / y1y0 +
             f01 * y0 / x0x1 / y0y1 +
             f10 * y1 / x0x1 / y0y1 +
             f11 * y0 / x0x1 / y1y0;

        a2 = f00 * x1 / x0x1 / y1y0 +
             f01 * x1 / x0x1 / y0y1 +
             f10 * x0 / x0x1 / y0y1 +
             f11 * x0 / x0x1 / y1y0;

        a3 = f00 / x0x1 / y0y1 +
             f01 / x0x1 / y1y0 +
             f10 / x0x1 / y1y0 +
             f11 / x0x1 / y0y1;
    }

    float Blerp::operator () (const math::Vector2f xy) const {
        return get(xy.x, xy.y);
    }

    float Blerp::get(const float x, const float y) const {
        return a0 + a1 * x + a2 * y + a3 * x * y;
    }


    float blerp(
        const float f00, const float f01,
        const float f10, const float f11,
        math::Vector2f origin,
        math::Vector2f size,
        math::Vector2f xy
    ) {
        return blerp(
            f00, f01,
            f10, f11,
            origin.x, origin.x + size.x,
            origin.y, origin.y + size.y,
            xy.x,
            xy.y
        );
    }

    float blerp(
        const float f00, const float f01,
        const float f10, const float f11,
        const float x0, const float x1,
        const float y0, const float y1,
        const float x, 
        const float y
    ) {
        const float dx = x1 - x0;
        const float dy = y1 - y0;
        return (f00 * (x1 - x) * (y1 - y) +
                f10 * (x - x0) * (y1 - y) +
                f01 * (x1 - x) * (y - y0) +
                f11 * (x - x0) * (y - y0)) /
                (dx * dy);
    }


}
}