#include "math/SGraph.hpp"

namespace ss { namespace math {
    Node::Node() {;}
    
    Node::Node(const Node& other)
    : edges_to(other.edges_to) {;}

    Node::Node(Node&& other)
    : edges_to(other.edges_to) {;}

    Node::~Node() {;}

    bool Node::connectTo(const size_t other) {
        if (!isConnectedTo(other)) {
            edges_to.push_back(other);
            return true;
        }
        return false;
    }

    bool Node::isConnectedTo(const size_t other) const {
        for (auto e : edges_to) {
            if (e == other) {
                return true;
            }
        }
        return false;
    }

    SGraph::SGraph() {;}
    
    SGraph::SGraph(const SGraph& other)
    : nodes(other.nodes) {;}

    SGraph::SGraph(SGraph&& other)
    : nodes(other.nodes) {;}

    SGraph::~SGraph() {;}

    bool SGraph::isConnected(const size_t a, const size_t b) const {
            if (std::max(a, b) >= nodes.size()) {
                return false;
            } else if (nodes[a].isConnectedTo(b) &&
                nodes[b].isConnectedTo(a)) {
                return true;
            } else { 
                return false; 
            }
        }

    bool SGraph::connect(const size_t a, const size_t b) {
        if (!isConnected(a, b)) {
            nodes[a].connectTo(b);
            nodes[b].connectTo(a);
            return true;
        }
        return false;
    }

    void SGraph::resize(const size_t size) {
        // TODO: When trimming the graph, any node edges
        // to removed nodes must also be cleaned.
        nodes.resize(size);
    }

    const Node& SGraph::getNode(const size_t index) const {
        return nodes[index];
    }

    std::set<size_t> SGraph::bfsFill(const size_t start) const {
        std::deque<size_t> frontier;
        std::set<size_t> result;
        std::vector<bool> visited(nodes.size(), false);
        frontier.push_back(start);

        while (!frontier.empty()) {
            const size_t current = frontier.front();
            frontier.pop_front();

            for (auto other : nodes[current].getEdges()) {
                if (visited[other] == false) {
                    frontier.push_back(other);
                }
            }

            visited[current] = false;
            result.insert(current);

        }

        return result;
    }
}
}