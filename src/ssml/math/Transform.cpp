#include "math/Transform.hpp"

namespace ss { namespace math {
    Transform2::Transform2()
    : a00(1.f), a01(0.f),
      a10(0.f), a11(1.f) {;}
      
    Transform2::Transform2(
        math::Vector2f v0,
        math::Vector2f v1
    ) : Transform2(v0.x, v1.x, v0.y, v1.y) {;}

    Transform2::Transform2(
        const float a00,
        const float a01,
        const float a10,
        const float a11
    ) : a00(a00), a01(a01),
        a10(a10), a11(a11) {;}

    Transform2 inverse(const Transform2& tform) {
        return (1.f / tform.determinant()) * 
            Transform2(
                 tform.a11, -tform.a01,
                 -tform.a10, tform.a00
            );
    }

    Transform2 operator * (const float lhs, const Transform2& rhs) {
        return Transform2(
            lhs * rhs.a00, lhs * rhs.a01,
            lhs * rhs.a10, lhs * rhs.a11
        );
    }

    math::Vector2f operator * (const Transform2& lhs, const math::Vector2f rhs) {
        return math::Vector2f(
            lhs.a00 * rhs.x + lhs.a01 * rhs.y,
            lhs.a10 * rhs.x + lhs.a11 * rhs.y
        );
    }

    Transform3::Transform3()
    : a00(1.f), a01(0.f), a02(0.f),
      a10(0.f), a11(1.f), a12(0.f),
      a20(0.f), a21(0.f), a22(1.f) {;}

    Transform3::Transform3(
        const float a00, const float a01, const float a02,
        const float a10, const float a11, const float a12,
        const float a20, const float a21, const float a22
    ) : a00(a00), a01(a01), a02(a02),
        a10(a10), a11(a11), a12(a12),
        a20(a20), a21(a21), a22(a22) {;}

    Transform3::Transform3(
        math::Vector3f v0,
        math::Vector3f v1,
        math::Vector3f v2
    ) : Transform3(
        v0.x, v1.x, v2.x,
        v0.y, v1.y, v2.y,
        v0.z, v1.z, v2.z) {;}

    float Transform3::determinant() const {
        return a00 * (a11 * a22 - a12 * a21)
             - a01 * (a12 * a20 - a10 * a22)
             + a02 * (a10 * a21 - a20 * a11);
    }

    Transform3 inverse(const Transform3& t) {
        return (1.f / t.determinant()) * Transform3(
            inverse(t.a11, t.a12, t.a21, t.a22),
            inverse(t.a02, t.a01, t.a22, t.a21),
            inverse(t.a01, t.a02, t.a11, t.a12),

            inverse(t.a12, t.a10, t.a22, t.a20),
            inverse(t.a00, t.a02, t.a20, t.a22),
            inverse(t.a02, t.a00, t.a12, t.a10),

            inverse(t.a10, t.a11, t.a20, t.a21),
            inverse(t.a01, t.a00, t.a21, t.a20),
            inverse(t.a00, t.a01, t.a10, t.a11)
        );
    }

    Transform3 operator * (const float lhs, const Transform3& rhs) {
        return Transform3(
            lhs * rhs.a00, lhs * rhs.a01, lhs * rhs.a02,
            lhs * rhs.a10, lhs * rhs.a11, lhs * rhs.a12,
            lhs * rhs.a20, lhs * rhs.a21, lhs * rhs.a22
        );
    }

    math::Vector3f operator * (const Transform3& lhs, const math::Vector3f rhs) {
        return math::Vector3f(
            lhs.a00 * rhs.x + lhs.a01 * rhs.y + lhs.a02 * rhs.z,
            lhs.a10 * rhs.x + lhs.a11 * rhs.y + lhs.a12 * rhs.z,
            lhs.a20 * rhs.x + lhs.a21 * rhs.y + lhs.a22 * rhs.z
        );
    }
}
}