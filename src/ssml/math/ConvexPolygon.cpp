#include "math/ConvexPolygon.hpp"
#include "math/VectorArithmetic.hpp"

#include <limits>

namespace ss { namespace math {
    ConvexPolygon::ConvexPolygon(std::vector<math::Vector2f> vectors)
    : vertexList(vectors) {
        ;
    }

    bool ConvexPolygon::containsPoint(const math::Vector2f point) const {
        if (getBoundingBox().contains(point)) {
            for (size_t i = 0; i < vertexList.size(); i++) {
                const Vector2f v0 = vertexList[i];
                const Vector2f v1 = vertexList[(i + 1) % vertexList.size()];
                const Vector2f v = v1 - v0;
                if (cross(point - v0, v) < 0.f) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    math::Vector2f ConvexPolygon::getFarthestInDirectionOf(const math::Vector2f dir) const {
        size_t maxIndex = 0;
        float maxDotProduct = math::dot(dir, vertexList[0]);

        for (size_t i = 1; i < vertexList.size(); i++) {
            const float newDotProduct = math::dot(dir, vertexList[i]);
            if (newDotProduct > maxDotProduct) {
                maxDotProduct = newDotProduct;
                maxIndex = i;
            }
        }

        return vertexList[maxIndex];
    }

    math::Vector2f ConvexPolygon::getVertexVector(const size_t vertexIndex) const {
        return vertexList[vertexIndex % vertexList.size()];
    }

    math::Vector2f ConvexPolygon::getEdgeVector(const size_t edgeIndex) const {
        const math::Vector2f edgeVector = getVertexVector(edgeIndex + 1) - getVertexVector(edgeIndex);
        return edgeVector;
    }

    Rect<float> ConvexPolygon::getBoundingBox() const {
        float min_x = std::numeric_limits<float>::max();
        float max_x = std::numeric_limits<float>::lowest();
        float min_y = std::numeric_limits<float>::max();
        float max_y = std::numeric_limits<float>::lowest();
        
        for (auto& v : vertexList) {
            if (v.x < min_x) min_x = v.x;
            if (v.x > max_x) max_x = v.x;
            if (v.y < min_y) min_x = v.y;
            if (v.y > max_y) max_x = v.y;
        }

        return math::Rect<float>(min_x, min_y, max_x - min_x, max_y - min_y);
    }

    const std::vector<Vector2f> ConvexPolygon::getVertexList() const {
        return vertexList;
    }

    ConvexPolygon generateCircle(const size_t numVertices, const float radius) {
        std::vector<Vector2f> vertices(numVertices);
        for (int i = 0; i < numVertices; i++) {
            const float fi = static_cast<float>(i);
            const float fn = static_cast<float>(numVertices);

            const float angle = two_pi * fi / fn;
            vertices[i] = Vector2f(
                radius * cosf(angle),
                radius * sinf(angle)
            );
        }
        return ConvexPolygon(vertices);
    }
}
}