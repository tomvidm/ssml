#include <math.h>

#include "math/Vector2.hpp"

namespace ss { namespace math {
    Vector2f dir(const float radian) {
        return Vector2f(cosf(radian), sinf(radian));
    }

    bool Vector2iCompare::operator()(const Vector2i& a, const Vector2i& b) const {
        return a.x < b.x && a.y < b.y;
    }

}
}