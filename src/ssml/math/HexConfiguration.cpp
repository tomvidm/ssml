#include "math/HexConfiguration.hpp"
#include "math/VectorArithmetic.hpp"

#include <cmath>
#include <algorithm>

namespace ss { namespace math {
    HexConfiguration::HexConfiguration(
        const Vector2f u,
        const Vector2f v,
        const Vector2f w)
    : vertexU(u),
      vertexV(v),
      vertexW(w),
      unitU(u - w),
      unitV(v - u),
      unitW(w - v),
      uv2xy(unitU, -unitW),
      xy2uv(math::inverse(uv2xy)) {
        ;
    }

    Vector2f HexConfiguration::hexToVector(const Hex<int> hex) const {
        return static_cast<float>(hex.getU()) * vertexU +
               static_cast<float>(hex.getV()) * vertexV +
               static_cast<float>(hex.getW()) * vertexW;
    }

    Vector2f HexConfiguration::hexToVector(const Hex<float> hex) const {
        return hex.getU() * vertexU +
               hex.getV() * vertexV +
               hex.getW() * vertexW;
    }

    Hex<float> HexConfiguration::toHexSpace(const Vector2f vec) const {
        const Vector2f uv = xy2uv * vec;
        return Hex<float>(uv.x, uv.y);
    }

    Hex<int> HexConfiguration::nearestHex(const Vector2f vec) const {
        return nearestHex(toHexSpace(vec));
    }

    // TODO
    Hex<int> HexConfiguration::nearestHex(const Hex<float> hexf) const {
        float ru = round(hexf.getU());
        float rv = round(hexf.getV());
        float rw = round(hexf.getW());

        const float u_diff = abs(hexf.getU() - ru);
        const float v_diff = abs(hexf.getV() - rv);
        const float w_diff = abs(hexf.getW() - rw);

        if (u_diff > v_diff && u_diff > w_diff) {
            ru = -(rv + rw);
        } else if (v_diff > w_diff) {
            rv = -(ru + rw);
        } else {
            rw = -(ru + rv);
        }

        return Hex<int>(
            static_cast<int>(ru),
            static_cast<int>(rv),
            static_cast<int>(rw)
        );
    }

    std::vector<std::pair<float, Hex<int>>> HexConfiguration::nearestNeighbors(const Vector2f pos) const {
        const Hex<int> centerHex = nearestHex(pos);

        const Hex<int> neighbor0 = centerHex + Hex<int>(1, -1, 0);
        const Hex<int> neighbor1 = centerHex + Hex<int>(1, 0, -1);
        const Hex<int> neighbor2 = centerHex + Hex<int>(0, 1, -1);
        const Hex<int> neighbor3 = centerHex + Hex<int>(-1, 1, 0);
        const Hex<int> neighbor4 = centerHex + Hex<int>(-1, 0, 1);
        const Hex<int> neighbor5 = centerHex + Hex<int>(0, -1, 1);

        std::vector<std::pair<float, Hex<int>>> neighborHexes = {
            std::make_pair(magnitude(pos - hexToVector(neighbor0)), neighbor0),
            std::make_pair(magnitude(pos - hexToVector(neighbor1)), neighbor1),
            std::make_pair(magnitude(pos - hexToVector(neighbor2)), neighbor2),
            std::make_pair(magnitude(pos - hexToVector(neighbor3)), neighbor3),
            std::make_pair(magnitude(pos - hexToVector(neighbor4)), neighbor4),
            std::make_pair(magnitude(pos - hexToVector(neighbor5)), neighbor5),
        };

        std::sort(neighborHexes.begin(), neighborHexes.end());

        return neighborHexes;
    }

    HexConfiguration HexConfiguration::makeFlatTop(
        const float majorWidth,
        const float minorWidth,
        const float height
    ) {
        return HexConfiguration(
            Vector2f(majorWidth / 2.f, 0.f),
            Vector2f(-minorWidth / 2.f, height / 2.f),
            Vector2f(-minorWidth / 2.f, -height / 2.f)
        );
    }

    HexConfiguration HexConfiguration::makePointyTop(
            const float majorHeight,
            const float minorHeight,
            const float width
    ) {
        return HexConfiguration(
            Vector2f(width / 2.f, minorHeight / 2.f),
            Vector2f(-width / 2.f, minorHeight / 2.f),
            Vector2f(0.f, -majorHeight / 2.f)
        );
    }
}
}