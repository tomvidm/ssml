#include "math/Bezier.hpp"

namespace ss { namespace math {
    math::Vector2f quadraticBezier(
        const math::Vector2f p0,
        const math::Vector2f p1,
        const math::Vector2f p2,
        const float t
    ) {
        const float tm = 1 - t;
        return math::Vector2f(
            tm * tm * p0 +
            2 * t * tm * p1 +
            t * t * p2
        );
    }

    float quadraticBezierUnitBox(
        const math::Vector2f cpoint,
        const float t
    ) {
        return quadraticBezier(
            math::Vector2f(0.f, 0.f),
            cpoint,
            math::Vector2f(1.f, 1.f),
            t
        ).y;
    }
}
}