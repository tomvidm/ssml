#include "math/Heightmap.hpp"

#include <random>
#include <algorithm>
#include <iostream>

namespace ss { namespace math {
    Heightmap::Heightmap(const size_t rows, const size_t cols, const float value)
    : rows(rows), cols(cols), minValue(value), maxValue(value) {
        values.resize(rows, std::vector<float>(cols, value));
    }

    Heightmap::Heightmap(
            const Heightmap& quad00,
            const Heightmap& quad01,
            const Heightmap& quad10,
            const Heightmap& quad11)
    : rows(quad00.getNumRows() + quad11.getNumRows()),
      cols(quad00.getNumCols() + quad11.getNumCols()),
      minValue(std::min({quad00.getMinValue(), quad01.getMinValue(), 
                         quad10.getMinValue(), quad11.getMinValue()})),
      maxValue(std::max({quad00.getMaxValue(), quad01.getMaxValue(), 
                         quad10.getMaxValue(), quad11.getMaxValue()})) {
        if (quad00.getNumRows() == quad01.getNumRows() &&
            quad00.getNumCols() == quad10.getNumCols() &&
            quad01.getNumCols() == quad11.getNumCols() &&
            quad10.getNumRows() == quad11.getNumRows()) 
        {
            values.resize(rows, std::vector<float>(cols, 0.f));

            for (size_t r = 0; r < quad00.getNumRows(); r++) {
                for (size_t c = 0; c < quad00.getNumCols(); c++) {
                    math::Vector2u coord(c, r);
                    set(coord, quad00.get(coord));
                }
            }

            for (size_t r = 0; r < quad01.getNumRows(); r++) {
                for (size_t c = 0; c < quad01.getNumCols(); c++) {
                    math::Vector2u coord(c, r);
                    math::Vector2u offset(quad01.getNumCols(), 0);
                    set(coord + offset, quad01.get(coord));
                }
            }

            for (size_t r = 0; r < quad10.getNumRows(); r++) {
                for (size_t c = 0; c < quad10.getNumCols(); c++) {
                    math::Vector2u coord(c, r);
                    math::Vector2u offset(0, quad10.getNumRows());
                    set(offset + coord, quad10.get(coord));
                }
            }

            for (size_t r = 0; r < quad11.getNumRows(); r++) {
                for (size_t c = 0; c < quad11.getNumCols(); c++) {
                    math::Vector2u coord(c, r);
                    math::Vector2u offset(quad11.getNumCols(), quad11.getNumRows());
                    set(offset + coord, quad11.get(coord));
                }
            }
        }
    }

    float& Heightmap::get(const size_t r, const size_t c) {
        return values[r % rows][c % cols];
    }

    float Heightmap::get(const size_t r, const size_t c) const {
        return values[r % rows][c % cols];
    }

    float& Heightmap::get(const math::Vector2u coord) {
        return get(coord.y, coord.x);
    }

    float Heightmap::get(const math::Vector2u coord) const {
        return get(coord.y, coord.x);
    }

    void Heightmap::set(const size_t r, const size_t c, const float value) {
        values[r][c] = value;
        if (value > maxValue) { maxValue = value; }
        if (value < minValue) { minValue = value; }
    }

    void Heightmap::set(const math::Vector2u coord, const float value) {
        values[coord.y][coord.x] = value;
        if (value > maxValue) { maxValue = value; }
        if (value < minValue) { minValue = value; }
    }

    float Heightmap::getMinValue() const {
        return minValue;
    }

    float Heightmap::getMaxValue() const {
        return maxValue;
    }

    void Heightmap::normalize() {
        const float min = minValue;
        const float max = maxValue;
        const float a = 1.f / (max - min);
        const float b = -min / (max - min);
        for (size_t r = 0; r < rows; r++) {
            for (size_t c = 0; c < cols; c++) {
                values[r][c] *= a;
                values[r][c] += b;
            }
        }
    }

    void Heightmap::threshold(const float t) {
        for (size_t r = 0; r < rows; r++) {
            for (size_t c = 0; c < cols; c++) {
                if (values[r][c] <= t) {
                    values[r][c] = 0.f;
                } else {
                    values[r][c] = 1.f;
                }
            }
        }
    }

    void Heightmap::operator += (const Heightmap& other) {
        for (size_t r = 0; r < rows; r++) {
            for (size_t c = 0; c < cols; c++) {
                values[r][c] += other.values[r][c];
                if (values[r][c] > maxValue) { maxValue = values[r][c]; }
                else if (values[r][c] < minValue) { minValue = values[r][c]; }
            }
        }
    }

    void Heightmap::operator *= (const float factor) {
        for (size_t r = 0; r < rows; r++) {
            for (size_t c = 0; c < cols; c++) {
                values[r][c] *= factor;
                minValue *= factor;
                maxValue *= factor;
                
            }
        }
    }
}
}