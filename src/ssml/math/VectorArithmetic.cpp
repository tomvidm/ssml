#include "math/VectorArithmetic.hpp"

namespace ss { namespace math {
    float dot(const Vector2f& lhs, const Vector2f& rhs) {
        return lhs.x * rhs.x + lhs.y * rhs.y;
    }

    float dot(const Vector3f& lhs, const Vector3f& rhs) {
        return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
    }


    float dotSquared(const Vector2f& lhs, const Vector2f& rhs) {
        const float result = lhs.x * rhs.x + lhs.y * rhs.y;
        return result * result;
    }

    float dotSquared(const Vector3f& lhs, const Vector3f& rhs) {
        const float result = lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
        return result * result;
    }

    float magnitudeSquared(const Vector2f& vec) {
        return dot(vec, vec);
    }

    float magnitudeSquared(const Vector3f& vec) {
        return dot(vec,  vec);
    }

    float magnitude(const Vector2f& vec) {
        return sqrtf(magnitudeSquared(vec));
    }

    float magnitude(const Vector3f& vec) {
        return sqrtf(magnitudeSquared(vec));
    }

    float cross(const Vector2f& lhs, const Vector2f& rhs) {
        return lhs.x * rhs.y - lhs.y * rhs.x;
    }

    Vector2f normalize(const Vector2f& vec) {
        return vec * (1.f / magnitude(vec));
    }

    Vector2f project(const Vector2f& lhs, const Vector2f& rhs) {
        return rhs * (dot(lhs, rhs) / dot(rhs, rhs));
    }

    Vector2f projectToX(const Vector2f& vec) {
        return Vector2f(vec.x, 0.f);
    }

    Vector2f projectToY(const Vector2f& vec) {
        return Vector2f(0.f, vec.y);
    }

    Vector2f reflectAbout(const Vector2f& v, const Vector2f& L) {
        return 2.f * (dot(v, L) / dot(L, L)) * L - v;
    }
}
}