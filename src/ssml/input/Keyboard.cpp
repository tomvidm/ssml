#include "input/Keyboard.hpp"

#include <iostream>

namespace ss { namespace input {
    KeyboardKeyEvent::KeyboardKeyEvent()
    : type(KeyboardKeyEventType::None),
      key(KeyboardKey::Unknown) {;}

    KeyboardKeyEvent::KeyboardKeyEvent(
        const KeyboardKeyEventType type,
        const KeyboardKey key)
    : type(type),
      key(key) {;}

    KeyboardKeyEvent::KeyboardKeyEvent(const KeyboardKeyEvent& other)
    : type(other.type),
      key(other.key) {;}

    KeyboardKeyEvent::~KeyboardKeyEvent() {;}

    KeyboardKeyEvent KeyboardKeyEvent::operator=(const KeyboardKeyEvent& other) {
        type = other.type;
        key = other.key;

        return *this;
    }

    bool KeyboardKeyEvent::operator==(const KeyboardKeyEvent& other) const {
        return (type == other.type) &&
               (key == other.key);
    }

    KeyboardKeyState::KeyboardKeyState()
    : isKeyPressed(false),
      lastPressTimer(system::Clock()),
      lastReleaseTimer(system::Clock()) {;}

    KeyboardKeyState KeyboardKeyState::operator=(const KeyboardKeyState& other) {
        isKeyPressed = other.isKeyPressed;
        lastPressTimer = other.lastPressTimer;
        lastReleaseTimer = other.lastReleaseTimer;

        return *this;
    }

    KeyboardKeyEvent KeyboardKeyState::onEvent(const sf::Event& sfmlEvent) {
        switch (sfmlEvent.type) {
            case sf::Event::EventType::KeyPressed:
                return onKeyPressEvent(sfmlEvent);
            case sf::Event::EventType::KeyReleased:
                return onKeyReleaseEvent(sfmlEvent);
            default:
                return KeyboardKeyEvent();
        }
    }

    KeyboardKeyEvent KeyboardKeyState::onKeyPressEvent(const sf::Event& sfmlEvent) {
        if (isKeyPressed) {
            return KeyboardKeyEvent(
                KeyboardKeyEventType::Holding,
                sfmlEvent.key.code
            );
        } else {
            lastPressTimer.restart();
            isKeyPressed = true;
            return KeyboardKeyEvent(
                KeyboardKeyEventType::Pressed,
                static_cast<KeyboardKey>(sfmlEvent.key.code)
            );
        }
    }

    KeyboardKeyEvent KeyboardKeyState::onKeyReleaseEvent(const sf::Event& sfmlEvent) {
        lastReleaseTimer.restart();
        isKeyPressed = false;
        return KeyboardKeyEvent(
            KeyboardKeyEventType::Released,
            sfmlEvent.key.code
        );
    }

    KeyboardKeyEvent KeyboardState::onEvent(const sf::Event& sfmlEvent) {
        return keyStates[static_cast<size_t>(sfmlEvent.key.code)].onEvent(sfmlEvent);
    }

    const KeyboardKeyState KeyboardState::getKeyState(KeyboardKey key) const {
        return keyStates[static_cast<size_t>(key)];
    }
}
}