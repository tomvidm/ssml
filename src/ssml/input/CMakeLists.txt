project(ssml-input)

add_library(ssml-input
    Mouse.cpp
    Keyboard.cpp
    Input.cpp
)

set_target_properties(ssml-input
    PROPERTIES 
    LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib
)

target_link_libraries(
    ssml-input
    ssml-math
    sfml-window  
)

include_directories(
    ${SFML_INCLUDE_DIR}
)