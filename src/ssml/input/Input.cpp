#include "input/Input.hpp"

#include <iostream>

namespace ss { namespace input {
    InputEvent::InputEvent()
    : type(InputEventType::None) {;}

    InputEvent::InputEvent(const InputEvent& other)
    : type(other.type) {
        switch (type) {
            case InputEventType::Mouse:
                mouseEvent = other.mouseEvent;
                break;
            case InputEventType::Keyboard:
                keyboardKeyEvent = other.keyboardKeyEvent;
                break;
            default:
                break;
        }
    }

    InputEvent::InputEvent(const MouseEvent& mouseEvent)
    : type(InputEventType::Mouse),
      mouseEvent(mouseEvent) {;}

    InputEvent::InputEvent(const KeyboardKeyEvent& keyboardKeyEvent)
    : type(InputEventType::Keyboard),
      keyboardKeyEvent(keyboardKeyEvent) {;}

    InputEvent::~InputEvent() {;}

    bool InputEvent::isLeftClick() const {
        if (getType() == InputEventType::Mouse) {
            if (getMouseEvent().getType() == MouseEventType::Button) {
                const ButtonEvent buttonEvent = mouseEvent.getButtonEvent();
                if (buttonEvent.getType() == ButtonEventType::Click) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    InputEvent InputEvent::operator=(const InputEvent& other) {
        type = other.type;
        switch (type) {
            case InputEventType::Mouse:
                mouseEvent = other.mouseEvent;
                break;
            case InputEventType::Keyboard:
                keyboardKeyEvent = other.keyboardKeyEvent;
                break;
            default:
                break;
        }

        return *this;
    }

    bool InputEvent::operator== (const InputEvent& other) const {
        if (type == other.type) {
            switch (type) {
                case InputEventType::Mouse:
                    return mouseEvent == other.mouseEvent; break;
                case InputEventType::Keyboard:
                    return keyboardKeyEvent == other.keyboardKeyEvent; break;
                case InputEventType::None:
                    return true;
            }
        } else {
            return false;
        }
    }

    InputEvent InputHandler::onEvent(const sf::Event& sfmlEvent) {
        switch (sfmlEvent.type) {
            case sf::Event::EventType::MouseButtonPressed:
            case sf::Event::EventType::MouseButtonReleased:
            case sf::Event::EventType::MouseMoved:
            case sf::Event::EventType::MouseWheelScrolled:
                return InputEvent(mouseState.onEvent(sfmlEvent));
            case sf::Event::EventType::KeyPressed:
            case sf::Event::EventType::KeyReleased:
                return InputEvent(keyboardState.onEvent(sfmlEvent));
            default:
                return InputEvent();
        }
    };

    const MouseState& InputHandler::getMouseState() const {
        return mouseState;
    }

    bool InputHandler::isKeyboardKeyPressed(const input::KeyboardKey key) const {
        return keyboardState.getKeyState(key).isPressed();
    }

    bool InputHandler::isMouseButtonPressed(const input::Button button) const {
        return mouseState.getButtonState(button).isPressed();
    }

    system::Time InputHandler::getMouseButtonHoldTime(const input::Button button) const {
        return mouseState.getButtonState(button).getHoldTime();
    }
}
}