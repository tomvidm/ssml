#include "input/Mouse.hpp"

#include <iostream>

namespace ss { namespace input {
    ButtonEvent::ButtonEvent()
    : button(Button::None),
      trigger(ButtonEventTrigger::None),
      type(ButtonEventType::None),
      cursorPosition(math::Vector2f()) {;}

    ButtonEvent::ButtonEvent(
            const Button button, 
            const ButtonEventTrigger trigger, 
            const ButtonEventType type,
            const math::Vector2f cursorPosition)
    : button(button),
      trigger(trigger),
      type(type),
      cursorPosition(cursorPosition) {;}

    ButtonEvent::ButtonEvent(const ButtonEvent& other)
    : button(other.button),
      trigger(other.trigger),
      type(other.type),
      cursorPosition(other.cursorPosition) {;}

    ButtonEvent::~ButtonEvent() {;}

    ButtonEvent ButtonEvent::operator=(const ButtonEvent& other) {
        button = other.button;
        trigger = other.trigger;
        type = other.type;
        cursorPosition = other.cursorPosition;
        return *this;
    }

    bool ButtonEvent::operator==(const ButtonEvent& other) const {
        return (button == other.button) &&
               (trigger == other.trigger) &&
               (type == other.type) &&
               (cursorPosition == other.cursorPosition);
    }

    ButtonState::ButtonState()
    : isButtonPressed(false),
      positionOfLastPress(math::Vector2f()),
      positionOfLastRelease(math::Vector2f()),
      lastPressTimer(system::Clock()),
      lastReleaseTimer(system::Clock()),
      clickTimeThreshold(system::milliseconds(150)),
      lastTriggeredEventType(ButtonEventType::None) {;}

    ButtonEvent ButtonState::onEvent(const sf::Event& sfmlEvent) {
        switch (sfmlEvent.type) {
            case sf::Event::EventType::MouseButtonPressed:
                return onPress(sfmlEvent);
            case sf::Event::EventType::MouseButtonReleased:
                return onRelease(sfmlEvent);
        }
    }

    ButtonEvent ButtonState::onPress(const sf::Event& sfmlEvent) {
        isButtonPressed = true;
        lastPressTimer.restart();
        positionOfLastPress = math::Vector2f(
            static_cast<float>(sfmlEvent.mouseButton.x),
            static_cast<float>(sfmlEvent.mouseButton.y)
        );

        if (lastReleaseTimer.getElapsedTime() < 2.f * clickTimeThreshold &&
            lastTriggeredEventType == ButtonEventType::Click) {
            return ButtonEvent(
                static_cast<Button>(sfmlEvent.mouseButton.button),
                ButtonEventTrigger::Press,
                ButtonEventType::DoubleClick,
                positionOfLastPress
            );
        } else {
            return ButtonEvent(
                static_cast<Button>(sfmlEvent.mouseButton.button),
                ButtonEventTrigger::Press,
                ButtonEventType::Press,
                positionOfLastPress
            );
        }
    }

    ButtonEvent ButtonState::onRelease(const sf::Event& sfmlEvent) {
        isButtonPressed = false;
        lastReleaseTimer.restart();
        positionOfLastRelease = math::Vector2f(
            static_cast<float>(sfmlEvent.mouseButton.x),
            static_cast<float>(sfmlEvent.mouseButton.y)
        );

        if (lastPressTimer.getElapsedTime() < clickTimeThreshold) {
            return ButtonEvent(
                static_cast<Button>(sfmlEvent.mouseButton.button),
                ButtonEventTrigger::Release,
                ButtonEventType::Click,
                positionOfLastRelease
            );
        } else {
            return ButtonEvent(
                static_cast<Button>(sfmlEvent.mouseButton.button),
                ButtonEventTrigger::Release,
                ButtonEventType::Release,
                positionOfLastRelease
            );
        }
    }

    MoveEvent::MoveEvent()
    : position(math::Vector2f()) {;}

    MoveEvent::MoveEvent(const math::Vector2f pos)
    : position(pos) {;}

    MoveEvent::MoveEvent(const MoveEvent& other)
    : position(other.position) {;}

    MoveEvent::~MoveEvent() {;}

    MoveEvent MoveEvent::operator=(const MoveEvent& other) {
        position = other.position;
        return *this;
    }

    bool MoveEvent::operator==(const MoveEvent& other) const {
        return position == other.position;
    }

    WheelEvent::WheelEvent() 
    : direction(WheelDirection::None)  {;}

    WheelEvent::WheelEvent(const WheelDirection direction)
    : direction(direction) {;}

    WheelEvent::WheelEvent(const WheelEvent& other)
    : direction(other.direction) {;}

    WheelEvent::~WheelEvent() {;}

    WheelEvent WheelEvent::operator=(const WheelEvent& other) {
        direction = other.direction;
        return *this;
    }

    bool WheelEvent::operator==(const WheelEvent& other) const {
        return direction == other.direction;
    }

    MouseEvent::MouseEvent()
    : type(MouseEventType::None) {;}

    MouseEvent::MouseEvent(const ButtonEvent& buttonEvent)
    : type(MouseEventType::Button),
      buttonEvent(buttonEvent) {;}

    MouseEvent::MouseEvent(const MoveEvent& moveEvent)
    : type(MouseEventType::Move),
      moveEvent(moveEvent) {;}

    MouseEvent::MouseEvent(const WheelEvent& wheelEvent)
    : type(MouseEventType::Wheel),
      wheelEvent(wheelEvent) {;}

    MouseEvent::MouseEvent(const MouseEvent& other)
    : type(other.type) {
        switch (other.type) {
            case MouseEventType::Button:
                buttonEvent = other.buttonEvent;
            case MouseEventType::Move:
                moveEvent = other.moveEvent;
            case MouseEventType::Wheel:
                wheelEvent = other.wheelEvent;
        }
    }

    MouseEvent::~MouseEvent() {;}

    MouseEvent MouseEvent::operator=(const MouseEvent& other) {
        type = other.type;

        switch (other.type) {
            case MouseEventType::Button:
                buttonEvent = other.buttonEvent;
            case MouseEventType::Move:
                moveEvent = other.moveEvent;
            case MouseEventType::Wheel:
                wheelEvent = other.wheelEvent;
        }

        return *this;
    }

    bool MouseEvent::operator==(const MouseEvent& other) const {
        if (type == other.type) {
            switch (type) {
                case MouseEventType::Button:
                    return buttonEvent == other.buttonEvent;
                case MouseEventType::Move:
                    return moveEvent == other.moveEvent;
                case MouseEventType::Wheel:
                    return wheelEvent == other.wheelEvent;
                case MouseEventType::None:
                    return true;
            }
        } else {
            return false;
        }
    }

    MouseState::MouseState() {
        ;
    }

    MouseEvent MouseState::onEvent(const sf::Event& sfmlEvent) {
        switch (sfmlEvent.type) {
            case sf::Event::EventType::MouseButtonPressed:
            case sf::Event::EventType::MouseButtonReleased: {
                size_t whichButton = static_cast<size_t>(sfmlEvent.mouseButton.button);
                ButtonEvent buttonEvent = buttons[whichButton].onEvent(sfmlEvent);
                return MouseEvent(buttonEvent);
            }
            case sf::Event::EventType::MouseMoved: {
                math::Vector2f position = math::Vector2f(
                    static_cast<float>(sfmlEvent.mouseMove.x),
                    static_cast<float>(sfmlEvent.mouseMove.y)
                );
                lastRegisteredPosition = position;
                lastRegisteredPixelPosition = math::Vector2i(
                    sfmlEvent.mouseMove.x,
                    sfmlEvent.mouseMove.y
                );
                return MouseEvent(MoveEvent(position));
            }
            case sf::Event::EventType::MouseWheelScrolled:
                std::cout << "Mouse wheel offset: " << sfmlEvent.mouseWheelScroll.delta << std::endl;
                if (sfmlEvent.mouseWheelScroll.delta > 0.f) {
                    return MouseEvent(WheelEvent(WheelDirection::Up));
                } else {
                    return MouseEvent(WheelEvent(WheelDirection::Down));
                }
            default: {
                return MouseEvent();
            }
        }
    }

    const ButtonState& MouseState::getButtonState(const Button button) const {
        size_t whichButton = static_cast<size_t>(button);
        return buttons[whichButton];
    }

    math::Vector2f MouseState::getDragDelta(const Button button) const {
        if (getButtonState(button).isPressed()) {
            return lastRegisteredPosition - getButtonState(button).getPositionOfLastPress();
        } else {
            return math::Vector2f(0.f, 0.f);
        }
    }
}
}