#include "debug/ConsoleLog.hpp"

namespace ss { namespace debug {
    ConsoleLog::ConsoleLog() 
    : capacity(20) {;}

    void ConsoleLog::pushLogMessage(const std::string logMessage) {
        if (stringDeque.size() == capacity) {
            stringDeque.pop_front();
            stringDeque.push_back(logMessage);
        }
    }

    void ConsoleLog::setCapacity(const size_t newCap) {
        capacity = newCap;

        while (stringDeque.size() > capacity) {
            stringDeque.pop_front();
        }
    }

    std::string ConsoleLog::toString(const size_t numLines) const {
        std::string result;
        for (size_t i = 0; i < stringDeque.size() && i < capacity; i++) {
            result += stringDeque[i] + "\n";
        }
        return result;
    }
}
}