#include "animation/SpriteAnimationController.hpp"

#include <iostream>

namespace ss { namespace animation {
    SpriteAnimationController::SpriteAnimationController()
    : currentFrameIndex(0), 
      frameDuration(system::Time::Zero),
      animationPtr(nullptr) {}

    void SpriteAnimationController::setAnimation(const SpriteAnimation& animation, const bool resetFrameCounter) {
        animationPtr = const_cast<SpriteAnimation * const>(&animation);
        if (resetFrameCounter) {
            currentFrameIndex = 0;
        }
    }

    void SpriteAnimationController::reset() {
        currentFrameIndex = 0;
        frameDuration = system::Time::Zero;
    }
    
    bool SpriteAnimationController::update(
            const system::Time& dt) {
        frameDuration += dt;
        if (frameDuration >= getCurrentFrameDuration()) {
            while (frameDuration > getCurrentFrameDuration()) {
                frameDuration -= getCurrentFrameDuration();
                currentFrameIndex += 1;
                currentFrameIndex %= animationPtr->getNumFrames();
            }
            return true;
        } else {
            return false;
        }
    }

    const Frame SpriteAnimationController::getCurrentFrame() const {
        std::cout << currentFrameIndex << std::endl;
        return animationPtr->getFrame(currentFrameIndex);
    }

    const system::Time SpriteAnimationController::getCurrentFrameDuration() const {
        return getCurrentFrame().frameDuration;
    }

    const math::Rect<float> SpriteAnimationController::getCurrentFrameRect() const {
        return getCurrentFrame().textureRect;
    }

    const size_t SpriteAnimationController::getCurrentFrameIndex() const {
        return currentFrameIndex;
    }
}
}