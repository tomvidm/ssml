#include "animation/SpriteAnimation.hpp"

namespace ss { namespace animation {
    SpriteAnimation::SpriteAnimation() {
        ;
    }

    void SpriteAnimation::addFrame(const Frame& frame) {
        frames.push_back(frame);
    }

    const size_t SpriteAnimation::getNumFrames() const {
        return frames.size();
    }

    const Frame SpriteAnimation::getFrame(const size_t index) const {
        return frames[index];
    }
}
}