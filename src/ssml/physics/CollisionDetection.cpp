#include "physics/CollisionDetection.hpp"
#include "physics/CollisionMeshIntersection.hpp"

namespace ss { namespace physics {
    bool bodiesIntersect(const DynamicBody* bodyA, const DynamicBody* bodyB, const float timeDelay) {
        return checkIntersection(
            bodyA->getRawCollisionMesh(),
            bodyB->getRawCollisionMesh(),
            bodyA->getFutureTransform(timeDelay),
            bodyB->getFutureTransform(timeDelay)
        );
    }
}
}