#include "physics/RadialCollision.hpp"

#include <cmath>
#include <iostream>

namespace ss { namespace physics {
    bool operator < (const CollisionPrediction& lhs, const CollisionPrediction& rhs) {
        if (lhs.predictionFlag == CollisionPrediction::Flag::NoCollision) {
            return false;
        } else {
            if (rhs.predictionFlag == CollisionPrediction::Flag::NoCollision) {
                return true;
            } else {
                return lhs.timeUntilCollision < rhs.timeUntilCollision;
            }
        }
    }

    // Verified using Geogebra
    CollisionPrediction predictRadialCollision(const physics::Body::shared bodyA, const physics::Body::shared bodyB) {
        return predictRadialCollision(
                    bodyA->getPosition(),
                    bodyB->getPosition(),
                    bodyA->getVelocity(),
                    bodyB->getVelocity(),
                    bodyA->getBoundingRadius(),
                    bodyB->getBoundingRadius()
                );
    }

    CollisionPrediction predictRadialCollision(
        const math::Vector2f posA, const math::Vector2f posB,
        const math::Vector2f velA, const math::Vector2f velB,
        const float radA, const float radB) {
        
        const math::Vector2f dPos = posA - posB;
        const math::Vector2f dVel = velA - velB;

        const float dotPV = math::dot(dPos, dVel);

        // Does the relative velocity go opposite to the relative position?
        if (dotPV < 0.f) {
            return CollisionPrediction{
                CollisionPrediction::Flag::NoCollision,
                system::seconds(0.f)
            };
        }

        // Avoid division by zero when both bodies have equal velocities
        if (dVel.x == 0.f && dVel.y == 0.f) {
            return CollisionPrediction{
                CollisionPrediction::Flag::NoCollision,
                system::seconds(0.f)
            };
        }
        
        const float dotPP = math::dot(dPos, dPos);
        const float dotVV = math::dot(dVel, dVel);
        const float sumRad = radA + radB;
        
        const float determinant = (dotPV * dotPV)/(dotVV * dotVV) - (dotPP - sumRad * sumRad)/dotVV;

        // Determinant equal to zero means that there are no collisions, either in past or the future.
        if (determinant <= 0.f) {
            return CollisionPrediction{
                CollisionPrediction::Flag::NoCollision,
                system::seconds(0.f)
            };
        }

        const float determinant_sqrt = sqrtf(determinant);
        const float t0 = -dotPV/dotVV - sqrtf(determinant);
        const float t1 = t0 + 2*determinant_sqrt;

        // t1 is always greater than t0
        if (t1 < 0.f) {
            return CollisionPrediction{
                CollisionPrediction::Flag::NoCollision,
                system::seconds(0.f)
            };
        } else if (t0 < 0.f) {
            return CollisionPrediction{
                CollisionPrediction::Flag::Collision,
                system::seconds(t1)
            };
        } else {
            return CollisionPrediction{
                CollisionPrediction::Flag::Collision,
                system::seconds(t0)
            };
        }
    }
}
}