#include <iostream>

#include "physics/CollisionMeshIntersection.hpp"

namespace ss { namespace physics {
    // Check mesh type of first argument
    bool checkIntersection(
        const CollisionMesh* meshA,
        const CollisionMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        math::Rect<float> boundsForA = transformA.transformRect(meshA->getBoundingRect());
        math::Rect<float> boundsForB = transformB.transformRect(meshB->getBoundingRect());
        // First check if the bounds intersects
        if (!boundsForA.intersects(boundsForB)) {
            return false;
        } else {
            switch (meshA->getMeshType()) {
                case MeshType::Line:
                    return checkIntersection(
                        static_cast<const LineMesh*>(meshA),
                        meshB,
                        transformA,
                        transformB
                    );
                case MeshType::AABB:
                    return checkIntersection(
                        static_cast<const AABBMesh*>(meshA),
                        meshB,
                        transformA,
                        transformB
                    );
                case MeshType::Circle:
                    return checkIntersection(
                        static_cast<const CircleMesh*>(meshA),
                        meshB,
                        transformA,
                        transformB
                    );
                case MeshType::Convex:
                    return checkIntersection(
                        static_cast<const ConvexMesh*>(meshA),
                        meshB,
                        transformA,
                        transformB
                    );
                default:
                    return false;
            }
        }
    }

    // Check mesh type of second argument, given LineMesh as first
    bool checkIntersection(
        const LineMesh* meshA,
        const CollisionMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        switch (meshB->getMeshType()) {
            case MeshType::Line:
                return checkLineToLineIntersection(
                    meshA, 
                    static_cast<const LineMesh*>(meshB),
                    transformA,
                    transformB
                );
            case MeshType::AABB:
                return checkLineToAABBIntersection(
                    meshA, 
                    static_cast<const AABBMesh*>(meshB),
                    transformA,
                    transformB
                );
            case MeshType::Circle:
                return checkLineToCircleIntersection(
                    meshA,
                    static_cast<const CircleMesh*>(meshB),
                    transformA,
                    transformB
                );
            case MeshType::Convex:
                return false;
            default:
                return false;
        }
    }

    // Check mesh type of second argument, given AABBMesh as first
    bool checkIntersection(
        const AABBMesh* meshA,
        const CollisionMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        switch (meshB->getMeshType()) {
            case MeshType::Line:
                return checkLineToAABBIntersection(
                    static_cast<const LineMesh*>(meshB),
                    meshA,
                    transformB,
                    transformA
                );
            case MeshType::AABB:
                return checkAABBToAABBIntersection(
                    meshA, 
                    static_cast<const AABBMesh*>(meshB),
                    transformA,
                    transformB
                );
            case MeshType::Circle:
                return checkAABBToCircleIntersection(
                    meshA,
                    static_cast<const CircleMesh*>(meshB),
                    transformA,
                    transformB
                );
            case MeshType::Convex:
                return false;
            default:
                return false;
        }
    }

    // Check mesh type of second argument, given CircleMesh as first
    bool checkIntersection(
        const CircleMesh* meshA,
        const CollisionMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        switch (meshB->getMeshType()) {
            case MeshType::Line:
                return checkLineToCircleIntersection(
                    static_cast<const LineMesh*>(meshB),
                    meshA,
                    transformB,
                    transformA
                );
            case MeshType::AABB:
                return checkAABBToCircleIntersection(
                    static_cast<const AABBMesh*>(meshB),
                    meshA,
                    transformB,
                    transformA
                );
            case MeshType::Circle:
                return checkCircleToCircleIntersection(
                    meshA,
                    static_cast<const CircleMesh*>(meshB),
                    transformA,
                    transformB
                );
            case MeshType::Convex:
                return false;
            default:
                return false;
        }
    }
    
    bool checkIntersection(
        const ConvexMesh* meshA,
        const CollisionMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        switch (meshB->getMeshType()) {
            case MeshType::Line:
                return checkLineToConvexIntersection(
                    static_cast<const LineMesh*>(meshB),
                    meshA,
                    transformB,
                    transformA
                );
            case MeshType::AABB:
                return checkAABBToConvexIntersection(
                    static_cast<const AABBMesh*>(meshB),
                    meshA,
                    transformB,
                    transformA
                );
            case MeshType::Circle:
                return checkCircleToConvexIntersection(
                    static_cast<const CircleMesh*>(meshB),
                    meshA,
                    transformB,
                    transformA
                );
            case MeshType::Convex:
                return false;
            default:
                return false;
        }
    }

    bool checkLineToLineIntersection(
        const LineMesh* meshA, 
        const LineMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        return math::linesIntersect(
            meshA->getLine().getTransformed(transformA),
            meshB->getLine().getTransformed(transformB)
        );
    }

    bool checkLineToAABBIntersection(
        const LineMesh* meshA, 
        const AABBMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        std::cout << "checkLineToAABBIntersection is not implemented!" << std::endl;
        return false;
    }

    bool checkLineToCircleIntersection(
        const LineMesh* meshA, 
        const CircleMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        return math::lineIntersectsCircle(
            meshA->getLine().getTransformed(transformA),
            math::getTranslationVector(transformB),
            meshB->getRadius()
        );
    }

    bool checkLineToConvexIntersection(
        const LineMesh* meshA, 
        const ConvexMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        std::cout << "checkLineToConvexIntersection is not implemented!" << std::endl;
        return false;
    }

    bool checkAABBToAABBIntersection(
        const AABBMesh* meshA, 
        const AABBMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        // The narrow phase does exactly what this function would do.
        // Until a better structure of the system is found,
        // this is ok.
        return true;
    }

    bool checkAABBToCircleIntersection(
        const AABBMesh* meshA, 
        const CircleMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        std::cout << "checkAABBToCircleIntersection is not implemented!" << std::endl;
        return false;
    }

    bool checkAABBToConvexIntersection(
        const AABBMesh* meshA, 
        const ConvexMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        std::cout << "checkAABBToConvexIntersection is not implemented!" << std::endl;
        return false;
    }

    bool checkCircleToCircleIntersection(
        const CircleMesh* meshA, 
        const CircleMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        const float sumRad = meshA->getRadius() + meshB->getRadius();
        const math::Vector2f posA = math::getTranslationVector(transformA);
        const math::Vector2f posB = math::getTranslationVector(transformB);
        
        return math::magnitudeSquared(posB - posA) < sumRad * sumRad;
    }

    bool checkCircleToConvexIntersection(
        const CircleMesh* meshA, 
        const ConvexMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        std::cout << "checkCircleToConvexIntersection is not implemented!" << std::endl;
        return false;
    }

    bool checkConvexToConvexIntersection(
        const ConvexMesh* meshA, 
        const ConvexMesh* meshB,
        const math::Transform& transformA,
        const math::Transform& transformB
    ) {
        std::cout << "checkConvexToConvexIntersection is not implemented!" << std::endl;
        return false;
    }
}
}