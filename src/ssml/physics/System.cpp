#include "physics/System.hpp"

#include <limits>
#include <iostream>

#include "physics/CollisionDetection.hpp"

namespace ss { namespace physics {
    void System::attachBody(DynamicBody::shared& body) {
        dynamicBodies.push_back(body);
    }

    void System::attachStatic(DynamicBody::shared& body) {
        staticBodies.push_back(body);
    }

    void System::update(const system::Time dt) {
        // Clear any previous events

        for (auto b : dynamicBodies) {
            b->update(dt);
        }
/*         events.clear();
        std::vector<bool> alreadyCollidedThisFrame(dynamicBodies.size(), false);
        std::vector<size_t> updateTheseNormally = { dynamicBodies.size() - 1};
        for (size_t i = 0; i < dynamicBodies.size(); i++) {
            ;//dynamicBodies[i]->addForce(gravityAcceleration, dt);
        }

        for (size_t i = 0; i < dynamicBodies.size(); i++) {
            // Dynamic-Dynamic collisions
            for (size_t j = i + 1; j < dynamicBodies.size(); j++) {
                if (i == dynamicBodies.size() - 1) { break; }
                if ((!alreadyCollidedThisFrame[i] || !alreadyCollidedThisFrame[j]) &&
                    physics::bodiesIntersect(dynamicBodies[i].get(), dynamicBodies[j].get(), dt.asSeconds())) {
                    events.push_back(predictCollision(dynamicBodies[i], dynamicBodies[j], dt));
                    alreadyCollidedThisFrame[i] = true;
                    alreadyCollidedThisFrame[j] = true;
                } else {
                    updateTheseNormally.push_back(i);
                }
            }

            // Dynamic-Static collisions
            for (size_t j = 0; j < staticBodies.size(); j++) {
                if (!alreadyCollidedThisFrame[i] &&
                    bodiesIntersect(dynamicBodies[i].get(), staticBodies[j].get(), dt.asSeconds())) {
                    events.push_back(predictCollision(dynamicBodies[i], staticBodies[j], dt));
                    alreadyCollidedThisFrame[i] = true;
                }
            }
        }
        
        // Process events
        processEvents(dt);
        
        // Bodies not involved in immediate collisions
        // are updated normally
        for (auto i : updateTheseNormally) {
            if (!dynamicBodies[i]->isAtRest()) {
                dynamicBodies[i]->update(dt);
            }
        } */
    }

    std::vector<DynamicBody::shared> System::getDynamicBodiesBoundingPoint(const math::Vector2f point) {
        std::vector<DynamicBody::shared> bodies;
        for (auto body : dynamicBodies) {
            if (body->getGlobalBounds().contains(point)) {
                bodies.push_back(body);
            }
        }

        return bodies;
    }

    std::vector<DynamicBody::shared> System::getStaticBodiesBoundingPoint(const math::Vector2f point) {
        std::vector<DynamicBody::shared> bodies;

        for (auto body : staticBodies) {
            if (body->getGlobalBounds().contains(point)) {
                bodies.push_back(body);
            }
        }
        return bodies;
    }

    void System::processEvents(const system::Time dt) {
        for (auto& e : events) {
            switch (e.getType()) {
                case EventType::Collision:
                    processCollisionEvent(e.getCollisionEvent(), dt);
                    break;
                default:
                    break;
            }
        }
    }

    void System::processCollisionEvent(const CollisionEvent collision, system::Time dt) {
        const system::Time timeToImpact = collision.getTimeToImpact();
        const system::Time timeAfterImpact = dt - timeToImpact;
        DynamicBody::shared bodyA = collision.getBodyA();
        DynamicBody::shared bodyB = collision.getBodyB();

        system::Time epsilon = system::microseconds(500);
        std::cout << dt.asMicroseconds() << std::endl;

        bodyA->update(timeToImpact - epsilon);
        bodyB->update(timeToImpact - epsilon);

        CollisionMesh::shared meshA = bodyA->getSharedCollisionMesh();
        CollisionMesh::shared meshB = bodyB->getSharedCollisionMesh();

        const math::Rect<float> rectA = meshA->getTransformedBoundingRect(bodyA->getTransform());
        const math::Rect<float> rectB = meshB->getTransformedBoundingRect(bodyB->getTransform());

        math::Vector2f velA = bodyA->getVelocity();
        math::Vector2f velB = bodyB->getVelocity();

        const float massA = bodyA->getMass();
        const float massB = bodyB->getMass();
        const float massRatioA = 2 * massB / (massA + massB);
        const float massRatioB = 2 * massA / (massA + massB);
    
        if ((rectA.left + rectA.width < rectB.left) || (rectA.left > rectB.left + rectB.width)) {
            std::cout << "A left of B or A right of B" << std::endl;
            const float vx_diff = velB.x - velA.x;
            const math::Vector2f vx_diff_vec = math::Vector2f(vx_diff, 0.f);
            velA += massRatioA * vx_diff_vec;
            velB -= massRatioB * vx_diff_vec;
        } else if ((rectA.top + rectA.height < rectB.top) || (rectA.top > rectB.top + rectB.height)) {
            const float vy_diff = velB.y - velA.y;
            const math::Vector2f vy_diff_vec = math::Vector2f(0.f, vy_diff);
            velA += massRatioA * vy_diff_vec;
            velB -= massRatioB * vy_diff_vec;
        }

        bodyA->setVelocity(velA);
        bodyB->setVelocity(velB);
        
        bodyA->update(timeAfterImpact + epsilon);
        bodyB->update(timeAfterImpact + epsilon);

    }
}
}