#include "physics/CollisionMesh.hpp"

namespace ss { namespace physics {

    // LineMesh methods

    LineMesh::LineMesh(const math::Line line)
    : line(line) {
        ;
    }

    std::vector<math::Vector2f> LineMesh::getVertices() const {
        return {line.p0, line.p1};
    }

    math::Rect<float> LineMesh::getBoundingRect() const {
        return line.getBoundingRect();
    }

    math::Rect<float> LineMesh::getTransformedBoundingRect(const math::Transform& transform) const {
        return transform.transformRect(getBoundingRect());
    }

    // AABBMesh methods

    AABBMesh::AABBMesh(const math::Rect<float> rect)
    : rect(rect) {
        ;
    }
    
    std::vector<math::Vector2f> AABBMesh::getVertices() const {
        return {
            math::Vector2f(rect.left, rect.top),
            math::Vector2f(rect.left + rect.width, rect.top),
            math::Vector2f(rect.left + rect.width, rect.top + rect.height),
            math::Vector2f(rect.left, rect.top + rect.height)
        };
    }

    math::Rect<float> AABBMesh::getBoundingRect() const {
        return rect;
    }

    math::Rect<float> AABBMesh::getTransformedBoundingRect(const math::Transform& transform) const {
        return transform.transformRect(getBoundingRect());
    }

    // CircleMesh methods

    CircleMesh::CircleMesh(const float radius)
    : radius(radius) {

    }

    std::vector<math::Vector2f> CircleMesh::getVertices() const {
        const size_t resolution = 30;
        std::vector<math::Vector2f> vertices;
        vertices.reserve(resolution);
        for (size_t i = 0; i < resolution; i++) {
            const float theta_p = 2 * 3.14159265f * static_cast<float>(i + 1) / static_cast<float>(resolution);
            const float theta = 2 * 3.14159265f * static_cast<float>(i) / static_cast<float>(resolution);
            
            vertices.push_back(
                math::Vector2f(
                    radius * cosf(theta),
                    radius * sinf(theta)
                )
            );
            
            vertices.push_back(
                math::Vector2f(
                    radius * cosf(theta_p),
                    radius * sinf(theta_p)
                )
            );
        }
        return vertices;
    }

    math::Rect<float> CircleMesh::getBoundingRect() const {
        return math::Rect<float>(
            -radius,
            -radius,
            2*radius,
            2*radius
        );
    }

    math::Rect<float> CircleMesh::getTransformedBoundingRect(const math::Transform& transform) const {
        return transform.transformRect(getBoundingRect());
    }

    // ConvexMesh methods

    ConvexMesh::ConvexMesh(const math::ConvexPolygon::shared& convex)
    : convex(convex) {
        ;
    }

    std::vector<math::Vector2f> ConvexMesh::getVertices() const {
        return {math::Vector2f(0.f, 0.f)};
    }

    math::Rect<float> ConvexMesh::getBoundingRect() const {
        return convex->getBoundingBox();
    }

    math::Rect<float> ConvexMesh::getTransformedBoundingRect(const math::Transform& transform) const {
        return transform.transformRect(getBoundingRect());
    }
}
}