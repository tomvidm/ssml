#include "physics/CollisionDetectionAABB.hpp"

#include <algorithm>

namespace ss { namespace physics {
    bool sweepGhostAABB(physics::Body::shared& bodyA,
                        physics::Body::shared& bodyB,
                        const system::Time ghostingInterval) {
        return sweepGhostAABB(
            bodyA->getGlobalBounds(),
            bodyB->getGlobalBounds(),
            bodyA->getVelocity(),
            bodyB->getVelocity(),
            ghostingInterval
        );
    }

    bool sweepGhostAABB(const math::Rect<float> rectA,
                        const math::Rect<float> rectB,
                        const math::Vector2f velocityA,
                        const math::Vector2f velocityB,
                        const system::Time ghostingInterval) {
        const float dx_a = velocityA.x > 0.f ? rectA.width : -rectA.width;
        const float dy_a = velocityA.y > 0.f ? rectA.height : -rectA.height;
        const float dx_b = velocityB.x > 0.f ? rectB.width : -rectB.width;
        const float dy_b = velocityB.y > 0.f ? rectB.height : -rectB.height;

        const math::Rect<float> ghostA(
            rectA.left,
            rectA.top,
            ghostingInterval.asSeconds() * velocityA.x + dx_a,
            ghostingInterval.asSeconds() * velocityA.y + dy_a
        );

        const math::Rect<float> ghostB(
            rectB.left,
            rectB.top,
            ghostingInterval.asSeconds() * velocityB.x + dx_b,
            ghostingInterval.asSeconds() * velocityB.y + dy_b
        );

        return ghostA.intersects(ghostB);
    }
}
}
