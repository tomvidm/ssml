#include "math/VectorArithmetic.hpp"

#include "physics/DynamicBody.hpp"

namespace ss { namespace physics {
    DynamicBody::DynamicBody()
    : mass(1.f),
      inertialMoment(1.f),
      angularVelocity(0.f),
      velocity(math::Vector2f(0.f, 0.f)) {
        ;
    }

    void DynamicBody::addForce(const Force force, const system::Time& dt) {
        addVelocity(force * dt.asSeconds() / mass);
    }

    void DynamicBody::addMomentum(const Impulse impulse) {
        velocity += (1/mass) * impulse;
    }

    void DynamicBody::addAngularMomentum(const float angularMomentum) {
        angularVelocity += angularMomentum / inertialMoment;
    }

    void DynamicBody::addVelocity(const math::Vector2f vel) {
        velocity += vel;
    }

    void DynamicBody::setVelocity(const math::Vector2f vel) {
        velocity = vel;
    }

    math::Vector2f DynamicBody::getVelocity() const {
        return velocity;
    }

    void DynamicBody::setAngularVelocity(const float radsPerSec) {
        angularVelocity = radsPerSec;
    }

    void DynamicBody::addAngularVelocity(const float radsPerSec) {
        angularVelocity += radsPerSec;
    }

    float DynamicBody::getAngularVelocity() const {
        return angularVelocity;
    }
    
    bool DynamicBody::isAtRest() const {
        return math::magnitudeSquared(velocity) < 0.005;
    }

    void DynamicBody::setMass(const float m) {
        mass = m;
    }

    float DynamicBody::getMass() const {
        return mass;
    }

    void DynamicBody::setInertialMoment(const float moment) {
        inertialMoment = moment;
    }

    float DynamicBody::getInertialMoment() const {
        return inertialMoment;
    }

    void DynamicBody::update(const system::Time& dt) {
        move(velocity * dt.asSeconds());
    }
    
    math::Transform DynamicBody::getFutureTransform(const float dt) const {
        if (isAtRest()) {
            return getTransform();
        } else {
            math::Transformable tempTransformable;
            tempTransformable.setPosition(getPosition() + dt * getVelocity());
            tempTransformable.setRotation(getRotation() + dt * getAngularVelocity());
            return tempTransformable.getTransform();
        }
    }
}
}