#include "physics/PhysicsEvent.hpp"
#include <iostream>

namespace ss { namespace physics {
    CollisionEvent::CollisionEvent()
    : bodyA(nullptr),
      bodyB(nullptr),
      timeToImpact(system::milliseconds(0)) {;}

    CollisionEvent::CollisionEvent(DynamicBody::shared bodyA, DynamicBody::shared bodyB, system::Time dt)
    : bodyA(bodyA),
      bodyB(bodyB),
      timeToImpact(dt) {;}

    CollisionEvent::CollisionEvent(const CollisionEvent& other)
    : bodyA(other.bodyA),
      bodyB(other.bodyB),
      timeToImpact(other.timeToImpact) {;}

    CollisionEvent::CollisionEvent(CollisionEvent&& other)
    : bodyA(other.bodyA),
      bodyB(other.bodyB),
      timeToImpact(other.timeToImpact) {
          other = CollisionEvent();
    }

    CollisionEvent::~CollisionEvent() {
        ;
    }

    CollisionEvent CollisionEvent::operator=(const CollisionEvent& other) {
        bodyA = other.bodyA;
        bodyB = other.bodyB;
        timeToImpact = other.timeToImpact;
        return *this;
    }

    Event::Event()
    : type(EventType::None) {;}

    Event::Event(const CollisionEvent& event)
    : type(EventType::Collision) {
        // This assignment causes segmentation fault
        collisionEvent = event;
    }

    Event::Event(const Event& other)
    : type(other.type) {
        switch (type) {
            case EventType::None:
                break;
            case EventType::Collision:
                collisionEvent = other.collisionEvent;
                break;
        }
    }

    Event::Event(Event&& other)
    : type(other.type) {
        switch (type) {
            case EventType::None:
                break;
            case EventType::Collision:
                collisionEvent = other.collisionEvent;
                break;
        }

        other = Event();
    }

    Event::~Event() {
        ;
    }

    Event Event::operator=(const Event& other) {
        switch (other.type) {
            case EventType::None:
                break;
            case EventType::Collision:
                collisionEvent = other.collisionEvent;
                break;
        }
        return *this;
    }

    Event makeCollisionEvent(DynamicBody::shared bodyA, DynamicBody::shared bodyB, system::Time dt) {
        return Event(CollisionEvent(bodyA, bodyB, dt));
    }

    CollisionEvent Event::getCollisionEvent() const {
        if (type == EventType::Collision) {
            return collisionEvent;
        } else {
            return CollisionEvent();
        }
    }
}
}