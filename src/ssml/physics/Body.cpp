#include "physics/Body.hpp"

namespace ss { namespace physics {
    Body::Body() {
        ;
    }

    math::Rect<float> Body::getLocalBounds() const {
        return collisionMesh->getBoundingRect();
    }

    math::Rect<float> Body::getGlobalBounds() const {
        return getTransform().transformRect(collisionMesh->getBoundingRect());
    }

    void Body::setCollisionMesh(CollisionMesh::shared mesh) {
        collisionMesh = mesh;
    }

    const CollisionMesh::shared Body::getSharedCollisionMesh() const {
        return collisionMesh;
    }

    const CollisionMesh* Body::getRawCollisionMesh() const {
        return collisionMesh.get();
    }
}
}