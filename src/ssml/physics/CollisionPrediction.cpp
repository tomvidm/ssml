#include "physics/CollisionPrediction.hpp"

#include "math/VectorArithmetic.hpp"

#include <iostream>

namespace ss { namespace physics {
    Event predictCollision(
        const DynamicBody::shared bodyA,
        const DynamicBody::shared bodyB,
        const system::Time dt
    ) {
        const int numIterations = 3;
        float dt_div = dt.asSeconds() / 2.f;
        for (int i = 0; i < numIterations; i++) {
            if (checkIntersection(
                bodyA->getRawCollisionMesh(),
                bodyB->getRawCollisionMesh(),
                bodyA->getFutureTransform(dt_div),
                bodyB->getFutureTransform(dt_div)
            )) {
                dt_div += dt_div / 2.f;
            } else {
                dt_div -= dt_div / 2.f;
            }
        }

        return makeCollisionEvent(bodyA, bodyB, system::seconds(dt_div));
    }

    bool checkTunneling(
        const DynamicBody::shared bodyA,
        const DynamicBody::shared bodyB,
        const system::Time dt
    ) {
        const math::Vector2f diffPos = bodyA->getPosition() - bodyB->getPosition();
        const math::Vector2f diffVel = bodyA->getVelocity() - bodyB->getVelocity();

        const float dotPP = math::dot(diffPos, diffPos);
        const float dotPV = math::dot(diffPos, diffVel);
        const float dotVV = math::dot(diffVel, diffVel);

        if (dotPV > 0) {
            if (dotPV + dt.asSeconds() * dotVV) {
                return true;
            }
        } else {
            return false;
        }
    }
}
}