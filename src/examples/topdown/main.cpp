#include "SFML/Window.hpp"

#include <iostream>
#include <cmath>

#include "TopdownTestbed.hpp"

int main() {
    using namespace ss;

    ss::TopdownTestbed app;

    app.run();

    return 0;
}
