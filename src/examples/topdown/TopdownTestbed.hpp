#pragma once

#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"

#include "input/Input.hpp"
#include "graphics/Tileset.hpp"
#include "graphics/Sprite.hpp"
#include "system/Renderer.hpp"

#include "Player.hpp"

namespace ss {
    class TopdownTestbed {
    public:
        TopdownTestbed();

        void run();
    private:
        void setup();
        void handleInput();
        void update(const system::Time dt);
        void render();

        void handleKeyboardEvent(input::KeyboardKeyEvent keyboardEvent);

        Player player;
        sf::RenderWindow window;
        input::InputHandler inputHandler;
        graphics::SharedTileset tileset;
        system::Renderer renderer;
        system::Clock renderTimer;
        system::Clock updateTimer;

        const system::Time renderInterval;
    };
}