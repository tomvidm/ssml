#include "Player.hpp"

#include <iostream>

namespace ss {
    void Player::onKeyboardKeyEvent(const input::KeyboardKeyEvent event) {
        if (state == PlayerState::Attacking) {
            return;
        } else {
            if (event.getType() == input::KeyboardKeyEventType::Pressed) {
                playerHasUpdatedState = true;
                state = PlayerState::Walking;
                switch (event.getKey()) {
                    case input::KeyboardKey::Up:
                        facingSum += static_cast<int>(Facing::Up);
                        facing = Facing::Up;
                        break;
                    case input::KeyboardKey::Left:
                        facingSum += static_cast<int>(Facing::Left);
                        facing = Facing::Left;
                        break;
                    case input::KeyboardKey::Down:
                        facingSum += static_cast<int>(Facing::Down);
                        facing = Facing::Down;
                        break;
                    case input::KeyboardKey::Right:
                        facingSum += static_cast<int>(Facing::Right);
                        facing = Facing::Right;
                        break;
                    case input::KeyboardKey::Space:
                        state = PlayerState::Attacking;
                        break;
                }
            } else if (event.getType() == input::KeyboardKeyEventType::Released) {
                playerHasUpdatedState = true;
                switch (event.getKey()) {
                    case input::KeyboardKey::Up:
                        facingSum -= static_cast<int>(Facing::Up);
                        break;
                    case input::KeyboardKey::Left:
                        facingSum -= static_cast<int>(Facing::Left);
                        break;
                    case input::KeyboardKey::Down:
                        facingSum -= static_cast<int>(Facing::Down);
                        break;
                    case input::KeyboardKey::Right:
                        facingSum -= static_cast<int>(Facing::Right);
                        break;
                    case input::KeyboardKey::Space:
                        state = PlayerState::Attacking;
                        break;
                }
                if (facingSum == 0) {
                    state = PlayerState::Idle;
                }
            }
        }
    }

    void Player::update(const system::Time dt) {
        playerSprite->update(dt);
        if (playerHasUpdatedState) {
            switch (state) {
                case PlayerState::Idle:
                    updateIdle();
                    break;
                case PlayerState::Walking:
                    updateWalking();
                    break;
                case PlayerState::Attacking:
                    updateAttacking();
                    break;
            }
        }
    }

    void Player::onAttackFinish() {
        state = PlayerState::Idle;
        updateIdle();
    }

    void Player::updateIdle() {
        playerHasUpdatedState = false;
        std::cout << "updateIdle" << std::endl;
        switch (facing) {
            case Facing::Up:
                playerSprite->setStaticSprite(tileset->getTextureRect("idle_up"));
                break;
            case Facing::Left:
                playerSprite->setStaticSprite(tileset->getTextureRect("idle_left"));
                break;
            case Facing::Down:
                playerSprite->setStaticSprite(tileset->getTextureRect("idle_down"));
                break;
            case Facing::Right:
                playerSprite->setStaticSprite(tileset->getTextureRect("idle_right"));
                break;
        }
    }

    void Player::updateWalking() {
        std::cout << "updateWalking" << std::endl;
        playerHasUpdatedState = false;
        switch (facing) {
            case Facing::Up:
                playerSprite->setAnimation(tileset->getAnimation("walk_up"));
                break;
            case Facing::Left:
                playerSprite->setAnimation(tileset->getAnimation("walk_left"));
                break;
            case Facing::Down:
                playerSprite->setAnimation(tileset->getAnimation("walk_down"));
                break;
            case Facing::Right:
                playerSprite->setAnimation(tileset->getAnimation("walk_right"));
                break;
        }
    }

    void Player::updateAttacking() {
        std::cout << "updateAttacking" << std::endl;
        playerHasUpdatedState = false;
        switch (facing) {
            case Facing::Up:
                playerSprite->setAnimation(tileset->getAnimation("attack_spear_up"));
                break;
            case Facing::Left:
                playerSprite->setAnimation(tileset->getAnimation("attack_spear_left"));
                break;
            case Facing::Down:
                playerSprite->setAnimation(tileset->getAnimation("attack_spear_down"));
                break;
            case Facing::Right:
                playerSprite->setAnimation(tileset->getAnimation("attack_spear_right"));
                break;
        }
    }
}