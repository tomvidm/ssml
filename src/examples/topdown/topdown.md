# Topdown showcase
This program showcases the sprite animations

## The player
The player can do a few things:

* When a direction key is pressed:
  * if player state is `Attacking`, return
  * the player facing changes to the corresponding direction.
  * the player states to `Walking`
* When a direction key is released:
  * if player state is `Attacking`, return
  * check if any direction keys are still pressed. If true:
    * change facing to the first retained direction, with precedence `up < left < down < right`
  * else:
    * keep facing, but set player state to `Idle`
* When the space bar is pressed:
  * if player state is `Attacking`, do nothing
  * else, set player state to `Attacking`

Any change in state should cause an update in the animation or static sprite.