#pragma once

#include <memory>

#include "input/Input.hpp"
#include "graphics/Sprite.hpp"

namespace ss {
    enum Facing {
        Up = 1,
        Left = 2, 
        Down = 4, 
        Right = 8
    };

    enum PlayerState {
        Idle,
        Walking,
        Attacking
    };

    class Player {
    public:
        Player()
        : playerHasUpdatedState(true),
          state(PlayerState::Idle),
          facing(Facing::Down),
          facingSum(1) {
              playerSprite = std::make_shared<graphics::Sprite>();
          }

        void onKeyboardKeyEvent(const input::KeyboardKeyEvent event);

        inline void acknowledgeCurrentState() { playerHasUpdatedState = false; }
        void update(const system::Time dt);
        void onAttackFinish();
        void updateIdle();
        void updateWalking();
        void updateAttacking();

        inline bool getState() const { return state; }
        inline Facing getFacing() const { return facing; }
        inline graphics::Sprite::shared getSprite() { return playerSprite; }
        inline void setTileset(graphics::SharedTileset newtileset) { 
            tileset = newtileset;
            playerSprite->setTextureFromTileset(*tileset); 
        }
    private:
        bool playerHasUpdatedState;
        PlayerState state;
        graphics::Sprite::shared playerSprite;
        graphics::SharedTileset tileset;
        Facing facing;
        int facingSum;
    };
}