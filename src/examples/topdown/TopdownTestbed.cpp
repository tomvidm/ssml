#include "TopdownTestbed.hpp"

#include <iostream>

namespace ss {
    TopdownTestbed::TopdownTestbed()
    : renderInterval(system::milliseconds(17)),
      window(sf::VideoMode(800, 600), "TopdownTestbed") {
        setup();
    }

    void TopdownTestbed::setup() {
        tileset = std::make_shared<graphics::Tileset>();
        tileset->loadFromJSON("/lpc_entry/xcf/char.json");
        player.setTileset(tileset);
        renderer.addDrawable(*player.getSprite());
        renderer.setRenderWindow(&window);
    }

    void TopdownTestbed::run() {
        while (window.isOpen()) {
            const system::Time dt = updateTimer.restart();
            handleInput();
            update(dt);

            if (renderTimer.getElapsedTime() > renderInterval) {
                render();
            }
        }
    }

    void TopdownTestbed::handleInput() {
        sf::Event sfmlEvent;

        while (window.pollEvent(sfmlEvent)) {
            if (sfmlEvent.type == sf::Event::EventType::Closed) {
                window.close();
            }

            input::InputEvent ssEvent = inputHandler.onEvent(sfmlEvent);

            if (ssEvent.getType() == input::InputEventType::Keyboard) {
                handleKeyboardEvent(ssEvent.getKeyboardKeyEvent());
            }
        }
    }
    
    void TopdownTestbed::handleKeyboardEvent(input::KeyboardKeyEvent keyboardEvent) {
        player.onKeyboardKeyEvent(keyboardEvent);
    }

    void TopdownTestbed::update(const system::Time dt) {
        player.update(dt);
    }

    void TopdownTestbed::render() {
        window.clear();
        renderTimer.restart();
        renderer.renderAll();
        window.display();
    }
}