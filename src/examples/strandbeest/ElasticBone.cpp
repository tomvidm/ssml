#include "ElasticBone.hpp"

#include "math/Vector2.hpp"

namespace ss {
    ElasticBone::ElasticBone(std::shared_ptr<physics::DynamicBody>& body_a,
                std::shared_ptr<physics::DynamicBody>& body_b,
                const float springFactor,
                const float dampingFactor)
    : endpoint_a(body_a),
      endpoint_b(body_b),
      eqLength(math::magnitude(endpoint_b->getPosition() - endpoint_a->getPosition())),
      springFactor(springFactor),
      dampingFactor(dampingFactor) {
        ;
    }

    void ElasticBone::update(const system::Time& dt) {
        const math::Vector2f dr = endpoint_b->getPosition() - endpoint_a->getPosition();
        const math::Vector2f dr_unit = math::normalize(dr);
        const float dr_mag = math::magnitude(dr);

        const physics::Force force = (-1.f) * springFactor * (dr_mag - eqLength) * dr_unit;

        const math::Vector2f vel_a = endpoint_a->getVelocity();
        const math::Vector2f vel_b = endpoint_b->getVelocity();
        const physics::Force damping_on_a = (-1.f) * dampingFactor * math::magnitude(vel_a) * vel_a;
        const physics::Force damping_on_b = (-1.f) * dampingFactor * math::magnitude(vel_b) * vel_b;

        endpoint_a->addForce(-force + damping_on_a, dt);
        endpoint_b->addForce(force + damping_on_b, dt);
    }

    float ElasticBone::getPotential() const {
        const float dr = math::magnitude(getEndpointB() - getEndpointA()) - eqLength;
        return 0.5f * springFactor * dr * dr;
    }

    void ElasticBone::setEquilibriumLength(const float newEquilibrium) {
        eqLength = newEquilibrium;
    }

    void ElasticBone::setSpringFactor(const float newSpringFactor) {
        springFactor = newSpringFactor;
    }

    void ElasticBone::setDampingFactor(const float newDampingFactor) {
        dampingFactor = newDampingFactor;
    }
}