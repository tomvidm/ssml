#pragma once

#include <memory>
#include <vector>

#include "SFML/Graphics.hpp"

#include "physics/DynamicBody.hpp"
#include "system/Time.hpp"

#include "ElasticBone.hpp"

namespace ss {
    class Strandbeest {
    public:
        Strandbeest();
        void attachBody(const std::shared_ptr<physics::DynamicBody>& body);
        void connectBodies(const size_t a, const size_t b, const float springFactor, const float dampingFactor);
        void update(const system::Time& dt);

        inline sf::Drawable* getBoneLines() { return &boneLines; }

        inline ElasticBone& getBone(const size_t i) { return bones[i]; }
    private:
        std::vector<std::shared_ptr<physics::DynamicBody>> joints;
        std::vector<ElasticBone> bones;

        sf::VertexArray boneLines;
    };
}