#pragma once

#include <memory>

#include "physics/DynamicBody.hpp"
#include "system/Time.hpp"

namespace ss {
    class ElasticBone {
    public:
        ElasticBone() = delete;
        ElasticBone(std::shared_ptr<physics::DynamicBody>& body_a,
                    std::shared_ptr<physics::DynamicBody>& body_b,
                    const float springFactor,
                    const float dampingFactor);

        // Calculate forces and apply forces to attached bodies.
        void update(const system::Time& dt);

        inline math::Vector2f getEndpointA() const { return endpoint_a->getPosition(); }
        inline math::Vector2f getEndpointB() const { return endpoint_b->getPosition(); }

        float getPotential() const;

        inline float getEquilibriumLength() const { return eqLength; }
        void setEquilibriumLength(const float newEquilibrium);
        void setSpringFactor(const float newSpringFactor);
        void setDampingFactor(const float newDampingFactor);
    private:
        std::shared_ptr<physics::DynamicBody> endpoint_a;
        std::shared_ptr<physics::DynamicBody> endpoint_b;

        float eqLength;
        float springFactor;
        float dampingFactor;
    };
}