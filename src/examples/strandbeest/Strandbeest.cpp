#include "Strandbeest.hpp"

namespace ss {
    Strandbeest::Strandbeest() {
        boneLines.setPrimitiveType(sf::PrimitiveType::Lines);
    }

    void Strandbeest::attachBody(const std::shared_ptr<physics::DynamicBody>& body) {
        joints.push_back(body);
    }

    void Strandbeest::connectBodies(const size_t a, const size_t b, const float springFactor, const float dampingFactor) {
        bones.push_back(ElasticBone(
            joints[a], joints[b], springFactor, dampingFactor
        ));

        boneLines.append(graphics::Vertex());
        boneLines.append(graphics::Vertex());
    }

    void Strandbeest::update(const system::Time& dt) {
        for (auto& bone : bones) {
            bone.update(dt);
        }

        for (auto& joint : joints) {
            joint->update(dt);
        }

        for (size_t i = 0; i < bones.size(); i++) {
            const ElasticBone& bone = bones[i];
            boneLines[2*i].position = bone.getEndpointA();
            boneLines[2*i + 1].position = bone.getEndpointB();
        }
    }
}