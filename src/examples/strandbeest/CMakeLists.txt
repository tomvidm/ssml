project(ssml-strandbeest-example)

add_executable(ssml-strandbeest-example
    main.cpp
    ElasticBone.cpp
    Strandbeest.cpp
)

include_directories(
    ${Boost_INCLUDE_DIRS}
)

target_link_libraries(
    ssml-strandbeest-example
    ssml-graphics
    ssml-math
    ssml-system
    ssml-physics
    ssml-input
)

set_target_properties(ssml-strandbeest-example
    PROPERTIES 
    RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin
)