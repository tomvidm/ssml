#include <memory>
#include <iostream>

#include "system/Engine.hpp"
#include "input/Input.hpp"

#include "Strandbeest.hpp"
#include "system/Engine.hpp"


int main() {
    using ss::physics::DynamicBody;
    using ss::math::Vector2f;

    ss::system::EngineConfig config = ss::system::EngineConfig::loadFromJSON("../resources/configs/config.json");
    ss::system::Engine engine(config);

    ss::Strandbeest sb;
    std::shared_ptr<DynamicBody> body_a = std::make_shared<DynamicBody>();
    std::cout << body_a->getPosition().x << std::endl;
    std::shared_ptr<DynamicBody> body_b = std::make_shared<DynamicBody>();
    std::shared_ptr<DynamicBody> body_c = std::make_shared<DynamicBody>();

    body_a->setPosition(Vector2f(100.f, 100.f));
    body_b->setPosition(Vector2f(300.f, 300.f));
    body_c->setPosition(Vector2f(500.f, 100.f));
    body_c->setMass(100000.f);

    sb.attachBody(body_a);
    sb.attachBody(body_b);
    sb.attachBody(body_c);
    
    sb.connectBodies(0, 1, 1.f, 0.04f);
    sb.connectBodies(1, 2, 1.f, 0.04f);
    sb.connectBodies(2, 0, 1.f, 0.04f);

    sb.getBone(0).setEquilibriumLength(sb.getBone(0).getEquilibriumLength() * 0.9f);
    sb.getBone(1).setEquilibriumLength(sb.getBone(1).getEquilibriumLength());
    sb.getBone(2).setEquilibriumLength(sb.getBone(2).getEquilibriumLength());

    engine.getRenderer().addDrawable(sb.getBoneLines());

    while (engine.isRunning()) {
        const ss::math::Vector2f mpos = engine.getInputHandler().getMouseState().getPosition();
        const ss::system::Time dt = engine.perFrameUpdate();

        sb.update(dt);
        std::cout << body_a->getPosition().x << std::endl;
        engine.render();
    }
    return 0;
}