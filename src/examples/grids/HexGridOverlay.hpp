#pragma once

#include <unordered_map>

#include "SFML/Graphics.hpp"

#include "math/Hex.hpp"
#include "math/HexConfiguration.hpp"
#include "graphics/Renderable.hpp"
#include "graphics/Vertex.hpp"
#include "graphics/VertexHandle.hpp"
#include "graphics/HexBuilder.hpp"

namespace ss {
    using graphics::Color;
    using graphics::Vertex;
    using graphics::VertexBuffer;
    using graphics::HexBuilder;
    using graphics::HexHandle;
    class HexGridOverlay 
    : public graphics::Renderable {
    public:
        HexGridOverlay(const math::HexConfiguration& config);

        void addHex(const math::Hex<int> hex);

        void updateVertices();
        void updateHexHandles();

        HexHandle& getHandle(const math::Hex<int> hex);

        void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        inline const math::Rect<float> getLocalBounds() const { return math::Rect<float>(0.f, 0.f, 1.f, 1.f); }
        inline const math::Rect<float> getGlobalBounds() const { return math::Rect<float>(0.f, 0.f, 1.f, 1.f); };
    private:
        std::vector<Vertex> vertices;
        std::vector<HexHandle> hexHandles;
        std::unordered_map<math::Hex<int>, size_t> hexHandleIndices;
        VertexBuffer vbuf;

        const math::HexConfiguration config;
    };
}