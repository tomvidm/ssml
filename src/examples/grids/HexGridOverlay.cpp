#include "HexGridOverlay.hpp"

#include <iostream>

namespace ss {
    HexGridOverlay::HexGridOverlay(const math::HexConfiguration& config) 
    : vbuf(sf::PrimitiveType::Quads),
      config(config) {
        ;
    }

    void HexGridOverlay::addHex(const math::Hex<int> hex) {
        if (hexHandleIndices.count(hex) > 0) {
            return;
        }

        const size_t index = hexHandles.size();
        hexHandleIndices.insert(std::pair<math::Hex<int>, size_t>(hex, index));
        vertices.resize(8 * index + 8);
        hexHandles.push_back(
            HexBuilder::buildHex(
                &vertices.data()[8 * index],
                config.hexToVector(hex),
                config
            )
        );

        hexHandles.back().setColor(Color(255, 255, 255, 128));
        updateVertices();
    }

    void HexGridOverlay::updateVertices() {
        vbuf.create(vertices.size());
        vbuf.update(vertices.data());
    }

    void HexGridOverlay::updateHexHandles() {
        for (size_t i = 0; i < hexHandles.size(); i++) {
            hexHandles[i] = graphics::HexHandle(&vertices.data()[8 * i]);
        }
    }

    void HexGridOverlay::draw(sf::RenderTarget& target, sf::RenderStates states) const {
        states.transform *= getTransform();
        target.draw(vbuf, states);
    }
}