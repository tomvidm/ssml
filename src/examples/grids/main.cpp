#include "system/Engine.hpp"

#include <iostream>
#include <memory>

#include "graphics/HexGrid.hpp"
#include "graphics/ConvexPolygon.hpp"
#include "math/HexConfiguration.hpp"
#include "math/Hex.hpp"
#include "math/ConvexPolygon.hpp"
#include "math/Vector2.hpp"
#include "graphics/SpriteText.hpp"

#include "HexGridOverlay.hpp"

int main() {
    using namespace ss;
    using graphics::HexGrid;
    using math::HexConfiguration;
    using math::Hex;
    using math::Vector2f;
    using graphics::Color;
    using graphics::ConvexPolygon;
    using graphics::SpriteText;
    system::EngineConfig config = ss::system::EngineConfig::loadFromJSON("../resources/configs/config.json");
    system::Engine engine(config);

    const size_t U = 128;
    const size_t V = 128;

    HexConfiguration hexConfig = HexConfiguration::makeFlatTop(32.f, 20.f, 32.f);
    std::shared_ptr<HexGrid> hgrid = std::make_shared<HexGrid>(U, V, hexConfig);
    std::shared_ptr<HexGridOverlay> overlay = std::make_shared<HexGridOverlay>(hexConfig);
    std::shared_ptr<SpriteText> text = std::shared_ptr<SpriteText>(new SpriteText());

    math::ConvexPolygon circMesh = math::generateCircle(20, 8.f);
    graphics::ConvexPolygon circ(circMesh);

    hgrid->setPosition(Vector2f(-64.f, -512.f));
    overlay->setPosition(Vector2f(-64.f, -512.f));


    Hex<int> currentMouseoverHex = Hex<int>(0, 0);
    Hex<int> currentSelectionHex = Hex<int>(0, 0);
    Color defaultColor = Color(128, 128, 255);
    Color mouseoverColor = Color(255, 128, 128);
    Color selectionColor = Color(255, 64, 64);

    for (size_t u = 0; u < U; u++) {
        for (size_t v = 0; v < V; v++) {
            hgrid->setHexColor(u, v, defaultColor);
        }
    }

    text->setString(std::string(
        "hello\nworld\ni\nam\nweaselboi"
    ));

    engine.getRenderer().addRenderable(hgrid);
    engine.getRenderer().addRenderable(overlay);
    engine.getRenderer().addRenderable(text);

    while (engine.isRunning()) {
        const system::Time time = engine.perFrameUpdate(); // Returns time since last frame
        const Vector2f mpos = engine.getInputHandler().getMouseState().getPosition();
        const Hex<int> newMouseover = hgrid->getHexFromGlobal(mpos);
        if (newMouseover.getU() >= 0 && newMouseover.getU() < U &&
            newMouseover.getV() >= 0 && newMouseover.getV() < V) {
            hgrid->setHexColor(currentMouseoverHex, defaultColor);
            currentMouseoverHex = newMouseover;
        }
        for (auto& event : engine.getInputEvents()) {
            if (event.isLeftClick()) {
                hgrid->setHexColor(currentSelectionHex, defaultColor);
                currentSelectionHex = newMouseover;
                overlay->addHex(newMouseover);
            }
        }
        hgrid->setHexColor(currentMouseoverHex, mouseoverColor);
        hgrid->setHexColor(currentSelectionHex, selectionColor);
        circ.setPosition(mpos);
        engine.render();
    }
    return 0;
}