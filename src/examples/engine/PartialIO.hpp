# pragma once

#include <unordered_map>

#include "math/Vector2.hpp"

// Partial IO
// For large maps, where a lot of changes to a procedurally generate map
// are stored sparsely, it makes no sense to store each data point individually.
// Due to linear growth of file size, some mechanism may (may, because no profiling is done)
// be needed to quickly find a data point, sublinearly.
//
// Let the World be a key-value map of (x, y) -> Chunk mappings.
// Let a chunk be a small rectangular (U, V) size section of the 2D World.
// For one World, all the chunks have the same (U, V) size.
// Each chunk has its data points generated procedurally. However, changes to the points
// will be stored separately. Therefore, the world is defined by a seed, the generating algorithm
// and the changes done afterwards.
// 
// The Partial IO system shall be template based, letting each Data point be any class, as long
// as the class has defined the methods toBinary and fromBinary.
// Upon reading a World-File, the header of the binary World file will specify how many chunks it
// keeps books on. Each Chunk will be successively read. Relevant data, such as position in the binary file
// should be stored in a hash map, with hash(u, v) -> chunk_binary_pos mapping.
// Upon any query, the Partial IO should consult the map to determine where in the file to look.

namespace ss {
    class Data {
    public:
        std::vector<char> toBinary() const;
        static Data fromBinary(char const * binData);
    private:
        int data;
    };

    template <typename T>
    class Chunk {
    public:
        std::vector<char> toBinary() const;
        static Chunk fromBinary(char const * binData);
    private:
        math::Vector2i chunkOrigin;
        std::unordered_map<math::Vector2i, T> chunkDelta;
    };
}