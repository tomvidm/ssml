### Superchunks
Using one monolithic file can be a problem when writing new data. If the application has some real time requirements, writing new data will in many cases require rewriting everything, as each chunk can take up variable amount of space.

Therefore, it might be more sensible to store "chunks of chunks". The "superchunk" should not exists inside the logic of the World, and is purely an additional abstraction for the WorldRW, for choosing the appropriate file for reading and writing.

### `class World<T>`
A World is a representation of an infinite 2D plane with integer coordinates mapping to some instance data type `T`. The data type can for example refer to some type of terrain.

The tile `T` is the smallest meaningful unit of the world. The World is divided into rectangular tilegrids. Each section is called a chunk. A chunk is either fully defined, or procedurally generated and modified later by a change (chunk delta).

#### Information in Header
* seed
* number of chunks
* size per chunk (X, Y)

### `class WorldRW<T>`
Storing every single chunk in working memory is not a solution. Some system needs to be in place, to allow for storing and writing to disk specific chunks. For procedurally generated worlds, the space requirements are only dependent on the explicitly modified chunks. 

Upon reading a world file, dictionary will be created, mapping chunk coordinate hashes to positions in the binary file where the chunk data is stored. This will circumvent linear search, and is a premature optimization - but not evil hehhhhhh.

### `class Chunk<T>`
A chunk is simply a 2D array of `<T>`, but with additional information such as the origin (`Vector2i`) to allow for easy conversion between global and local coordinates.

#### Information in header
* chunk origin (x, y)
* size of chunk (X, Y)
* `sizeof(T)`

### `class ChunkDelta<T>`
A ChunkDelta wraps a `unordered_map<Vector2i, T>`. It can be applied to a Chunk to overwrite data.
 The `World<T>` wraps an `unordered_map<Vector2i, T>`

#### Information in header
* chunk origin (x, y)
* number of elements in unordered_map
* `sizeof(T)`

 ### `class ChunkRW<T>`

