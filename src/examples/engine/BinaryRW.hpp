#pragma once

#include <vector>

namespace ss {
    template <typename T>
    class BinaryRW {
    public:
        virtual std::vector<char> toBinary() const = 0;
        static T fromBinary(char * binary);
    };

    class Chunk : public BinaryRW<Chunk> {

    };
}