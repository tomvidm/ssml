#include "InteractiveGrid.hpp"

namespace ss {
    InteractiveGrid::InteractiveGrid(
        const size_t usize,
        const size_t vsize,
        const graphics::HexConfig& config)
    : hexConfig(config),
      hexgrid(usize, vsize, config),
      mouseoverHex(math::Hex<int>(-1, -1)),
      lastMouseoverHex(math::Hex<int>(-1, -1)) {;}

    void InteractiveGrid::updateMouseover(const math::Vector2f mpos) {
        lastMouseoverHex = mouseoverHex;
        mouseoverHex = hexgrid.getHexFromGlobal(mpos);

        updateTileColors();
    }

    void InteractiveGrid::updateTileColors() {
        if (mouseoverHex.getU() >= 0 &&
            mouseoverHex.getU() < hexgrid.getSizeU() &&
            mouseoverHex.getV() >= 0 &&
            mouseoverHex.getV() < hexgrid.getSizeV()) 
        {
            hexgrid.setHexColor(lastMouseoverHex, graphics::Color::White);
            hexgrid.setHexColor(mouseoverHex, graphics::Color::Red);
        }
    }
}