#pragma once

#include "math/Vector2.hpp"
#include "math/Hex.hpp"
#include "graphics/HexGrid.hpp"

namespace ss {
    class InteractiveGrid {
    public:
        InteractiveGrid(
            const size_t usize,
            const size_t vsize,
            const graphics::HexConfig& config);
        
        inline graphics::HexGrid& getHexGrid() { return hexgrid; }
        void updateMouseover(const math::Vector2f mpos);
    private:
        void updateTileColors();

        graphics::HexConfig hexConfig;
        graphics::HexGrid hexgrid;
        math::Hex<int> mouseoverHex;
        math::Hex<int> lastMouseoverHex;
    };
}