#include "Engine.hpp"
#include "graphics/HexGrid.hpp"
#include "input/Input.hpp"
#include "InteractiveGrid.hpp"

#include <iostream>

int main() {
    ss::EngineConfig config = ss::EngineConfig::loadFromJSON("../resources/configs/config.json");
    ss::Engine engine(config);

    while (engine.isRunning()) {
        const ss::math::Vector2f mpos = engine.getInputHandler().getMouseState().getPosition();
        engine.perFrameUpdate();
        engine.render();
    }
    return 0;
}
