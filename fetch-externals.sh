#!/bin/bash
mkdir external
cd external
git clone https://github.com/Tencent/rapidjson.git
git clone https://github.com/google/googletest.git
git clone https://github.com/Auburns/FastNoise.git
echo "Finished cloning externals!"