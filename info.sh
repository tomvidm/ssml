#!/bin/bash

echo "Number of lines in .cpp files:"
find src/ -name '*.cpp' | xargs wc -l | grep total

echo "Number of lines in .hpp files:"
find include/ -name '*.hpp' | xargs wc -l | grep total