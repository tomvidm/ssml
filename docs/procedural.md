# Stochastic Variables and Procedural Generation
## Procedural generation
SSML shall provide a simple and flexible interface for procedural generation of data.

### Use cases

* Easily customizable discrete distributions.
* Perlin noise generation of heightmaps
* The user shall be able to use heightmaps as probability distributions.



### Reproducibility of results
The `math::rand(key, seed)` function gives a reproducible random number, that will be equal for each call with the same arguments.

