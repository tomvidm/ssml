## Input Broadcaster
This class shall, upon receiving an event, broadcast this event to all `Listener' objects that are attached.

* Each `K` maps to one or more `V`.
* More than one `K` can map to the same value.
* Conversely, each instance of a listening `V` shall map all `K` instances that references it.
* When asked to remove some value `V`, the structure should remove all references to it in the `K-V` map, and also the entry in the `V-K` map.
* The point above also applies to the other direction.