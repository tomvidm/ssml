# GUI system
## Widget
The widget is the base gui class, from which all other gui widgets are derived.
The gui defines:
* The drawable area
* Event response

The gui is also an event listener, and can subscribe to events (usually mouse events). 

The widget implements the WigetShape, which is just a wrapper for a vertex buffer. Each widget is made up of 9 quads.

The widget should also broadcast GUI events, for such actions as:
* Closing the widget
* Resizing it
* Moving it by dragging
* Snapping to another widget
* Widget is brought into focus