# Graphics Module
## Renderer
The renderer simplifies the rendering of renderable instances. The user interacts with the renderer simply by adding shared pointers to renderable instances. The renderer handles cleanup, draw order and more.

To make the renderer more flexible, the user can specify render layers, which takes care of the cleanup and draw order. The render layers are publicly accessible through string keys, allowing the user to show/hide specific layers, as well as specifying their ordering.

### Preventing duplicate entries
1. Ask the user to take care
2. Keep a map that keeps track of a `<Renderable*, RenderLayer*>` mapping, so that any addition queries this map to prevent duplicates. A hash map should be fast and the memory footprint minimal.

## RenderLayer
The render layer stores a vector `std::shared_ptr<Renderable>`. The renderer interacts with the render layers upon rendering in the following way:

1. `cleanRenderables()` - Removes any renderable pointer with a use_count equal to 1.
2. `updateRenderList(Rect<float>)` - Updates the render list with all the renderables that intersects the culling rect.
3. `render(RenderWindow*)` - Render to the window

## Graphics assets
Graphics assets such as textures, texture atlases and spritesheets should be wrapped in one unified class.

## Error texture
Assets that use textures should have a plan B, should some texture file be unloaded or the loading has failed. This should be the error texture, and will be ripped off from Valve games. A pink and black checkerboard texture.

## SpriteText
This class should let the user draw text by using sprites instead of fonts. 

## Tileset
The Tileset class holds a shared pointer to a texture, and allows the user to store subrects of the texture in a vector, and map string aliases to their respective indices. The purpose of this class is to allow retrieving texture rects easily:

The below example shows how to use the Tileset with a Tilemap:
```c++
SharedTileset tileset = std::make_shared<Tileset>();
tileset->loadFromJSON("grassland_spritesheet.json");

Tilemap tilemap(8, 8, math::Vector2f(32.f, 32.f), &tileset);
```
## QuadArray
**Derives from: Renderable**

The QuadArray represents an array of quads with arbitrary relative transforms. The QuadArray is dynamically resized, and any quad can be easily removed or added, but in linear time. An optional stirng mapping to indices should be provided with the interface. Best for a large amount of quads with few removals.

## QuadMap
**Derives from: Renderable**

The QuadMap represents an array of quads with arbitrary relative transforms. The QuadArray is dynamically resized, and any quad can be easily removed or added, but in logarithmic time. (Use of map instead of unordered_map, to allow for quick iterations over the elements). The QuadMap uses a string mapping for providing access to elements. Best for relatively few quads (Due to the cost of maintaining the heap property of binary trees) if there is a lot of removals and insertions.

## QuadGrid
**Derives from: Renderable**

The QuadGrid represents a 2D grid of uniformly spaced quads. The user is provided with an interface for setting the colour and texture rectangle of each individual tile, both with and without immediate copying of the vertices to the GPU.

The QuadGrid does not have any internal texture pointer. Textures must be passed to the draw call manually.

The QuadGrid uses the SFML 2.5.0 feature VertexBuffer to avoid the problem with VertexArrays that copies the entire vertex array to the vertex buffer for each draw call.

For an `R x C` grid of quads, the QuadGrid stores `4 * (R * C)` vertices in a vector, and only updates the parts of the vertex buffer that are changed.

## Tilemap 
**Derives from: QuadGrid**

The Tilemap is an extension of the QuadGrid. The Tilemap holds a shared pointer to a Tileset, and provides methods to set texture rects based on the Tileset. The Tilemap also uses the Tileset texture and passes it to the draw call.

## CompositeTilemap
**Derives from: Renderable**
**Implements: Tilemap**

The Composite tilemap class has a similar interface to Tilemap. 

All calls to modify til (x, y) will be performed on the Tilemap that bounds that coordinate.

Culling is performed by the `updateVisibility(Rect<float> visible)` method. The caller should have access to the visibility rect and pass it to this method. More elegant handling of this problem should be considered.

Cached Tilemap instances are stored in a cache based on a `math::Vector2i` coordinate. The CompositeTilemap should have a mechanism of handling operations attempted on unloaded Tilemap instances.

Tilemaps are stored as shared pointers in an unordered map. During the `updateVisibility` procedure, coordinates to tilemaps with bounds that intersect the provided visibility rect shall be stored to an internal list. This list will be used on each draw call.