# Tilesets

## Tileset
Each tileset stores all the subsprites, in an array with optional string mapping.
The tileset should contain the following:
* The subsprites
* Any animations

### Subsprite
The subsprite stores the pixel information of the subsprite
* The top-left origin of the subsprite
* The size of the subsprite

### Animations
Each tileset can store a number of sprite animations. Animations are arrays of animation frames stored in memory, and accessible through a string mapping. Each animation frame should contain the following:
* The id of the subsprite it uses.
* The duration of the frame, in milliseconds.

## Loading tilesets
Loading Tilesets is simple. Tilesets are stored in JSON files, and the loading procedure will depend on the type of tileset.
For regularly spaced tiles, JSON format A is used.
For irregularly spaced tile, JSON format B is used.
### Format A
For regularly spaced subrects, there is still the chance of the designer having black tiles. No reason to load these, so row-column pairs are used instead for referencing tiles. An id should be given each subsprite after loading, because animations also need to reference them, but through the row-column pair.
```json
{
    "texture_file": "char_leather_spear.png",
    "tileset_format": "regular",
    "subsprite_spacing_x": 0,
    "subsprite_spacing_y": 0,
    "subsprite_size_x": 32,
    "subsprite_size_y": 32,
    "subsprites": [
        {
            "type": "single",   # A single sprite.
            "alias": "idle_up", # What string will map to this subsprite.
            "tile": [8, 0]      # The row-column coordinate.
        },
        {  
            "type": "multiple",    # A group of related sprites
            "alias": "walk_up", # What string will map to this subsprite.
            "tiles": [
                [8, 1],
                [8, 2],
                [8, 3],
                [8, 4],
                [8, 5],
                [8, 6],
                [8, 7],
                [8, 8]
            ]
        }
    ]
}
```

### Format B
```json
{
    "texture_file": "char_leather_spear.png",
    "tileset_format": "irregular",
    "number_of_subsprites": 1,
    "subsprites": [
        {
            "id": 0,
            "alias": "subsprite01",
            "origin_x": 0,
            "origin_y": 0,
            "size_x": 32,
            "size_y": 32
        }
    ],
    "animations": [
        {
            "id": 0,
            "alias": "test",
            "frames": [
                {"subsprite_id": 0, "duration": 100},
                {"subsprite_id": 0, "duration": 100}
            ]
        }
    ]
}
```