## Resource Loading & Generation
Whne loading or generating expensive resources on the main thread in a real time application, the blocking time may be undesirable. A mechanism must be in place to reliably perform loading and generation on a separate thread (or multiple). 

In the context of procedurally generated resources, this is important in cases where loading happens near continuously. (Ex: Scrolling in a procedurally generated 2D world.)

```c++
template <typename T>
class ExpensiveResource {
public:
    T& get();
    T get() const;
    const T& get() const;

    void loadResource()         // Details of loading provided by type T

    bool isLoading() const;     // resourceData is locked
    bool isLoaded() const;      // resourceData is unlocked and m_isSuccessfullyLoaded
private:
    bool m_isSuccessfullyLoaded;
    static T m_defaultData;     // Data to return if resourceData is locked
    T m_resourceData;           // Data to return if resourceData is unlocked
};
```