## Purpose
The Engine class encapsulates all the tasks such as input handling, physics and rendering. An engine instance should be what the user interacts with, and provides the following public methods:
* A getter for the renderer: To allow for some control of the rendering process, the user is encourages to access the renderer themself to add or remove renderables, manage render layers and views.
* A getter for the input handler: The input handler is only modified by SFML Events. The user is only provided with the results of this, and the internal states are read only.
* A pre-rendering update method is provided, which returns the time since last frame.

The Engine class is not a "game engine". It is a component that handles input and rendering. Probably, the class should be renamed to avoid miscommunication about the intent of the component. 