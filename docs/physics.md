## Special cases
**A body A collides with body B and C at the same time:**
To avoid doing a collision resolution for A more than once, the system should keep track of whether or not a given body is already involved in a collision event.

Procedure:
```
1. Body A will collide with body B.
  1. A and B are not involved in a collision event
  2. Body A and B are flagged as involved in a collision event.
2. Body B will collide with body C.
  1. Body A is already involved in a collision event.
  2. Body C is flagged as involved in a collision event.
3. For each valid collision event, resolve the event.
```

**Avoid collision events is the dot product of the normal and velocity is 0:**
In the case of something resting on a surface, do not resolve a collision if the dot product of the velocity and the surface normal is 0.

## Constant forces
A system shall optionally lte the user specify a constant force for a system. This constant force will affect all moving and resting bodies attached to the system.

# Right Body states
The rigid body should can be in three general states:
`Moving, Resting and Static`
These states determine how the physics system decides which pairs to compare. At each update interval, the physics system will:

* Compare every moving body A with every other moving body M
* Compare every moving body A with every resting body R
* Compare every moving body A with every static body S
* Process every predicted collision between for pairs (A, M), (A, R) and (A, S)
  * For cases (A, M) and (A, R), always check their resulting velocities to determine if they should be moved to appropriate array, and to update their state variables.
* Update position of all moving bodies

```
Dynamic-Dynamic
> A is moving and stored in dynamicBodies array
> B is moving and stored in dynamicBodies array
> A and B collides
> Collision resolution for A and B

Dynamic-Static
> A is moving and stored in dynamicBodies array
> B is static and stored in staticBodies array
> A collides with B
> Collision resolution for A

Dynamic-Resting
> A is moving and stored in dynamicBodies array
> B is resting and stored in restingBodies array
> A collides with B
> Collision resolution for A and B
> B is moved into dynamicBodies array if resulting velocity is high enough

Dynamic-Forced
> A is moving and stored in dynamicBodies array
> B is being forced and stored in restingBodies array
>> H is the history of the last 3-4 transforms of the body
>> H is used to approximate and set the velocity of A to be used in collision resolution

```

## Motion caused by other means
Let's say an object is set to follow the transform of a mouse cursor. With collision detection in mind; how should 