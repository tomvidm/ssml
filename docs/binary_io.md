# Binary IO
For many purposes, storing data in a JSON format is both fast, convenient and human readable. However, as the size of the JSON files increase, such a solution will no longer be feasible for real time applications.

As an example, let's assume a large 2D world with integer coordinates. For each coordinate pair (x, y), there is a corresponding tile T. Let us also assume that each tile is set procedurally, based on some seed S. The player is able to edit each tile, and wants to be able to store the changes for later retrieval.