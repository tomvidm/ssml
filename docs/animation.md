# Animation system
## Sprite animations

## Animated transforms

## Skeletal animation
The skeletal animation system lets the user attach sprites (or some other stuff) to fixtures. These fixtures can have other fixtures positioned relative to itself. For example, an upper body can have fixtures for shoulders, legs and a head. 

Skeletal animations shall support inverse kinematics, and integrate well with the physics system. As an example, it should be possible to build a character whose arms are connect to the shoulders by a spring, and should react to accordingly for absrupt changes in velocity. Rigs should also feature *dismemberment*, allowing parts of a Rig to be detached, creating two separate objects. This is another reason to let sprite attach to the rig, instead of directly having the sprite as a member. All the sprites are still in the render queue, and independent of rig configuration.

### Fixture
The fixture is the basis for the skeletal system. A fixture is either a root or a joint.
Each fixture has a parent (except for the root) and an array of children. When the fixture changes its transform, the transform is inherited by all its children. The inheritance will happen from a bottom-top manner, because the children should be unaware of its global position - on its local position relative to its parent.


### Root
The root is the main object that provides access to the joints. The root is transformable, and all its children is transformed relative to it.

### Joint